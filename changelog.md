# v2.4.2 (14.03.2025)

### improved
- do not log function name twice when locking/unlocking a status
- false arguments when locking/unlocking a status are no longer tolerated

### upgraded
- apply latest commits for rsync-time-backup (all commits from 2021-2024)
- upgrade calcardbackup to latest version (>= v8.1.0)

- - -

# v2.4.1 (10.01.2025)

### fixed
- on some systems `forked-daapd` could not be removed

- - -

# v2.4.0 (08.01.2025)

### upgraded
- upgrade `forked-daapd` to `owntone`

- - -

# v2.3.0 (01.12.2024)

### added
- iPhone/iPad-Backup

### improved
- some optimizations on nashorn webinterface regarding ajax, js and noscript version

### upgraded
- upgrade Nextcloud to version 21.0

- - -

# v2.2.0 (21.09.2024)

### added
- install `libimobiledevice-utils` and `usbmuxd` to prepare iOS/iPadOS-Backup/Recovery

### fixed
- change systemd-unit file for `dnscrypt-proxy.service` to restart on-failure, as on-abort is obviously not sufficient
- two typographical errors in `syslog_watcher_ignore.conf` have been corrected

### improved
- restart `dnscrypt-proxy.service` with `daily_jobs.sh` as its memory comsumption grows over time leading to an oom-issue
- wait 15 seconds after restarting dnscrypt-proxy or dnsmasq to make sure resolving works for following commands
- check status of `systemd-networkd.service` every hour and restart it if it is not running due to a bug in systemd 247
- align `upgrade_nextcloud.sh` with `ncupgrade v4.6.0`

### upgraded
- upgrade Nextcloud to version 20.0

- - -

# v2.1.0 (20.04.2024)

### improved
- reduce memory consumption of MariaDB
- restart `dnscrypt-proxy.service` automatically if exited with an unclean signal

### upgraded
- upgrade Nextcloud to version 19.0

- - -

# v2.0.1 (04.08.2023)

### fixed
- don't set override for `systemd-networkd-wait-online.service` on other boards than espressobin

- - -

# v2.0.0 (07.07.2023)

### fixed
- disabling the `hash_check_server` did not work as expected
- remove 'default_server' from nginx config as it prevents ssl access via IP to the webinterface
- don't try to reload empty elements from array `services_to_be_reloaded`

### improved
- improve legibility with 'Atkinson Hyperlegible' font
- remove some unneeded font files
- make Internet-Browsers after update read changed files from the server and not from their cache
- add Ethernet MAC-Adress to `/etc/systemd/network/10-br0.netdev` on espressobin
- minor adjustments in `syslog_watcher.sh` improve error reporting
- minor modifications in some scripts increase robustness and make shellcheck happy
- add loglevel for systemd-journald

### upgraded
- Upgrade to Debian Bullseye

- - -

# v1.10.1 (20.04.2023)

### fixed
- resizing SD-Card on Odroid HC4 led to an error during update

- - -

# v1.10.0 (18.04.2023)

### fixed
- do not overwrite date of last check for an update after an update
- fix some redirects
- in some cases calcardbackup could not be updated
- circumvent a bug in dnsmasq for Debian Bullseye: [nashorn-recipe #45](https://codeberg.org/nashorn/nashorn-recipe/issues/45)
- a problem with the media downloader has been solved
- do not show defaults in the status-section of the nashorn website during an update
- only do a fast-forward when pulling a repo from github (don't merge)

### improved
- move 'install_packages()' to 'functions_nashorn_update' as it is only being used during updates
- optimized check for a valid webserver certificate
- minor code optimizations
- don't log warnings when using nextclouds `occ`
- add major nextcloud version number to user-agent string
- show detailed software version on index.php
- add `--force` flag to `dnsmasq-refresh.sh` to disable timestamp check of list with adservers
- minor code improvements in update_nashorn.sh and functions_nashorn_update

### upgraded
- upgrade Nextcloud to version 18

- - -

# v1.9.0 (24.01.2023)

### added
- option to run all update functions to reset nashorn
- make URLs to nashorn repos and the integrity check configurable
- option to change hostname

### improved
- make sure that packages do not only get unpacked, but also do get configured
- remove unneeded *.dpkg-dist config files
- add syslog and kern.log when zipping logfiles for download
- remove an unneeded update of apts package index
- use number of processors for calculating processor load
- first preparations for the migration to Debian Bullseye
- update_nashorn.sh has been restructured
- on update execute only update routines that haven't been executed yet
- introduce a new variable to be able to notify about unsucessful updates
- improve process of invoking updates via webfrontend
- write messages also to logfile when executing scripts on the command line
- add $updated_until to user agent string when checking for updates
- update_history.log: add $updated_until and shorten commit hash
- move functions only needed for updates to 'functions_nashorn_update'
- print also currently running function name to log messages
- add year to timestamp of log messages
- unify output of feedback-websites for remote.php

### fixed
- corrected a typo in logrotate for syslog_watcher
- fix changelog (split v1.8.0 and v1.8.1)
- fix logrotate in armbian-hardware-optimization
- fix eventually changed logrotate config files
- stop and disable 'armbian-hardware-optimize.service'
- remove gzipped logfiles smaller than 100 Bytes
- use 24h format when zipping logfiles
- fix update of apt package lists
- fix text on webinterface: 'letzte Aktualisierung' --> 'letzte Überprüfung'
- also reset file owner in data folder when fixing permissions

- - -

# v1.8.1 (15.05.2022)

### improved
- make sharenames visible for guests
- allow releaseinfo changes for debian
- run debians unattended-upgrade with every update
- update php7.3 and minidlna

- - -

# v1.8.0 (27.04.2021)

### added
- option to support SMBv1 (be aware that this comes
  [with a high security risk](https://www.heise.de/newsticker/meldung/Wegen-Sicherheitsproblemen-Kein-SMB1-in-Windows-Neuinstallationen-3743127.html)!)

### improved
- improvement of disk spin down
- use APCu also for nextcloud cronjob
- do not restart PHP and MariaDB during Nextcloud cronjob
- make sure to not decrypt both drives simultaneously to mitigate oom issue
- update drive database from smartmontools with every update
- optimize nginx configuration
- separate between enabled javascript and request via direct link in remote.php
- increase limits for php-fpm
- reset permissions on dataPartition with update
- use APCu also for php-cli

### fixed
- correct permissions for status.txt
- revoke a modification in the samba config, that was supposed to avert a macOS Finder bug but caused problems with other clients

- - -

# v1.7.0 (26.07.2021)

### added
- set date via remote.php and restart chrony
- display current date and time in status section on nashorn.local
- display temperature of both internal disks in status section on nashorn.local 
- add user configurable bandwidth limit per download of the media downloader

### improved
- reject invalid user login requests to samba in any case
- detect `no useable certificate found` (dnscrypt-proxy) and set system time with IP from ntp.pool.org
- rename `restart-forked-daapd` to `restart_itunes_server` to use consistent link scheme for `remote.php`
- use the stable branch of calcardbackup as the master branch is deprecated since calcardbackup v2.0.0
- unify code for updating all used repos and make shellcheck happy
- simplify code installing/updating youtube-dl
- update ca-certificates and tzdata when updating nashorn
- resorting code in `update_nashorn.sh` to improve readability of code
- prevent user nashorn from getting root privileges
- log for which versions update tasks will be performed

- - -

# v1.6.0 (03.05.2021)

### improved
- adapt texts to new logo
- optimize process to renew webserver certificate if root certificate was recreated
- let 'renew_ssl_server-cert.sh' just do what its name says and add 'daily_jobs.sh'
- restart services only when configuration changed on daily jobs
- refresh list with adservers on a daily basis
- improve test for valid syntax of list with adservers
- deactivate Armbian reopository in apt sources
- add link to a fallback resolver list to dnscrypt-proxy configuration

### fixed
- reset autoupdatecheck in deliveready.sh
- disable dnscrypt-proxy in deliveready.sh and reenable it on next boot

- - -

# v1.5.0 (13.04.2021)

### added
- checkbox to enable/disable daily check for automatic updates
- testfile to test power status of disks
- display date of oldest backup on nashorn.local

### changed
- removed branch 'beta'
- new NAShorn-logo on webinterface

### improved
- update syslog_watcher_ignore.conf
- add used space and total space in GB of both HDDs to webinterface
- minor beautifications on nashorn.local 
- remove some unneeded css from nashorn.local
- check integrity of nashorn-repo at codeberg.org before pulling the repo for an update
- allow http(s)-access only for local clients
- link to downloaded media file in the download history
- prevent disks from spinning up for Nextclouds' cronjobs (config.php and datadirectory)
- run nextcloud cronjob every 5 minutes as it causes no disk activity anymore
- disable trying to be a samba master
- force smartd to read S.M.A.R.T. attributes after internal backup when disks are spinning anyway
- prohibit smartd to read S.M.A.R.T. attributes when disks are in standby to prevent disk spinups
- sync mysql only with internal backup when disks are spinning anyway
- syslog_watcher only needs to reload ignore list only on reboot or an after update
- update service file for forked-daapd (allow 10 restarts in 10 minutes)
- show samba shares only to users with read or write access to the share
- sort lines in status.txt alphabetically
- code readability
- manual for linux users

### fixed
- create_vtodo.sh: check for existence of config file before including it
- add user to systemd override to not break minidlna when upgrading
- syslog_watcher.sh: ignore hostname in syslog
- some minor improvements under the hood
- ajax did not get immediately loaded after onload of form
- syslog_watcher did not reread ignore list after update

- - -

# v1.4.0 (12.01.2021)

### added
- error pages for nginx
- script to automate tasks that need to be done before delivering NAShorn to customer
- possibility to delete all reminders from tasklist 'Meldung' via link

### fixed
- fix permissions for requested file in data folder instead of returning 403
- internalbackup.sh: don't rsync owner & permissions for data
- long folder names get folder icon in browser navigation through data folder

- - -

# v1.3.0 (07.01.2021)

### added
- browser navigation through data folder with webserver listing to be found under nashorn.local/Daten

### fixed
- rsync_tmbackup.sh: do not exit without deleting inprogress file after successful backup
- respect upper/lower case in filenames of samba-shares

### improved
- rsync_tmbackup.sh: increased verbosity when gracefully exiting the script
- rsync_tmbackup.sh: some additional inline comments to the script to improve code readability
- don't check for spun up disks when reading disk temperature in `internalbackup.sh`
- use server side includes for common headers on nashorn websites
- minor css-style improvements on index.php and ytdl.php
- cleanup: remove some files not needed anymore
- always use https for cups

- - -

# v1.2.0 (05.11.2020)

### added
- possibility to renew NAShorn root certificate

### fixed
- mysql-folder did not get synced to disk, if last change was older than 31 minutes
- touch table `oc_calendarobjects` on update to force mysql sync to disk on next run of `rsync_mysql.sh`
- ignore nonfatal error when setting MTU (introduced with Kernel 5.6)
- ignore dnscrypt-proxy timeouts occuring during setup when connected to DHCP-server of Mac OS

### improved
- add inline comment in `nashorn_cronjobs` about time interval for rsync_mysql.sh
- check disk temperatures every 3 hours (instead of every 30 minutes)
- ignore error, if disk does not support command to check IDE power mode status
- make sure to renew webserver-certificate after renewal of root-certificate

- - -

# v1.1.0 (30.10.2020)

### improved
- update instructions to iOS14
- better readability of code in `dnsmasq-refresh.sh`

### fixed
- fix incorrectly detected device path to backup disk
- make sure file for video-download-history is existing
- minor typo fix in update_history.log

- - -

# v1.0.0 (19.10.2020)

### added
- debug flag for `video-downloader.sh`
- legal section on nashorn.local website
- instructions for linux and other computers
- human readable software version on nashorn.local with link to changelog.md on codeberg.org
- display version of youtube-dl on nashorn.local including link to ChangeLog

### improved
- don't log complete diff of adserverlist at weekly refreshment
- add debug level to video_downloader.sh
- write message to log only if IP/FQDN/NameserverIP has changed to reduce logsize
- 'command-not-found' removed to speed up apt-get operations during update process
- quietly resynchronize package index with sources prior to checking/installing software
- remove packages quietly to increase readability of logfile

### fixed
- use 24 hour format instead of 12 hour format for filename of zipped logfiles
- don't log private data when downloading media files
- delete private data from log files
- link to crt-certificate in Android instructions
- fix installation instructions for certificate on windows with firefox
- reload unit files prior to reloading/-starting services
- minor css fix in noscript version of status table on nashorn.local

- - -

# v0.9.4 (21.09.2020, Beta 4)

### added
- new file `changelog.md` as changelog
- encrypt DNS-lookups for external domains with dnscrypt-proxy
- website to download video/audio from YouTube and german public broadcast stations
- show system load and network load on NAShorn website
- log for update history
- user editable config file to switch between branches of nashorn-software

### changed
- lowered loglevel for forked-daapd and minidlna due to unneeded logging of tmp files (ytdl)

### improved
- pull repo before checking out to a different branch when updating
- user is now able to poweroff NAShorn while an internal backup is running
- restructure NAShorn webinterface (add Dienste at top of site)
- update iTunes-Server (forked-daapd) to version 27.2
- use gzip instead of zip for logrotate
- keep logfiles for 20 years
- improve logging of disk temperature
- keep database on SD-Card and sync to disk only, if calendar/addressbook-tables have been modified
- increase and improve logging verbosity
- use subfolders for NAShorn configuration files where applicable (sysctl.conf, dnsmasq)
- syslog_watcher ignores some more unwanted fails/errors/warnings
- tidy up nashorn.js
- make NAShorn itself use its own DNS-Server to encrypt DNS-Traffic
- cleanup of repo, added .gitignore to keep repo clean from not needed files (e.g. .DS_Store, *.bak)
- systemd overrides for some services
- rewritten code for detection of IP, FQDN and local nameserver
- added a fallback option to be able to bring NAShorn back online in case something goes wrong with DNS
- use different expiration strategy for internalbackup of MariaDB to save disk space
- ignore Nextlouds apps.json for internalbackup
- log: clarify whether internalbackup.sh was invoked from cron or user interface

### fixed
- add symlinks to webserver-certificate for all domains/IPs to CUPS
- logrotation
- link to iTunes-Server on NAShorn website
- detection of FQDN and IP did not work reliably in some cases
- use uptime instead of date of last reboot fixes wrong reboot-date on website
- tolerate multiple nameservers given from systemd-resolved
- disable bin-logging of MariaDB as it bloats up ramlog and is not needed, because of daily backup
- increase open_files_limit for MariaDB
- minor fixes in CSS for NAShorn-website
- install adservers.conf also, if it is not existing yet
- check for disable IPv6 with every update
- some minor fixes and code optimizations

- - -

# v0.9.3 (14.08.2020, Beta 3)

### added
- give user feedback after clicking poweroff (display pullplug, or redirect to nashorn.eu/poweroff
- status-section of `index.php` shows now branchname, systemload and networkload (networkload only with activated javascript)
- user editable configfile `.user-config` in data-folder to give user possibility to switch between software-branches
- available branches: `stable` (stable), `beta` (unstable), `development` (experimental)
- log temperature every 4 hours and directly after backup of data folder to `nashorn.log`
- IP-address of nashorn.eu added to `dnsmasq.conf` to be able to redirect without dns-request

### changed
- renaming of branches: default branch is now `stable` instead of `master`
- remove periodically execution of long S.M.A.R.T-selftest from `smartd.conf` to keep discs cool and quite

### fixed
- autodetection of `IP` and `FQDN` could fail with some routers

### improved
- deactivate poweroff during update
- lazy load images in `index.php`
- increased verbosity
- some minor code optimizations and stability improvements

### deprecated
- branch `master` is subject to be deleted after all beta tester have upgraded to v0.9.3

- - -

# v0.9.2 (31.07.2020, Beta 2)

### added
- testing script for `internalbackup.sh`

### fixed
- remove `delaycompress` from `logrotate.conf` eliminates an error
- continue with backup, even if `calcardbackup` reports an error
- `internalbackup.sh` did not log some errors
- `internalbackup.sh` was not able to delete files from unfinished backup
- `internalbackup.sh`: don't rsync acls as it can be very expensive
- let user `nashorn` do backup of data folder to respect reserved block count
- fix permissions for files created by `calcardbackup`
- fix I/O error on some drives due to explicitely setting unsupported default value in `hdparm.conf`

### improved
- simplify configs for forked-daapd, hdparm, minidlna
- unify `nashorn_cronjobs` and pipe cronjob logging to `nashorn.log`
- increase error tolerance in scripts
- block backup/update if either one is running
- remove obsolete functions
- restart `syslog_watcher.sh` whenever `syslog` is truncated
- `syslog_watcher.sh` now parses only last 300 lines from `syslog` after bootup
- minor code optimizations and stability improvements
- increased verbosity for logs
- `rsync-tmbackup.sh`:
   - reflect behaviour of `df` when calculating to be used disk-space
   - respect reserved block count when calculating to be used disk-space
   - use block size when calculating to be used disk-space
   - don't preserve acls for shadow directories as this can be expensive
   - various stability improvements and code optimizations
   - do not log filenames to respect privacy of users
   - add warning about hardcoded paths to README.md
   - always keep at least two backups
   - add reminder after backups have been deleted if there are no more than 2 backups

- - -

# v0.9.1 (21.07.2020, Beta 1)

### added
- networkd-wait-online: ignore all interfaces except for `br0`
- new logfile `syslog_watcher.log` logs syslog_watcher actions
- show timestamp of last reboot in `index.php`
- systemd overrides for services which need dataPartition to be mounted on bootup

### fixed
- start `forked-daapd`, `mariadb` and `minidlna` after disks are mounted at bootup
- wait until dataPartition is mounted before checking for certificate
- don't exit `syslog_watcher.sh`, if `create_vtodo.sh` exits with an error
- make `index.php` to work also with javascript being deactivated

### improved
- only load items that are subject to change via ajax
- load ajax request into iframe instead of reloading whole website
- reload (instead of restart) `samba` after update to not break timemachine (no disconnect of clients)
- increased logging for various events
- remove all locks on reboot
- `syslog_watcher.sh`: reread ignore list from time to time
- remove stuff from pre-beta phase from `update_nashorn.sh`
- deactivate links on website, when an event is already running (backup, update)
- redirect to `index.php` with opened settings from `remote.php`
- split script and noscript version of status-section into different sections in `index.php`
- simplify check for renewal of webserver certificate and improve logging
- some minor code optimizations

- - -

# v0.9.0 (17.07.2020, Beta)

- initial release, rollout to beta testers
