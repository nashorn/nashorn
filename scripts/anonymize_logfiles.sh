#!/usr/bin/env bash
# shellcheck disable=SC1117

##################################################
#
# This script anonymizes logfiles on NAShorn
# and also removes diffs from old behaviour of dnsmasq_refresh.sh from logfiles
#
##################################################


set -euo pipefail
#set -x

# include functions_nashorn:
. /root/nashorn/scripts/functions_nashorn

anonymize_logfile() {
  # anonymizes logfile
  # arguments: ${1} - logfile to anonymize

  # remove URLs and paths to files in media-folders:
  sed -i -E "/\[video_downloader.sh\]/ s/(filme|musik|https?:\/)\/[^ ']+/\1\/DUMMY/g" "${1}"

  # remove URL/supported-Domain from check whether URL is from a supported website,
  # remove title, thumbnail and filename of video/audio to be downloaded,
  # remove detailed log of changed lines in dnsmasq.d/adservers.conf:
  sed -i -E "/((teststring|match) was '.*'|video-(title|thumbnail|filename)[[:space:]]*:|address=\/.+\/0\.0\.0\.0)/d" "${1}"

}

# counter for scanned and potentially anonymized logfiles:
count=0

# go through /var/log.hdd/nashorn and /var/log.hdd/nashorn and anonymize all logfiles '/var/log.hdd/nashorn/nashorn.log*':
for logdir in "/var/log.hdd/nashorn" "/var/log/nashorn"; do

  cd "${logdir}"

  for file in "nashorn.log"*; do

    # exclude zip-files as they were created before video_downloader.sh was added:
    [[ ${file##*.} != "zip" ]] || continue

    # last modification time of file:
    mtime=$(stat -c %Y "${file}")

    # gunzip logfile, if it is gzipped:
    if [[ ${file} == *.gz ]]; then
      gunzip "${file}"
      log=${file%.gz*}
    else
      log=${file}
    fi

    anonymize_logfile "${log}"
    count=$(( count + 1 ))

    # gzip logfile again, if it was gzipped:
    if [[ ${file} == *.gz ]]; then
      gzip -9 "${log}"
    fi

    # restore last modification time of file:
    touch -d "@${mtime}" "${file}"

  done

done

# set status that logfiles have been anonyized:
read_status_txt
statustxt[logfiles_anonymized]=yes
write_status_txt

# delete cronjob to anonymize logfiles (it has to be only run once):
rm -rf /etc/cron.d/nashorn_anonymize_logfiles

write_to_log "${count} Logfiles anonymisiert und cronjob 'nashorn_anonymize_logfiles' gelöscht!"

exit 0
