#!/usr/bin/env bash

#################################################################
#
# check_for_update.sh: checks for available update and update integrity
#                      of the nashorn software.
#
# This script compares the commit hash from the running branch of the
# repository 'nashorn' (read from /var/www/status.txt) with the commit hash
# in a textfile on a different server (hash_check_server). The URL of the hash_check_server
# and the desired branch are being read from '/media/dataPartition/data/.user-config'.
# If the commit hashes differ, then the commit hash found on the hash_check_server
# gets compared with the commit hash for the branch on the git server (which is also
# configured in /media/dataPartition/data/.user-config). If the commit
# hash from the git server is the same as the one retrieved from the
# hash_check_server and it differs from the commit hash of the running software,
# then there is an update available and can safely be pulled from the git server.
#
# It is possible to disable the hash_check_server in .user-config. In this case
# the integrity check is being skipped.
#
# The script does nothing else than checking for an available update and
# update integrity. Feedback to the invoker is given with different exit codes:
#
#   0 - update is available and can be pulled from the git server:
#       commit hashes on the hash_check_server and the git server are identical, but
#       the local commit hash is different. If the hash_check_server is disabled in
#       .user-config, the integrity is not being verified.
#   1 - an undefined error has occured:
#       the script exited with an error that has nothing to do with the commits
#  10 - no update available:
#       local hash and hash on the hash_check_server are identical
#       If the hash_check_server is disabled in .user-config, the local hash gets
#       compared with the hash from the git server.
#  11 - error in retrieving commit hash from the hash_check_server
#       (among other causes, this also happens with an invalid branch name)
#  12 - error in retrieving the latest commit hash from the git server
#  13 - local hash, hash from the hash_check_server and user_config[git_server]-hash
#       are all different: either the hash on the hash_check_server is not yet updated
#       or the repository on the git server is corrupt. This will never happen, if the
#       hash_check_server is disabled in .user-config
#  20 - the script ran through until the very end without finding an update nor an error
#       (this should never happen!)
#
#################################################################

# for debugging:
#set -x
#set -euo pipefail

# include functions_nashorn:
. /root/nashorn/scripts/functions_nashorn

# read status.txt to get the commit hash of the installed software version:
read_status_txt
# read .user-config to get the URL of the nashorn repo, the domain of the hash_check_server and the branch name for
# which we want to check for an update:
read_user_config


no_update_available() {
    write_to_log "no update available for '${user_config[branch]}'."
    exit 10
}

update_available() {
  # arguments: - ${1}: text whether repository has been integrity verified or not
  write_to_log "update available for '${user_config[branch]}' ${1}:" \
               "   -- local:        ${statustxt[software_commit_hash]}" \
               "   -- ${user_config[url_repo_nashorn]}: ${commit_hash_git_server}"
  exit 0
}


if [[ ${user_config[hash_check_server]} == "disabled" ]]; then
  # Integrity check is disabled. We need to remark that in the log:
  write_to_log "WARNING: integrity check of repo is disabled in '.user-config'"
else
  # create user-agent string to be used for requesting the commit hash from the hash_check_server:
  user_agent_string="NAShorn/check_for_update.sh/v${statustxt[software_version]}_uu${statustxt[updated_until]}_NC${statustxt[nextcloud_version]}"
  # add reboot_required marker to the user agent string, if a reboot is required and the board has not yet been rebooted:
  if [[ ${statustxt[reboot_required]} == "yes" ]]; then
    user_agent_string="${user_agent_string}_rr"
  fi
  # create url to the desired commit hash on the hash_check_server:
  commit_hash_url="https://${user_config[hash_check_server]}/nashorn_latest_commit_hash_${user_config[branch]}.txt"
  # get the commit hash for the software branch configured in .user-config, deposited at the hash_check_server
  commit_hash_check_server="$(curl -Ssf --user-agent "${user_agent_string}" "${commit_hash_url}" 2>/dev/null)"
  error="${?}"
  # do not continue, if the request failed. For example this is the case, when:
  #    - the webserver returns a #404 "not found" (occurs with invalid branch name)
  #    - the webservers answer is empty (e.g. http #301: redirect to another location)
  if [[ ${error} -ne 0 || -z ${commit_hash_check_server:-} ]]; then
    write_to_log "commit hash for '${user_config[branch]}' could not be retrieved from hash_check_server '${user_config[hash_check_server]}' (curl: ${error})!"
    exit 11
  fi

  # we are only interested in the first word of the retrieved file (in case there were received more lines for whatever reason):
  commit_hash_check_server="$(trim_leading_trailing_spaces "${commit_hash_check_server}")"
  commit_hash_check_server="${commit_hash_check_server%%[[:space:]]*}"
  # exit script, if the commit hash from the hash_check_server is the same as the installed commit hash:
  if [[ ${commit_hash_check_server} == "${statustxt[software_commit_hash]}" ]]; then
    # no update available:
    no_update_available
  fi
fi

# get the commit hash for the software branch of the nashorn repo (both configured in .user-config):
commit_hash_git_server="$(GIT_TERMINAL_PROMPT=0 git ls-remote --heads "${user_config[url_repo_nashorn]}" "${user_config[branch]}")"
# also here we are only interested in the first word:
commit_hash_git_server="$(trim_leading_trailing_spaces "${commit_hash_git_server}")"
commit_hash_git_server="${commit_hash_git_server%%[[:space:]]*}"
# do not continue, if the result is empty:
if [[ -z ${commit_hash_git_server:-} ]]; then
  write_to_log "commit hash for '${user_config[branch]}' could not be retrieved from repo '${user_config[url_repo_nashorn]}'!"
  exit 12
fi

# If hash_check_server is set to "disabled" in .user-config, we do only need to compare local and remote hashes:
if [[ ${user_config[hash_check_server]} == "disabled" ]]; then
  if [[ ${commit_hash_git_server} == "${statustxt[software_commit_hash]}" ]]; then
    # no update available:
    no_update_available
  else
    # the retrieved hash from the git server is different from the the local commit hash. That means
    # there is an update available, but log that the git-server  has not been checked for integrity:
    update_available "(integrity NOT verified!)"
  fi
fi

# verify that hashes from the hash_check_server and the git server are identical (to verify the integrity of repo at the git server):
if [[ ${commit_hash_check_server} == "${commit_hash_git_server}" ]]; then
  # the retrieved hashes from the hash_check_server and from the git server are the same but they are different from
  # the local commit hash. That means there is a new version available and the repo at the git server has integrity.
  #
  # there is an update available and the the integrity of the repo on the git server has been verified.
  # An update may safely be pulled and installed:
  update_available "(integrity verified)"
else
  # the retrieved commit hashes are different, meaning that:
  #    - either the commit hash at the hash_check_server has not been updated
  #    - or the remote repo on the git server is corrupt.
  write_to_log "different commit hashes for branch '${user_config[branch]}' on hash_check_server and nashorn-repository." \
               "   -- installed:    ${statustxt[software_commit_hash]} (${statustxt[software_branch]})" \
               "   -- ${user_config[hash_check_server]}:   ${commit_hash_check_server}" \
               "   -- ${user_config[url_repo_nashorn]}: ${commit_hash_git_server}" \
               "   ------> update not yet released or the repo on the git server is corrupt!"
  exit 13
fi

exit 20
