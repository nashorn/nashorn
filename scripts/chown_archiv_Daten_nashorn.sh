#!/usr/bin/env bash

# chown_archiv_Daten_nashorn.sh
# ändert den Eigentümer des Datenordners im Archiv auf 'nashorn:nashorn'

. /root/nashorn/scripts/functions_nashorn

# falls internalbackup.sh gerade ausgeführt wird: warten bis backup beendet ist:
read_status_txt
while [[ ${statustxt[backup_in_progress]} == "yes" ]]; do
  sleep 60
  read_status_txt
done

# Backup-Platte 'rw'-remounten:
remount_backup_disk "rw"
write_to_log "Besitzrechte des Datenordners im Archiv auf nashorn:nashorn ändern..."
chown -R nashorn:nashorn /media/backup/archiv/Daten

read_status_txt
if [[ ${statustxt[backup_in_progress]} == "no" ]]; then
  # Backup-Platte 'ro'-remounten:
  remount_backup_disk "ro"
else
  write_to_log "internes Backup läuft gerade. Backup-Platte wird nicht 'ro'-remountet."
fi

write_to_log "Besitzrechte des Datenordners im Archiv erfolgreich geändert."

exit 0
