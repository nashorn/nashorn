#!/usr/bin/env bash

#################################################################
#
# This script creates a vtodo in a tasklist of user (user) and shares
# that tasklist with a certain user (sharee).
# If the tasklist doesn't exist, it will be created.
#
# Usage: ./create_vtodo.sh "REMINDER_SUMMARY" "REMINDER_DESCRIPTION"
#    or: ./create_vtodo.sh --only-create-and-share-vtodolist
#    or: ./create_vtodo.sh --clear-vtodolist
#    or: ./create_vtodo.sh --delete-vtodolist
#
# NOTE: if the first given argument is --only-create-and-share-vtodolist
#       then the task list will only be created and shared and no vtodo
#       will be created.
# NOTE: if the first given argument is --clear-vtodolist, then all VTODOS
#       will be deleted from the tasklist
# NOTE: if the first given argument is --delete_vtodolist, then the
#       tasklist will be deleted with all containing tasks and removes
#       the share
#
#################################################################

# for debugging:
#set -x

# set -euo pipefail

#################################################################
# BEGIN VARIABLE DECLARATION
# defaults will be overwritten with values found in file ${nashorn_conf}
# ( ${nashorn_conf} is being declared in functions_nashorn )
#################################################################

# Nashorn-Funktionen einbinden:
. /root/nashorn/scripts/functions_nashorn
# read status.txt (software_version) for event PRODID
read_status_txt

# Nextcloud-URL:
nextcloud_url="https://127.0.0.1/nextcloud/"

# Nextcloud user account (dummy password will be replaced with value in ${nashorn_conf}):
user="system"
nextclouduser_system_password="n"

# name of user to share Tasklist with (sharee):
sharee="nashorn"

# reminder properties:
tasklist="Meldung"     # name of the tasklist to be used
color="#FF0000"        # the color of the tasklist
priority="1"           # priority of VTODO (1=highest ... 3=lowest (for iOS), (Nextcloud: 1-9))
status="NEEDS-ACTION"  # status of the VTODO (NEEDS-ACTION, COMPLETED, IN-PROCESS or CANCELLED)
reminder_alarm="yes"   # "yes" or "no" whether or not an alarm should be displayed on clients

# file with vtimezone (needs to be included in tasklist and VTODO):
vtimezonefile="${scripts_repo}/vtimezone-europe-berlin.ics"
# Timezone used for VTODO:
timezone="Europe/Berlin"

# konfigurierte Variablen einlesen, falls $nashorn_conf vorhanden ist:
if [[ -f ${nashorn_conf} ]]; then
  # shellcheck source=/etc/nashorn/nashorn.conf
  . "${nashorn_conf}"
fi

#########    DO NOT CHANGE VARIABLES BELOW THIS LINE!   #########
#
# URL to tasklist (set together with variables declared above):
tasklist_url="${nextcloud_url%%/}/remote.php/dav/calendars/${user}/${tasklist}/"
# Product-ID:
prodid="-//nashorn//create_vtodo_v${statustxt[software_version]}//EN"
# datestamp used for VTODO (set together with variables declared above):
dtstamp=";TZID=${timezone}:$(date +%Y%m%dT%H%M%S)"

#################################################################
# END VARIABLE DECLARATION                                      #
#################################################################


# check for argument (text for reminder):
[[ -n ${1:-} ]] || error_exit "Argument missing (summary text for VTODO) - at least one argument ist mandatory!"

#
# BEGIN FUNCTIONS:
#
# NOTICE: creation of data for requests "stolen" from Nextcloud requests analyzed with Browser developer tools
#         as suggested by tcitworld: https://help.nextcloud.com/t/api-for-sharing-calendars-task-lists/78885/2


clear_tasklist() {
  # deletes all VTODO from the configured tasklist

  local data header1 header2 everything regex ics total_objects deleted_objects undeleted_objects i result

  # prepare data for REPORT request:
  data='
    <x1:calendar-query xmlns:x1="urn:ietf:params:xml:ns:caldav">
      <x0:prop xmlns:x0="DAV:" />
      <x1:filter>
        <x1:comp-filter name="VCALENDAR">
          <x1:comp-filter name="VTODO" />
        </x1:comp-filter>
      </x1:filter>
    </x1:calendar-query>'

  header1='Content-Type: application/xml; charset=utf-8'
  header2='Depth: 1'

  # MKCOL request to create the tasklist on the server (catch stderr, throw away stdout):
  everything="$(curl -sS -f -k -u "${user}:${nextclouduser_system_password}" -H "${header1}" -H "${header2}" -X REPORT -d "${data}" "${tasklist_url}" 2>&1 || : )"

  regex=".*d:href>/nextcloud/remote\\.php/dav/calendars/system/${tasklist}/nashorn.+\\.ics</d:href.*"
  while [[ ${everything:-} =~ ${regex} ]]; do
    everything="${everything#*d:href>\/nextcloud\/remote\.php\/dav\/calendars\/system\/${tasklist}\/}"
    ics+=("${everything%%\<\/d:href>*}")
  done

  # total objects to be deleted:
  total_objects=${#ics[@]}
  # counter for deleted objects:
  deleted_objects=0
  # counter for undeleted objects:
  undeleted_objects=0

  for (( i=0; i<${#ics[@]}; i++ )); do

    # delete tasklist (catch stderr, throw away stdout):
    result="$(curl -sS -f -k -u "${user}:${nextclouduser_system_password}" "${tasklist_url}${ics[${i}]}" -X DELETE 2>&1 1>/dev/null || : )"
    # Leerzeichen am Ende der curl-Ausgabe löschen:
    result="${result% }"

    # Falls result nicht leer ist, hat curl einen Fehler zurückgegeben, aber 404 kann auch ignoriert werden, denn das besagt dass das Objekt nicht existiert:
    if [[ -z ${result:-} || ${result:-} != "curl: (22)"*"404" ]]; then
      # counter für gelöschte Objekte erhöhen:
      deleted_objects=$(( deleted_objects + 1 ))
    else
      # undeleted_objects erhöhen, da dieses Objekt nicht gelöscht werden konnte:
      undeleted_objects=$(( undeleted_objects + 1 ))
    fi

  done

  write_to_log "(clear_tasklist) ${deleted_objects:-0} Erinnerungen wurden gelöscht, ${undeleted_objects:-} Erinnerungen konnten nicht gelöscht werden (insgesamt ${total_objects:-0})."

  return "${undeleted_objects:-0}"
}

create_tasklist() {
  # creates the configured tasklist. No need to check, whether the tasklist is already existing, because the
  # server returns a different html-status code, but curl doesn't throw an error because of that.

  # this will only create a tasklist, not a calender (<x1:comp name="VTODO"/>)

  local data result error

  # prepare data for MKCOL request
  data='
    <x0:mkcol xmlns:x0="DAV:">
      <x0:set>
        <x0:prop>
          <x0:resourcetype>
            <x0:collection/>
            <x1:calendar xmlns:x1="urn:ietf:params:xml:ns:caldav"/>
          </x0:resourcetype>
          <x0:displayname>'${tasklist}'</x0:displayname>
          <x6:calendar-color xmlns:x6="http://apple.com/ns/ical/">'${color}'</x6:calendar-color>
          <x4:calendar-enabled xmlns:x4="http://owncloud.org/ns">1</x4:calendar-enabled>
          <x1:supported-calendar-component-set xmlns:x1="urn:ietf:params:xml:ns:caldav">
            <x1:comp name="VTODO"/>
          </x1:supported-calendar-component-set>
          <x1:calendar-timezone xmlns:x1="urn:ietf:params:xml:ns:caldav">
            <![CDATA[
              BEGIN:VCALENDAR
              PRODID:'${prodid}'
              VERSION:2.0
              '$(cat "${vtimezonefile}")'
              END:VCALENDAR
            ]]>
          </x1:calendar-timezone>
        </x0:prop>
      </x0:set>
    </x0:mkcol>'

  # MKCOL request to create the tasklist on the server (catch stderr, throw away stdout):
  result="$(curl -sS -f -k -u "${user}:${nextclouduser_system_password}" -H "Content-Type: application/xml; charset=utf-8" -X MKCOL "${tasklist_url}" -d "${data}" 2>&1 1>/dev/null || : )"
  # Leerzeichen am Ende der curl-Ausgabe löschen:
  result="${result% }"
  # curl errorcode extrahieren:
  error=${result#*\(}
  error=${error%%\)*}

  # write according message to nashorn.log:
  if [[ -z ${result:-} ]]; then
    # Ergebnis ist leer - Erinerungsliste wurde ohne Fehlermeldung erstellt:
    write_to_log "Erinnerungsliste 'Meldung' erstellt."
  elif [[ ${result:-} == "curl: (22)"*"405" ]]; then
    # Fehler 405 - Erinnerungsliste existiert bereits.
    # Dies ist der normale Fall, der jedes Mal eintritt, wenn eine Erinnerung erstellt wird.
    # Daher ist eine Log-Ausgabe nicht erforderlich und die error-Variable kann gelöscht werden:
    unset -v error
  else
    # unbekannter Fehler:
    write_to_log "Fehler beim Erstellen der Erinnerungsliste 'Meldung': '${result}'"
  fi
  return "${error:-0}"
}


create_vtodo() {
  # creates a vtodo with alarm and priority in the configured tasklist.

  # Summary of task (given as first argument to this script):
  summary="${1}"
  description="${2:-}"
  # set valarm to an empty string:
  valarm=""

  # UUID for VTODO:
  uuid_vtodo="nashorn-$(cat /proc/sys/kernel/random/uuid)"

  # prepare VALARM component, if configured with variable $reminder_alarm:
  if [[ ${reminder_alarm:-no} == "yes" ]]; then
    # UUID for VALARM:
    uuid_valarm="nashorn-$(cat /proc/sys/kernel/random/uuid)"
    # VALARM-component to be included in the VEVENT (no spaces at the beginning of lines allowed!):
    valarm='
BEGIN:VALARM
ACTION:DISPLAY
DESCRIPTION:'${summary}'
TRIGGER;VALUE=DATE-TIME'${dtstamp}'
UID:'${uuid_valarm}'
X-WR-ALARMUID:'${uuid_valarm}'
END:VALARM'
  fi

  # prepare data for vtodo. No spaces at the beginning of lines within the string allowed!
  data='
BEGIN:VCALENDAR
VERSION:2.0
PRODID:'${prodid}'
CALSCALE:GREGORIAN
BEGIN:VTODO
UID:'${uuid_vtodo}'
DTSTAMP'${dtstamp}'
CREATED'${dtstamp}'
LAST-MODIFIED'${dtstamp}'
SUMMARY:'${summary}'
PRIORITY:'${priority}'
STATUS:'${status}'
DESCRIPTION:'${description}${valarm}'
END:VTODO
'$(cat "${vtimezonefile}")'
END:VCALENDAR'
  # End vtodo

  # PUT request to create the event on the server:
  curl -sS -k -u "${user}:${nextclouduser_system_password}" "${tasklist_url}${uuid_vtodo}.ics" -X PUT -d "${data}" 2>&1

  # write message to nashorn.log about created vtodo:
  write_to_log "VTODO CREATED: ${description}"
}


delete_tasklist() {
  # deletes the configured tasklist with all included tasks. This will also remove the share of the tasklist.

  local result error

  # delete tasklist (catch stderr, throw away stdout):
  result="$(curl -sS -f -k -u "${user}:${nextclouduser_system_password}" "${tasklist_url}" -X DELETE 2>&1 1>/dev/null || : )"
  # Leerzeichen am Ende der curl-Ausgabe löschen:
  result="${result% }"
  # curl errorcode extrahieren:
  error=${result#*\(}
  error=${error%%\)*}

  # write according message to nashorn.log:
  if [[ -z ${result:-} ]]; then
    # Ergebnis ist leer - Erinerungsliste wurde ohne Fehlermeldung gelöscht:
    write_to_log "Erinnerungsliste 'Meldung' gelöscht."
    # also print a notice to syslog_watcher.log, that tasklist has been reset:
    {
      printf '%s\n' "###################################################################################"
      printf '%s\n' "$(date +"${date_fmt}") [${0##*/}]: Erinnerungsliste 'Meldung' gelöscht."
    } >> "${logfile%/*}/syslog_watcher.log"
  elif [[ ${result:-} == "curl: (22)"*"404" ]]; then
    # Fehler 404 - Erinnerungsliste existiert nicht:
    write_to_log "Erinnerungsliste 'Meldung' existiert nicht ('${result}')."
    # error-Variable löschen, da dies kein Fehler ist, der uns interessiert:
    unset -v error
  else
    # unbekannter Fehler:
    write_to_log "Fehler beim Löschen der Erinnerungsliste 'Meldung': '${result}'"
  fi

  return "${error:-0}"
}


share_tasklist() {
  # shares the configured tasklist with certain user (the sharee), who has read-write access to the list.
  # only read-access doesn't make any sense, since MacOS and iOS allow to locally(!) delete
  # events from this list and this list itself despite the missing write privileges. Nothing will
  # be deleted from the server though, which may irritate users because locally deleted events will still
  # be present on other devices (and the server).

  local data result error

  # prepare data for POST request:
  data='
    <x4:share xmlns:x4="http://owncloud.org/ns">
      <x4:set>
        <x0:href xmlns:x0="DAV:">principal:principals/users/'${sharee}'</x0:href>
        <x4:read-write/>
      </x4:set>
    </x4:share>'

  # POST request to share the tasklist with the sharee (catch stderr, throw away stdout):
  result="$(curl -sS -f -k -u "${user}:${nextclouduser_system_password}" -H "Content-Type: application/xml; charset=utf-8" -X POST "${tasklist_url}" -d "${data}" 2>&1 1>/dev/null || : )"
  # Leerzeichen am Ende der curl-Ausgabe löschen:
  result="${result% }"
  # curl errorcode extrahieren:
  error=${result#*\(}
  error=${error%%\)*}

  # Wenn die Erinnerungsliste ohne Fehlermeldung geteilt wurde oder bereits mit 'nashorn' geteilt war ist das Ergebnis leer.
  # Falss $result nicht leer ist, ist also ein Fehler aufgetreten, der ausgegeben werden muss:
  if [[ -n ${result:-} ]]; then
    # unbekannter Fehler:
    write_to_log "Fehler beim Teilen der Erinnerungsliste 'Meldung': '${result}'"
  fi

  return "${error:-0}"
}


#
# END FUNCTIONS
#


# if first argument is --clear-vtodolist: delete all vtodos from the tasklist and exit this script:
if [[ ${1} == "--clear-vtodolist" ]]; then
  clear_tasklist || errorcode="${?}"
  exit ${errorcode:-0}
fi

# if first argument is --delete-vtodolist: delete vtodolist including all vtodos and shares and exit this script:
if [[ ${1} == "--delete-vtodolist" ]]; then
  delete_tasklist || errorcode="${?}"
  exit ${errorcode:-0}
fi

create_tasklist || errorcode="${?}"

# only create a vtodo, if not desired to only create and share the todolist:
if [[ ${1} != "--only-create-and-share-vtodolist" ]]; then
  # without sleeping in between, the server throws a "Bad Gateway" error:
  sleep 5
  create_vtodo "${1}" "${2}"
fi

# without sleeping in between, the server throws a "Bad Gateway" error:
sleep 5

share_tasklist || errorcode=$(( errorcode + ${?} ))

exit ${errorcode:-0}
