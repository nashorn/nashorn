#!/usr/bin/env bash

########################################################
#
# daily_jobs.sh - führt tägliche Wartungsaufgaben aus (wird auch bei reboot von reboot_jobs.sh aufgerufen)
#
# Argumente:
#     - ${1}: wenn gesetzt auf "rootcert_renewed" wird ein Erneuern des Webserverzertifikates erzwungen.
#             (wird benutzt beim erstem Bootvorgang beim Kunden!)
#
# (c) 2021 Freirechner GbR
#
########################################################

# zum debuggen:
#set -x
#set -euo pipefail

# NAShorn Funktionen einbinden und Nameserver, IP und FQDN ermitteln
. "/root/nashorn/scripts/functions_nashorn"

# dnscrypt-proxy.service neu starten, um OOM-issue zuvor zu kommen:
reload_services "dnscrypt-proxy"

# Variable setzen, dass root-Zertifikat erneuert wurde. Andere übergebene Werte werden ignoriert:
if [[ ${1:-} == "rootcert_renewed" ]]; then
  rootcert_renewed=${1:-}
fi

# prüfen, ob Webserver-Zertifikat erneuert werden muss:
write_to_log "'renew_ssl_server-cert.sh' aufrufen:"
"${scripts_repo}/renew_ssl_server-cert.sh" "${rootcert_renewed:-}"

# die Trusted-Domains von Nextcloud prüfen und aktualisieren, falls sich etwas geändert hat:
write_to_log "Nextclouds Trusted Domains prüfen und ggfls. aktualisieren:"
add_trusted_domains_to_nextcloud

# prüfen, ob die NAShorn-DNS-Konfiguration noch gültig ist und gegebenenfalls anpassen:
check_dns_configuration

# Datenbankeinträge älter als 7 Tage aus dem Verlauf des Videodownloaders löschen:
"${scripts_repo}/video_downloader.sh" "videolink_history_delete" "7"

# Adserverliste für dnsmasq aktualisieren:
"${scripts_repo}/dnsmasq_refresh.sh"

# remove Samba core dumps from log directories:
if [[ -n $(find /var/log{,.hdd}/samba/cores/{n,s}mbd/ -mindepth 1) ]]; then
  write_to_log "deleting samba core dump files from '/var/log/' and '/var/log.hdd/'..."
  rm -rf /var/log{,.hdd}/samba/cores/{n,s}mbd/*
fi
# Damit sind die täglichen Jobs alle ausgeführt:
write_to_log "Ende 'daily_jobs.sh'"

exit 0
