#!/usr/bin/env bash

#################################################################
#
# deliveready.sh: delete traces of testboot
#
# After testing, some things need to be reset before delivering NAShorn
# to a customer. These things are:
#   - delete and recreate tasklist 'Meldungen'
#   - delete root certificate and key of the certificate authority
#   - delete recipe repo
#
# After the mentioned jobs are done, a poweroff is issued.
#
#################################################################

# for debugging:
#set -x
#set -euo pipefail

# dieses Skript darf nur über remote.sh aufgerufen werden:
if [[ ${1:-} != "remote.sh_says_hi" ]]; then
  printf '%s\n' "++  'deliveready.sh' ist nicht von remote.sh aufgerufen worden. Abbruch des Skripts."
  exit 1
fi

#############################################
### Vorbereitungen

# Funktionen für NAShorn einbinden:
# shellcheck source=functions_nashorn
. /root/nashorn/scripts/functions_nashorn

# Ausgabe ins Log zur Info, dass deliveready.sh gestartet wurde:
write_to_log "Start"

# Bisher sind keine Fehler aufgetreten:
error=0

#############################################
### Erinnerungsliste 'Meldungen' löschen (inklusive aller Inhalte) und neu erstellen:
printf '%s\n' "++  [${0##*/}]: Erinnerungsliste 'Meldung' löschen..."
"${scripts_repo}"/create_vtodo.sh --delete-vtodolist || errorcode="${?}"

# Sleep a little bit, otherwise a 'Bad Gateway Error' might occur:
sleep 5

printf '%s\n' "++  [${0##*/}]: Erinnerungsliste 'Meldung' neu erstellen und mit user 'nashorn' teilen..."
"${scripts_repo}"/create_vtodo.sh --only-create-and-share-vtodolist || errorcode=$(( errorcode + ${?} ))

if [[ ${errorcode:-0} -eq 0 ]]; then
  ausgabe="Aufgabenliste 'Meldung' wurde erfolgreich gelöscht, neu erstellt und geteilt."
else
  ausgabe="FEHLER BEIM LÖSCHEN DER AUFGABENLISTE 'Meldung' - ERRORCODE: ${errorcode}!"
  # errorcode zurücksetzen
  unset -v errorcode
  # Fehlerzähler erhöhen:
  error=$(( error + 1 ))
fi
printf '%s\n' "++  [${0##*/}]: ${ausgabe}"


#############################################
### Root-Zertifikat und Schlüssel der CA löschen (damit es beim nächsten Reboot neu erstellt wird):
printf '%s\n' "++  [${0##*/}]: lösche Root-Zertifikat und Schlüssel der CA..."
rm "${ca}/NAShorn-Zertifikat.pem" || errorcode="${?}"
rm "${ca}/cakey.pem" || errorcode=$(( errorcode + ${?} ))
if [[ ${errorcode:-0} -eq 0 ]]; then
  ausgabe="Root-Zertifikat und Schlüssel der CA wurden erfolgreich gelöscht."
else
  ausgabe="FEHLER BEIM LÖSCHEN VON ROOT-ZERTIFIKAT UND SCHLÜSSEL DER CA - ERRORCODE: ${errorcode}!"
  # errorcode zurücksetzen
  unset -v errorcode
  # Fehlerzähler erhöhen:
  error=$(( error + 1 ))
fi
# Ausgabe im Logfile verewigen:
write_to_log "${ausgabe}"
# Ebenso auf stdout ausgeben (für Aufruf über remote.php):
printf '%s\n' "++  [${0##*/}]: ${ausgabe}"


#############################################
### Autoupdatececk auf 'undefined' zurücksetzen:
read_status_txt || errorcode=1
statustxt[autoupdatecheck]="undefined"
write_status_txt || errorcode=$(( errorcode + 1 ))
if [[ ${errorcode:-0} -eq 0 ]]; then
  ausgabe="Autoupdatecheck auf 'undefined' zurückgesetzt."
else
  ausgabe="FEHLER BEIM ZURÜCKSETZEN VON AUTOUPDATECHECK - ERRORCODE: ${errorcode}!"
  # errorcode zurücksetzen
  unset -v errorcode
  # Fehlerzähler erhöhen:
  error=$(( error + 1 ))
fi
# Ausgabe im Logfile verewigen:
write_to_log "${ausgabe}"
# Ebenso auf stdout ausgeben (für Aufruf über remote.php):
printf '%s\n' "++  [${0##*/}]: ${ausgabe}"


#############################################
### dnscrypt-proxy.service deaktivieren
# (damit beim ersten Boot beim Kunden keine Fehlermeldung über fehlerhaften fallback-resolver in Erinnerungsliste 'Meldungen' erstellt wird):
systemctl disable dnscrypt-proxy.service
ausgabe="'dnscrypt-proxy.service' wurde deaktiviert (wird beim nächsten Boot wieder aktiviert)."
# Ausgabe im Logfile verewigen:
write_to_log "${ausgabe}"
# Ebenso auf stdout ausgeben (für Aufruf über remote.php):
printf '%s\n' "++  [${0##*/}]: ${ausgabe}"


#############################################
### Ende Gelände
# write message to nashorn.log:
if [[ ${error:-0} -eq 0 ]]; then
  ausgabe="erfolgreich beendet."
else
  ausgabe="NICHT FEHLERFREI BEENDET - ${error} FEHLER!."
fi
write_to_log "${ausgabe}"
printf '%s\n' "++  [${0##*/}]: ${ausgabe}"


# ByeBye
exit ${error:-0}
