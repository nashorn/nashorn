#!/usr/bin/env bash

##################################################
#
# dnsmasq_refresh.sh - refresh the list with adservers for dnsmasq
#
# arguments:
#    --force: force refreshing the adserverlist (even if it is not older than 24 hours)
#
##################################################


# for debugging:
#set -x
#set -euo pipefail

# reduce script priority, to save resources for other processes
renice -n 19 $$ >/dev/null 2>&1

# include functions_nashorn
. /root/nashorn/scripts/functions_nashorn

# path to temporary file to store an intermediate state of the downloaded list with adservers:
adservers_temp="/tmp/dnsmasq.adservers.conf"

# if '--force' flag is unset, refresh adserverlist only, if it is older than 24 hours:
if [[ ${1:-} != "--force" ]]; then

  # timestamp of the local file with the list of adservers:
  timestamp_adservers_dnsmasq=$(stat -c %Y "${adservers_dnsmasq}" 2>/dev/null)
  # yesterdays timestamp plus 1 minute (downloading and creating the adservers list takes a little while - thats what this 1 minute buffer is for)
  timestamp_yesterday=$(( $(date +%s -d "yesterday") + 60 ))

  if [[ ${timestamp_yesterday} -lt ${timestamp_adservers_dnsmasq:-0} ]]; then
    write_to_log "List with adservers doesn't need to be refreshed as it is not older than 24 hours."
    exit 0
  fi

fi

# get hosts list from Stephen Black:
curl -sS https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts | awk '$1 == "0.0.0.0" { print "address=/"$2"/0.0.0.0"}' > "${adservers_temp}"

# proceed only, if there is at least one correct line in the temporary file (TLDs and everything not ending with 0.0.0.0 gets sorted out)
if grep -qsE '^address=\/([a-z0-9-]+\.)+[a-z0-9-]+\/0\.0\.0\.0$' "${adservers_temp}" >/dev/null 2>&1; then

  # print only syntactically correct lines to the file with the adservers which will be used by dnsmasq.service
  grep -E '^address=\/([a-z0-9-]+\.)+[a-z0-9-]+\/0\.0\.0\.0$' "${adservers_temp}" > "${adservers_dnsmasq}"

  # dnsmasq 2.85-1 (which is coming with Debian Bullseye) doesn't tolerate domain names with a dash that doesn't
  # an alphanumeric character on both sides of it. The exact behaviour is a bit erratic though (see ref-2).
  # Only Debian Bullseye is affected. At the time fo writing this, Debian Bookworm (dnsmasq 2.89-1) does
  # not seem to have this problem (16.04.2023)
  # To circumvent this issue for Bullseye, domain names containing dashes which do not have an alphanumeric
  # character on both sides of it, will be removed from the blocklist in the following if-clause.
  # ref-1: https://www.mail-archive.com/dnsmasq-discuss@lists.thekelleys.org.uk/msg15824.html
  # ref-2: https://codeberg.org/nashorn/nashorn-recipe/issues/45
  if [[ ${debian_codename} == "bullseye" ]]; then
    sed -i -E -- '/((\/|\.|-)-|-(\/|\.))/d' "${adservers_dnsmasq}"
  fi

  # restart dnsmasq to activate the changed configuration with the new adserver list:
  systemctl restart dnsmasq.service >> "${logfile}" 2>&1

  write_to_log "refreshed list with adservers and restarted dnsmasq.service."

else

  # there was not a single line which matched the regular expression for correct lines:
  write_to_log "Not a single line found which matches the regular expression. Will not refresh list with adservers."

fi

# delete temporary file:
rm -f "${adservers_temp}"

# the list with adservers used to be at a different location in first versions of nashorn. Let's delete it, if it is still there:
[[ ! -f /etc/dnsmasq.adservers.conf ]] || rm -f /etc/dnsmasq.adservers.conf

# ByeBye
exit 0
