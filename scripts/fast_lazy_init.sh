#!/usr/bin/env bash

#
# remounts both drives with init_itable=0 to speed up inode initialization
#

set -euo pipefail

. /root/nashorn/scripts/functions_nashorn

write_to_log "remount both drives with 'init_itable=0' to speed up inode initialization:"
mount --verbose --target /media/backup --options remount,rw,init_itable=0
mount --verbose --target /media/dataPartition --options remount,init_itable=0

exit 0
