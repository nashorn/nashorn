#!/usr/bin/env bash

#################################################################
#
# idevicebackup_wrapper.sh: wrapper script to start a backup of an iDevice
#
# Usage: `./remote.sh idevicebackup`
#
#################################################################


# for debugging:
#set -x
#set -euo pipefail

# dieses Skript darf nur über remote.sh aufgerufen werden:
if [[ ${1:-} != "remote.sh_says_hi" ]]; then
  printf '%s\n' "++  'idevicebackup_wrapper.sh' ist nicht von remote.sh aufgerufen worden. Abbruch des Skripts."
  exit 1
fi

# Abbruch des Skriptes, fall kein iDevice angeschlossen ist (kann nur per Terminal passieren):
if ! idevice_id > /dev/null 2>&1; then
  printf '%s\n' "++  Es ist kein iDevice angeschlossen. Abbruch des Skripts."
  exit 1
fi

function encryption_on() {
  # aktiviert Verschlüsselung mit idevicebackup_password_new

  # altes Passwort ist leerer String, daher ist davon auszugehen, dass noch kein Sicherungspasswort vergeben ist:
  if ! idevicebackup2 encryption on "${user_config[idevicebackup_password_new]}"; then
    # Verschlüsselung konnte nicht aktiviert werden:
    write_to_log "Verschlüsselung konnte nicht aktiviert werden!"
    unknown_password="yes"
  else
    write_to_log "Verschlüsselung erfolgreich aktiviert!"
    # Da diese Funktion als fallback auch aufgerufen wird, falls ein altes Sicherungspasswort in .user-config angegeben ist,
    # das aber nicht zum Gerät passt, muss hier unknown_password zurückgesetzt werden, da schon auf "yes" gesetzt war wegen
    # "falschem" Passwort:
    unset -v unknown_password
  fi
}

function change_pw() {
  # ändert das Sicherungspasswort auf dem Gerät. Benutzt werden die Passwörter aus der .user-config
  if ! idevicebackup2 changepw "${user_config[idevicebackup_password_old]}" "${user_config[idevicebackup_password_new]}"; then
    # Passwortänderung fehlgeschlagen!
    # Verschlüsselungspasswort konnte nicht geändert werden: angegebenens altes Passwort stimmt nicht!
    write_to_log "Sicherungspasswort konnte nicht geändert werden ('idevice_password_old' passt nicht zum angeschlossenen Gerät)."
    unknown_password="yes"
  else
    write_to_log "Sicherungpasswort erfolgreich geändert!"
    # Da diese Funktion auch als fallback aufgerufen wird, falls kein altes Sicherungspasswort in .user-config
    # angegeben ist, muss hier unknown_password zurückgesetzt werden, da schon auf "yes" gesetzt war, da kein Passwort
    # vergeben werden konnte:
    unset -v unknown_password
  fi
}

#############################################
### Vorbereitungen

# Funktionen für NAShorn einbinden:
# shellcheck source=functions_nashorn
. /root/nashorn/scripts/functions_nashorn

# Ordner in dem die iDevice-Sicherungen landen sollen:
idevicebackup_folder="/media/dataPartition/backup/iDeviceBackup"
# Datei für das Logfile von idevicebackup (wird jedesmal überschrieben, die letzten 10 Zeilen werden immer ins nashorn.log geschrieben)
idevicebackup_log="/media/dataPartition/internal/idevicebackup.log"

# Auf Ordner für iDevice-Sicherungen prüfen, und erstellen falls nicht vorhanden:
if [[ ! -d "${idevicebackup_folder}" ]]; then
  write_to_log "Ordner für die iDevice-Sicherungen erstellen: '${idevicebackup_folder}'"
  sudo -u nashorn mkdir -p "${idevicebackup_folder}"
fi

# .user-config einlesen (wegen des Passworts):
read_user_config

# Prüfen, ob Verschlüsselung aktiv ist und in .user-config angegebene Passwörter zum angeschlossenen idevice passen:
if [[ -z ${user_config[idevicebackup_password_old]:-} ]]; then

  # altes Passwort ist leerer String, daher ist davon auszugehen, dass noch kein Sicherungspasswort vergeben ist.
  # daher wird über die oben definierte Funktion die Verschlüsselung aktiviert:
  write_to_log "kein 'idevice_password_old' in .user-config eingetragen. Aktiviere Verschlüsselung:"
  encryption_on
  if [[ ${unknown_password:-no} == "yes" ]]; then
    write_to_log "Vielleicht ist Verschlüsselung schon aktiviert mit 'idevicebackup_password_new'?"
    # den in der .user-config angegebenen Wert für idevicebackup_password_old zwischenspeichern:
    temp_old="${user_config[idevicebackup_password_old]}"
    user_config[idevicebackup_password_old]="${user_config[idevicebackup_password_new]}"
    change_pw
    # idevicebackup_password_old wieder zurücksetzen auf den in der .user-config angegebenen Wert:
    user_config[idevicebackup_password_old]="${temp_old}"
  fi

else

  # altes Passwort ist in .user-config angegeben, daher versuchen, das Passwort vom alten auf das neue zu ändern:
  write_to_log "'idevice_password_old' in .user-config vorhanden. Ändere Passwort von old auf new:"
  change_pw
  if [[ ${unknown_password:-no} == "yes" ]]; then
    if [[ ${user_config[idevicebackup_password_old]:-} != "${user_config[idevicebackup_password_new]:-}" ]]; then
      write_to_log "'idevicebackup_password_old' und 'idevicebackup_password_new' unterscheiden sich. Vielleicht passt 'idevicebackup_password_new'?"
      # den in der .user-config angegebenen Wert für idevicebackup_password_old zwischenspeichern:
      temp_old="${user_config[idevicebackup_password_old]}"
      user_config[idevicebackup_password_old]="${user_config[idevicebackup_password_new]}"
      change_pw
      # idevicebackup_password_old wieder zurücksetzen auf den in der .user-config angegebenen Wert:
      user_config[idevicebackup_password_old]="${temp_old}"
    else
      write_to_log "Probiere 'idevicebackup_password_new' nicht aus, da identisch mit 'idevicebackup_password_old'."
    fi
    if [[ ${unknown_password:-no} == "yes" ]]; then
      write_to_log "Vielleicht ist Verschlüsselung noch nicht aktivert?"
      encryption_on
    fi
  fi

fi

# Falls Passwort geändert wurde, muss das entsprechend in der .user-config eingetragen werden:
if [[ ${unknown_password:-no} == "no" ]]; then
  # Passwortänderung erfolgreich durchgeführt!
  # altes Passwort ändern auf den Wert des neuen Passworts (nur falls beide Werte ungleich):
  if [[ ${user_config[idevicebackup_password_old]:-} != "${user_config[idevicebackup_password_new]}" ]]; then
    sed -i -E "s|^idevicebackup_password_old=.*|idevicebackup_password_old=${user_config[idevicebackup_password_new]}|g" "${user_config_file}"
    write_to_log "'idevicebackup_password_old' in .user-config auf neuen Wert aktualisiert."
  fi
  # ebenso prüfen, ob neues Passwort übereinstimmt mit dem in der .user-config angegebenen (evtl. wurde Standardwert benutzt, weil Wert in .user-config ungültig):
  if ! grep "^idevicebackup_password_new=${user_config[idevicebackup_password_new]}\$" "${user_config_file}" > /dev/null; then
    sed -i -E "s|^idevicebackup_password_new=.*|idevicebackup_password_new=${user_config[idevicebackup_password_new]}|g" "${user_config_file}"
    write_to_log "idevicebackup_password_new in .user-config aktualisiert."
  fi
fi

# Es kann in jedem Fall eine valide Sicherung erstellt werden, selbst, wenn das Passwort unbekannt sein sollte.
# Allerdings muss bei unbekanntem Passwort ein Hinweis im Webfrontend angezeigt werden.

# vor dem Start der Sicherung zwei Variablen in der status.txt verändern:
#   - ob das Sicherungspasswort bekannt oder unbekannt ist
#   - Sicherungsdatum der letzten Sicherung auf 'unvollständig' setzen
read_status_txt
statustxt[idevicebackup_unknown_password]="${unknown_password:-no}"
statustxt[idevicebackup_date]="unvollständig"
write_status_txt

# Zeitstempel in idevicebackup.log schreiben (und damit bisheriges Log überschreiben und "neu anlegen"):
write_to_log "idevicebackup.log neu anlegen und in erster Zeile Zeitstempel eintragen."
date +"${date_fmt}" > "${idevicebackup_log}" 2>&1

# Meldung ins nashorn.log, dass idevicebackup2 nun aufgerufen wird:
write_to_log "idevicebackup2 aufrufen, um Sicherung des iDevice zu erstellen"

# jetzt gehts lo-hoos:
# shellcheck disable=SC2024
sudo -u nashorn idevicebackup2 backup --full "${idevicebackup_folder}" >> "${idevicebackup_log}" 2>&1

# Prüfen, ob die Sicherung erfolgreich erstellt wurde:
return_code=${?}

# in jedem Fall die letzten 10 Zeilen des idevicebackup.log ins nashorn.log schreiben:
tail "${idevicebackup_log}" >> "${logfile}"

# Nachricht ob Fehler aufgetreten sind ins logfile:
if [[ ${return_code} -eq 0 ]]; then
  write_to_log "Juhuu! Bei der Sicherung sind keine Fehler aufgetreten (Code: ${return_code})!"
else
  write_to_log "ERROR! Bei der Sicherung sind Fehler aufgetreten (Code: ${return_code})!"
fi

# Zeitstempel und Fehlerflag dieser Sicherung in $statustxt[idevicebackup_date] verewigen:
write_to_log "Zeitstempel und Fehlerflag dieser Sicherung in 'status.txt' schreiben"
read_status_txt
statustxt[idevicebackup_date]="$(date +"%d.%m.%Y, %H:%M:%S")"
statustxt[idevicebackup_return_code]="${return_code}"
write_status_txt

write_to_log "Ende iDeviceBackup."

# ByeBye
exit ${return_code}
