#!/usr/bin/env bash

#################################################################
#
# This script runs internal backups on NAShorn
#
# Usage: ./remote.sh backup_now
#
#################################################################

# for debugging:
#set -x

#set -euo pipefail


#################################################################

# Nashorn-Funktionen einbinden:
. /root/nashorn/scripts/functions_nashorn
# Arbeitsverzeichnis merken:
working_dir="$(pwd)"

_output() {
  # gibt angegebene Zeichenketten mit Datum und Skriptname auf stdout aus:
  printf "$(date +"${date_fmt}") [${0##*/}]: %s\\n" "${@}"
}

print_human_readable_date() {
  # gibt den Verzeichnisnamen eines Backups (z.B.: '2021-03-12-080021') in lesbarem Format aus (z.B.: '12.03.2021, 08:00:21')
  # Argumente: ${1} - der Verzeichnisname des Backups
  printf '%s' "${1:8:2}.${1:5:2}.${1:0:4}, ${1:11:2}:${1:13:2}:${1:15:2}"
}

_output "##################################################################################"
# dieses Skript darf nur über remote.sh aufgerufen werden, da dort das Backup-Lock gesetzt wird:
if [[ ${1:-} != "remote.sh_says_hi" ]]; then
  _output "'internalbackup.sh' ist nicht von remote.sh aufgerufen worden. Abbruch des Updates."
  exit 1
fi
_output "START internalbackup.sh"


# zuerst prüfen, ob beide Laufwerke eingehängt sind:
test=$(findmnt | grep media)
if [[ ${test} != *dataPartition* ]]; then
  error_exit "'dataPartition' ist nicht eingehängt. Abbruch internalbackup.sh."
fi
if [[ ${test} != *backup* ]]; then
  error_exit "'backup' ist nicht eingehängt. Abbruch internalbackup.sh."
fi
_output "'dataPartition' und 'backup' sind vorhanden."


# HDD1 spin up
touch /media/dataPartition/.placeholder
# wait for spin up to complete
sleep 4
# HDD2 spin up and remount writeable
_output "remounting backup-HDD read-write..."
mount -t ext4 -o remount,rw /dev/mapper/backup /media/backup || error_exit "remount (rw) der Backup-HDD fehlgeschlagen!"


# check for date and run some S.M.A.R.T selftests on certain days:
heute=$(date +%d)
if [[ ${heute} =~ ^(0|1|2)(1|2)$ ]]; then
  declare_disks
  # timestamp needed to know how long to wait, to make sure S.M.A.R.T. tests are completed before reading attributes at the end of the script:
  timestamp=$(date +%s)

  case ${heute} in

    01|11|21 )
      # SMART short selftest in background (1-10 minutes: checks the electrical and mechanical performance as well as the read performance of the disk)
      _output "invoking S.M.A.R.T short selftests on both drives."
      smartctl -t short "${disk_sata}" > /dev/null
      smartctl -t short "${disk_usb}" > /dev/null
      # for end of the script: time to wait to make sure that the test has finished (some disks might stay spun up otherwise):
      time_to_complete=180
   ;;

    02|12|22 )
      # SMART immediate offline test (couple of seconds: this test just updates SMART-attribute values and reports errors in SMART-error log)
      _output "invoking S.M.A.R.T immediate offline test on both drives."
      smartctl -t offline "${disk_sata}" > /dev/null
      smartctl -t offline "${disk_usb}" > /dev/null
      # for end of the script: time to wait to make sure that the test has finished (some disks might stay spun up otherwise):
      time_to_complete=60
    ;;

  esac

fi


# rsync flags for rsync_tmbackup.sh (NOTE: later used --with-shadow is not a rsync-flag, but a rsync_tmbackup.sh-flag!):
rsync_flags="-D --numeric-ids --links --hard-links --one-file-system --recursive --no-inc-recursive --delete"
rsync_flags="${rsync_flags} --times --group --xattrs"
rsync_flags="${rsync_flags} --info=FLIST1,MISC2,STATS2 --human-readable"
# for debugging: add logging of file-changes (as default disabled for privacy reasons):
#rsync_flags="${rsync_flags} -v --itemize-changes"


# do rsync_tmbackup.sh of folder '/media/dataPartition/data' (as user 'nashorn'):
_output "invoking rsync_tmbackup.sh for '/media/dataPartition/data' (as user 'nashorn')..."
cp -a /root/rsync-time-backup/rsync_tmbackup.sh /home/nashorn/
chown nashorn:nashorn /home/nashorn/rsync_tmbackup.sh
# we need to change to a directory user 'nashorn' has access to. Otherwise find (within rsync_tmbackup.sh) reports errors to
# not being able to restore the initial working directory (when invoked via cronjob):
# shellcheck disable=SC2164
cd /home/nashorn
sudo -u nashorn /home/nashorn/rsync_tmbackup.sh --rsync-set-flags "${rsync_flags}" --with-shadow -- /media/dataPartition/data /media/backup/archiv/Daten
rm /home/nashorn/rsync_tmbackup.sh
# shellcheck disable=SC2164
cd "${working_dir}"


# do rsync_tmbackup.sh of folder '/media/dataPartition/mysql':
_output "invoking rsync_tmbackup.sh for '/media/dataPartition/mysql'..."
# sync folder with mysql-databases to dataPartition prior to running rsync_tmbackup:
rsync -a --delete /var/lib/mysql /media/dataPartition/
# use a different strategy here, because mysql seems to need a lot of data regularly (looks like at least 150MB each daily backup):
# (explanation of '--strategy' flag: https://codeberg.org/nashorn/rsync-time-backup#user-content-backup-expiration-logic)
/root/rsync-time-backup/rsync_tmbackup.sh --rsync-set-flags "${rsync_flags}" --strategy "1:1 7:7 30:60" /media/dataPartition/mysql /media/backup/mysql


# do rsync_tmbackup.sh of folder '/media/dataPartition/nextcloud':
_output "invoking rsync_tmbackup.sh for '/media/dataPartition/nextcloud'..."
# sync Nextclouds' config.php and datafolder to the dataPartition prior to running rsync_tmbackup:
rsync -a "${www}"/nextcloud/config/config.php /media/dataPartition/nextcloud/config.php
rsync -a --delete /var/nextcloudData /media/dataPartition/nextcloud/
# add exclude pattern for unneeded '/nextcloudData/appdata_[INSTANCEID]/appstore/apps.json' which is refreshed by nextcloud every day:
/root/rsync-time-backup/rsync_tmbackup.sh --rsync-set-flags "${rsync_flags} --exclude='/nextcloudData/appdata_*/appstore/apps.json'" /media/dataPartition/nextcloud /media/backup/nextcloud


# run calcardbackup before backing up the folder containing incremental backups:
_output "creating backup of addressbooks and calendars with calcardbackup..."
mkdir -p /media/dataPartition/backup/adressen-kalender
# error handling for calcardbackup:
if ! /root/calcardbackup/calcardbackup /var/www/nextcloud -b -ltm 30 -o /media/dataPartition/backup/adressen-kalender -z >/dev/null; then
  _output "'calcardbackup' exited with an error. Continuing with backup anyway."
else
  _output "'calcardbackup' finished successfully."
fi
# Besitzrechte korrigieren:
chown -R nashorn:nashorn /media/dataPartition/backup/adressen-kalender


# change rsync_flags for backups which do not need incremental function:
# we use different flags here, because we want the more efficient new recursive algorithm and do not need --no-inc-recursive (as with rsync_tmbackup.sh)
rsync_flags="-ahHxX --delete --info=FLIST1,MISC2,STATS2"
# for debugging: add logging of file-changes (as default disabled for privacy reasons):
#rsync_flags="${rsync_flags} -v --itemize-changes"

# backup folder containing incremental backups (e.g.: Apples Timemachine backupbundles):
_output "rsyncing folder containing incremental backups (/media/dataPartition/backup/)..."
# shellcheck disable=2086
rsync ${rsync_flags} /media/dataPartition/backup/ /media/backup/archiv/Backup


# internal folder:
_output "rsyncing internal folder (/media/dataPartition/internal/)..."
# shellcheck disable=2086
rsync ${rsync_flags} /media/dataPartition/internal/ /media/backup/internal


# remount backup-HDD read-only
_output "remounting backup-HDD read-only..."
mount -t ext4 -o remount,ro /dev/mapper/backup /media/backup

# write temperature of disks to log:
write_disk_temp_to_log


###################################
# find the last complete backup and how many complete backups there are under /media/backup/data
# foldername of the last two backups:
last_complete_backup="$(find "/media/backup/archiv/Daten/" -maxdepth 1 -type d -name "????-??-??-??????" -print | sort | tail -n2)"
number_of_backups="$(find "/media/backup/archiv/Daten/" -maxdepth 1 -type d -name "????-??-??-??????" -print | wc -l)"
if [[ -f /media/backup/archiv/Daten/backup.inprogress ]]; then
  # decrease $number_of_backups by one, if a backup in progress wasn't finished:
  number_of_backups=$(( number_of_backups - 1 ))
  # the second last backup is the last complete backup:
  last_complete_backup=${last_complete_backup%%[[:space:]]*}
else
  # the last backup is the last complete backup:
  last_complete_backup=${last_complete_backup##*[[:space:]]}
fi
# create human readable date format from foldername (DD.MM.YYYY, HH:MM:SS Uhr):
date_last_complete_backup="$(print_human_readable_date "${last_complete_backup##*/}")"

oldest_backup="$(find /media/backup/archiv/Daten/ -maxdepth 1 -type d -name '????-??-??-??????' -prune | sort | head -n1)"
# check, if a backup could be found:
if [[ -n ${oldest_backup} ]]; then
  # create human readable date format from foldername (DD.MM.YYYY, HH:MM:SS Uhr):
  date_oldest_backup="$(print_human_readable_date "${oldest_backup##*/}")"
else
  # no backup was found! Set date_oldest_backup to "undefined":
  date_oldest_backup="undefined"
fi

_output "updating status.txt: number_of_backups=${number_of_backups}; date_last_complete_backup=${date_last_complete_backup}; date_oldest_backup=${date_oldest_backup} ..."
# Status-Array erneut einlesen, falls sich mittlerweile etwas geändert haben sollte:
read_status_txt

# a decrease in number of backups tells us, that backups have been deleted:
if [[ ${number_of_backups} -le ${statustxt[number_of_backups]} ]]; then
  # there were old backups deleted or none created, meaning we run out of space on the backup-Harddisk
  # change the according flag in status.txt:
  statustxt[backups_deleted]=yes
fi

# update number_of_backups in Status-Array:
statustxt[number_of_backups]=${number_of_backups}

# update date of last complete backup:
statustxt[date_last_complete_backup]=${date_last_complete_backup}

# update date of oldest backup:
statustxt[date_oldest_backup]=${date_oldest_backup}

# write status.txt:
write_status_txt

if [[ ${statustxt[number_of_backups]} -le 5 && ${statustxt[backups_deleted]} == "yes" ]]; then

  _output "creating reminder, because  number_of_backups=${number_of_backups} is less or equal 5..."

  text="vollständige Versionsstände"
  # singular, falls es nur noch einen einzigen Versionsstand im Archiv gibt:
  [[ ${statustxt[number_of_backups]} -gt 1 ]] || text="vollständiger Versionsstand"

  # Standard-Text, falls 5 oder weniger Backups vorhanden sind:
  reminder_summary="WARNUNG: Nur ${statustxt[number_of_backups]} ${text} im internen Archiv!"
  reminder_description="Das interne Archiv ist bald vollständig belegt. Vorsichtshalber solltest du Daten löschen."

  if [[ ${statustxt[number_of_backups]} -eq 2 ]]; then

    if [[ -f /media/backup/archiv/Daten/backup.inprogress ]]; then
      # Text ändern, falls nur 2 vollständige + 1 unvollständiges Backup vorhanden:
      reminder_summary="WARNUNG: internes Archiv voll!"
      reminder_description="Backup konnte nicht erstellt werden, da das interne Archiv vollständig belegt ist. Es sind nur noch ${statustxt[number_of_backups]} vollständige Versionen, sowie die unvollständige letzte vorhanden."
    else
      # Text ändern, falls nur 2 vollständige Backups vorhanden:
      reminder_summary="WARNUNG: Nur ${statustxt[number_of_backups]} vollständige Versionsstände im internen Archiv!"
      reminder_description="Das interne Archiv ist vollständig belegt. Es sind nur noch ${statustxt[number_of_backups]} Versionen vorhanden."
    fi

  fi

  # create reminder:
  "${scripts_repo}/create_vtodo.sh" "${reminder_summary}" "${reminder_description}"

fi


# check, if internalbackup took longer than the time needed to complete the S.M.A.R.T. tests.
# if not:  sleep until time_to_complete is over before reading S.M.A.R.T. attributes to make sure that the test has finished.
# (some disks might stay spun up otherwise):
if [[ $(( $(date +%s) - ${timestamp:-0} )) -lt ${time_to_complete:-0} ]]; then
  _output "sleeping until $(date --date="@$(( timestamp + time_to_complete ))" +"%H:%M:%S") (to make sure S.M.A.R.T. tests are completed before reading smart attributes)."
  sleep "$(( time_to_complete - ( $(date +%s) - timestamp ) ))"
fi


# make smartd poll the smart attributes (this needs to be done after S.M.A.R.T. tests are finished, otherwise some disks might stay spun up):
_output "forcing smartd to read S.M.A.R.T. attributes."
killall -USR1 smartd


_output "ENDE internalbackup.sh"

exit 0
