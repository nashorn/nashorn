#!/usr/bin/env bash

########################################################
#
# dieses Skript führt diverse Aufgaben bei reboot von NAShorn aus:
#   - erstellt CA-Zertifikat, falls nicht vorhanden (erster Start beim Kunden, oder Aufruf mit Argument 'renew_rootcert')
#   - löscht die SSH-Schlüssel und erstellt sie neu
#   - fügt den FQDN dem Webserver-Zertifikat und Nextclouds trusted_domains hinzu
#   - startet syslog_watcher.sh (erstellt Erinnerungen bei (warn|error|fail) im syslog)
#
# Argumente:
#    ${1} - `renew_rootcert`: um NAShorn-Root-Zertifikat (und Webserver-Zertifikat) ohne Neustart erneuern zu können.
#                             (in dem Fall werden die SSH-Schlüssel nicht neu erstellt und syslog_watcher wird nicht gestartet)
#
# v0.6 (c) 2020 Freirechner GbR
#
########################################################

#set -euo pipefail

# Funktionen einbinden, falls noch nicht geschehen:
[[ -n ${logfile:-} ]] || . /root/nashorn/scripts/functions_nashorn

# Logeintrag ausgeben, warum dieses Skript aufgerufen wurde:
if [[ ${1:-} == "renew_rootcert" ]]; then
  write_to_log "Argument 'renew_rootcert' wurde übergeben: NAShorn-Zertifikat und zugehörigen Schlüssel löschen..."
  # Root-Zertifikat und zugehörigen Schlüssel löschen, damit es neu erstellt wird:
  rm -rf "${ca}/NAShorn-Zertifikat.pem" "${ca}/cakey.pem"
else
  write_to_log "Start 'reboot_jobs.sh' - Neustart von NAShorn."
fi
# IPv6 deaktivieren:
write_to_log "IPv6 auf allen Interfaces deaktivieren:"
sysctl -w net.ipv6.conf.all.disable_ipv6=1

##########################################
# Prüfen, ob Root-Zertifikat der CA vorhanden. Falls nicht, muss es erstellt  werden (das ist auch der Fall beim ersten Start beim Kunden):
[[ -f "${ca}/NAShorn-Zertifikat.pem" && -f "${ca}/cakey.pem" ]] || {
  # Logeintrag ausgeben, falls dies der erste Start beim Kunden ist:
  [[ ${1:-} == "renew_rootcert" ]] || write_to_log "Erster Start beim Kunden (NAShorn-Zertifikat und zugehöriger Schlüssel nicht im CA-Ordner von nginx)"

  ########################################
  ## CA erstellen:
  write_to_log "CA erstellen:"
  # Root-Zertifikat der CA erstellen (20 Jahre Laufzeit):
  openssl req -x509 -config "${ca}/openssl-ca.cnf" -sha256 -nodes -out "${ca}/NAShorn-Zertifikat.pem" -outform PEM -days 7305
  # Das gerade erstellte Root-Zertifikat in ein für Android kompatibles Format umwandeln:
  openssl x509 -inform PEM -outform DER -in "${ca}/NAShorn-Zertifikat.pem" -out "${ca}/NAShorn-Zertifikat.crt"
  # Zugriffsrechte einschränken:
  chmod 400 "${ca}/"*
  chmod 100 "${ca}"

  # Variable setzen, dass Root-Zertifikat erneuert wurde
  # (wird später übergeban an 'renew_ssl_server-cert.sh' oder 'daily_jobs.sh', um zu signalisieren, dass Webserverzertifikat neu erstellt werden muss):
  rootcert_renewed="rootcert_renewed"

  # Root-Zertifikat der CA dem Truststore dieser Installation hinzufügen (Format muss pem sein, Dateiname muss aber auf crt aufhören):
  write_to_log "Root-Zertifikat der CA verlinken und dem lokalen Truststore hinzufügen..."
  ln -sf "${ca}/NAShorn-Zertifikat.pem" "/usr/local/share/ca-certificates/NAShorn-Zertifikat.pem.crt"
  update-ca-certificates

  # Root-Zertifikat der CA nach /var/www/ kopieren.
  write_to_log "Root-Zertifikat ins Webroot kopieren (/var/www/)..."
  cp "${ca}/NAShorn-Zertifikat."{pem,crt} /var/www/
  # Leserechte für alle einräumen (um Manipulation zu verhindern, Besitzrechte NICHT auf www-data übertragen):
  chmod 644 /var/www/NAShorn-Zertifikat.{pem,crt}

  # warten, bis dataPartition eingehängt ist:
  wait_for_mount "/media/dataPartition"
  # Root-Zertifikat und zugehöriger Schlüssel der CA ins interne Verzeichnis /media/dataPartition/internal/ssl/ kopieren.
  write_to_log "Root-Zertifikat und Schlüssel in den internen Ordner von 'dataPartition' kopieren..."
  cp "${ca}/NAShorn-Zertifikat."{pem,crt} "${ca}/cakey.pem" /media/dataPartition/internal/ssl/
  # Rechte einschränken:
  chmod 400 /media/dataPartition/internal/ssl/*
  chown -R root:root /media/dataPartition/internal/ssl

  # Root-Zertifikat der CA in Samba-Share der dataPartition kopieren:
  copy_root_certificate_to_datafolder

  # Falls über `remote.sh renew_rootcert` aufgerufen, muss jetzt nur noch das Webserver-Zertifikat erneuert werden:
  if [[ ${1:-} == "renew_rootcert" ]]; then
    ## Das Webserer-Zertifikat neu erstellen:
    write_to_log "renew_ssl_server-cert.sh aufrufen, um Webserverzertifikat neu zu erstellen:"
    "${scripts_repo}/renew_ssl_server-cert.sh" "${rootcert_renewed:-}"
    # die Trusted-Domains von Nextcloud prüfen und aktualisieren, falls sich etwas geändert hat:
    write_to_log "Nextclouds Trusted Domains prüfen und ggfls. aktualisieren:"
    add_trusted_domains_to_nextcloud
    # Das war alles für `renew_rootcert`:
    write_to_log "Ende 'renew_rootcert': Root-Zertifikat und Webserver-Zertifikat wurden neu erstellt."
    exit 0
  fi


  ########################################
  ## SSH-Schlüssel löschen und neu erzeugen:
  write_to_log "Erster Start beim Kunden: SSH-Schlüssel löschen und neu erzeugen:"
  rm /etc/ssh/ssh_host_*
  dpkg-reconfigure --frontend=noninteractive openssh-server


  # Ende erster Start beim Kunden
  write_to_log "Erster Start beim Kunden: ENDE"
}


##########################################
###### was folgt, wird bei jedem Reboot geprüft/ausgeführt:

# Alle Locks entfernen:
write_to_log "Alle Locks entfernen:"
remove_locks

# reboot_required auf "no" setzen (da ja gerade ein Neustart durchgeführt wurde):
write_to_log "Status-Variable 'reboot_required' zurücksetzen..."
read_status_txt
statustxt[reboot_required]="no"
write_status_txt

# dnscrypt-proxy aktivieren, falls deaktiviert (wird deaktiviert in deliveready.sh):
if [[ $(systemctl is-enabled dnscrypt-proxy.service) != "enabled" ]]; then
  # dies erfolgt bei jedem Reboot, falls Kunde Stecker zu früh zieht, oder bei erstem Boot Stromausfall oder ähnliches):
  write_to_log "dnscrypt-proxy.service aktivieren:"
  systemctl enable dnscrypt-proxy.service
  # Notiz: der Dienst wird dann über Aufruf von 'check_dns_configuration' in 'daily_jobs.sh' neu gestartet werden.
fi

# warten, bis dataPartition eingehängt ist:
wait_for_mount "/media/dataPartition"

# syslog_watcher.sh aufrufen und im Hintergrund ausführen
"${scripts_repo}"/syslog_watcher.sh >> "${logfile}" 2>&1 &
write_to_log "syslog_watcher.sh gestartet."

# etwas warten, bis nach Reboot alle Dienste gestartet sind:
sleep 30

# Sicherstellen, dass /var/log/journal als Ordner (oder symlink auf einen Ordner) existiert.
# Dies erfolgt erst nach dem 30-Sekunden-Schlaf, damit der Kopiervorgang /var/log.hdd -> /var/log von armbian-ramlog abgeschlossen ist:
if [[ ! -d "/var/log/journal" ]]; then
  if [[ -f "/var/log/journal" ]]; then
    write_to_log "WARNING: '/var/log/journal' ist eine Datei. Wird gelöscht, um Ordner zu erstellen..."
    rm "/var/log/journal" # in case this was for whatever reason a file, it needs to be removed before creating the folder
  fi
  write_to_log "Erstelle Ordner '/var/log/journal'..."
  mkdir "/var/log/journal"
  chown -R root:systemd-journal "/var/log/journal"
fi

write_to_log "ENDE 'reboot_jobs.sh' und Aufruf von 'daily_jobs.sh'."

## im Hintergrund daily_jobs.sh aufrufen (eine Sekunden vorher schlafen, damit dieses Skript hier vorher beendet werden kann):
{ sleep 1; "${scripts_repo}/daily_jobs.sh" "${rootcert_renewed:-}" ; } &

exit 0
