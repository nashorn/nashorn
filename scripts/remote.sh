#!/usr/bin/env bash

# zum debuggen:
#set -euo pipefail

# dieses Skript außerhalb des Webroots platzieren (z.B. unter /usr/local/bin/nashorn/) und:
#   - chown www-data:www-data
#   - chmod 100
#   - außerdem muss diesem Skript sudo-Recht für www-data eingeräumt werden (z.B. mit visudo).

# Bei Bedarf Priorität dieses Skripts erhöhen ($$ liefert die pid des laufenden Skriptes)
# vermutlich nicht notwendig für NAShorn:
#renice -n -15 -p $$ > /dev/null 2>&1

# Funktionen einbinden und logfile erstellen:
. /root/nashorn/scripts/functions_nashorn
create_nashorn_logfile

# Variablen aus /var/www/status.txt einlesen:
read_status_txt

# übergebene Werte prüfen und entsprechende Aktion ausführen:
case "${1:-}" in

    autoupdatecheck)

        # testen, ob zweiter übergebener Wert gültig ist:
        regex='^(disable|enable|toggle)$'
        if [[ ${2:-} =~ ${regex} ]]; then

          case ${2} in

            disable)
              statustxt[autoupdatecheck]="off"
              ausgabe="Automatische Updates deaktiviert."
            ;;

            enable)
              statustxt[autoupdatecheck]="on"
              ausgabe="Automatische Updates aktiviert."
            ;;

            toggle)
              if [[ ${statustxt[autoupdatecheck]} == "off" ]]; then
                # autoupdatecheck aktivieren:
                statustxt[autoupdatecheck]="on"
                ausgabe="Automatische Updates aktiviert."
              elif [[ ${statustxt[autoupdatecheck]} == "on" ]]; then
                # autoupdatecheck deaktivieren:
                statustxt[autoupdatecheck]="off"
                ausgabe="Automatische Updates deaktiviert."
              else
                ausgabe="Toggle autoupdatecheck nicht möglich, da statustxt[autoupdatecheck] weder 'off' noch 'on', sondern '${statustxt[autoupdatecheck]}'."
              fi
            ;;

          esac

          # geänderten Status in status.txt abspeichern:
          write_status_txt

        else
          ausgabe="ungültiger Wert für autoupdatecheck übergeben: '${2:-}'"
        fi

        write_to_log "${ausgabe}"
    ;;

    backup_hour)
        # testen, ob zweiter übergebener Wert gültig ist (00-23):
        regex='^([01][0-9]|2[0-3])$'
        if [[ ${2:-} =~ ${regex} ]]; then
            # /etc/cron.d/nashorn_internalbackup mit entsprechender Uhrzeit neu schreiben:
            set_internalbackup_hour "${2}" >> "${logfile}" 2>&1
        fi
    ;;

    backup_now)
        # jetzt ein Backup auf die 2. HDD erstellen (# Prüfung auf Lock in status.txt erfolgt in internalbackup.sh):

        # prüfen von wo aus dieses Backup aufgerufen wurde:
        if [[ ${2:-} =~ ^(Cronjob|Webfrontend)$ ]]; then
          # 2. Parameter ist korrekt übergeben. Diesen der Variablen $invoked_by zuweisen:
          invoked_by="${2}"
        else
          # 2. Parameter unkorrekt oder leer. Vermutlich vom Terminal aus aufgerufen:
          invoked_by="Terminal"
        fi

        # Logausgabe, von wo aus 'backup_now' aufgerufen wurde:
        write_to_log "'backup_now' von '${invoked_by}' aufgerufen."

        # kein Backup während Update:
        if [[ ${statustxt['update_in_progress']} == "yes" ]]; then
            write_to_log "Update-Lock ist gesetzt. Backup nicht möglich solange Update läuft. Abbruch des Backups."
            exit 1
        fi
        # Backup-Lock setzen, um weitere Backups zu verhindern:
        lock_status "Backup" || { write_to_log "Backup-Lock konnte nicht gesetzt werden. Abbruch des Backups."; exit 1; }

        ${scripts_repo}/internalbackup.sh "remote.sh_says_hi" "${invoked_by}" >> "${logfile_internalbackup}" 2>&1

        # Backup-Lock wieder entfernen, da Backup beendet:
        unlock_status "Backup" || write_to_log "Notice: Backup-Lock konnte nicht entfernt werden."
        write_to_log "'internes Backup jetzt' beendet."

        # status.txt erneut einlesen, falls die Einstellung für automatische Updates während des Backups geändert wurde:
        read_status_txt
        # automatischen check für Updates und eventuelles Update nur dann nach internalbackup.sh aufrufen, wenn:
        #    - diese Funktion über den internalbackup-Cronjob aufgerufen wurde
        #    - automatische Updates im Webinterface aktiviert sind
        if [[ ${invoked_by} == "Cronjob" && ${statustxt[autoupdatecheck]} == "on" ]]; then
          # Update im Hintergrund ausführen, damit der aktuelle Aufruf dieses Skripts beendet wird
          # (Parameter "AutoUpdate" als zweiten Parameter übergeben, damit das Updateskript nur aufgerufen wird, wenn
          #  im Repo eine aktuellere Version gefunden wurde):
          ${scripts_repo}/remote.sh "update" "AutoUpdate" >> "${logfile}" 2>&1 &
        fi
    ;;

    change_hostname)
        # ändert den Hostnamen des Systems zu dem im Parameter angegebenen. Gültigkeit des Parameters (Hostnames) wird
        # hier nicht überprüft, um dieses Script schlank zu halten. Überpüfung erfolgt in remote.php und in der von hier
        # aufgerufenen Funktion change_hostname().

        # Logausgabe, von wo aus 'change_hostname' aufgerufen wurde:
        write_to_log "'change_hostname' von '${3:-Terminal}' aufgerufen."
        change_hostname "${2:-}"
    ;;

    change_to_local_nameserver)
        # ändert /etc/resolv.conf und dnsmasq-Konfiguration so, dass der lokale Nameserver statt dnscrypt-proxy benutzt wird
        # verschlüsseltes DNS mit dnscrypt-proxy lässt sich über run_all_updates wieder aktivieren.

        change_to_local_nameserver
    ;;

    clear_vtodolist)
        # löscht alle Erinnerungen aus der Erinnerungsliste 'Meldung' des Nextcloud-system-users
        ${scripts_repo}/create_vtodo.sh --clear-vtodolist >> "${logfile}" 2>&1
    ;;

    date)
        # stellt das Datum auf den übergebenen Wert ein.
        # Datum muss als zweites Argument in der folgenden Form übergeben werden: TT.MM.JJJJ

        # regex, um Datumsformat zu überprüfen:
        date_regex='^(3[01]|[12][0-9]|0[1-9])\.(1[012]|0[1-9])\.[0-9]{4}$'

        if [[ -z ${2:-} ]]; then
            # es wurde kein 2. Argument übergeben
            # (kann nicht passieren, wenn über remote.php aufgerufen)
            write_to_log "es wurde kein Datum als 2. Argument übergeben: '${2:-}'"
            exit 1
        elif [[ ! ${2:-} =~ ${date_regex} ]]; then
            # das übergebene 2. Argument stellt kein gültiges Datum in der Form TT.MM.JJJJ dar:
            # (kann nicht passieren, wenn über remote.php aufgerufen, da dort auch schon auf gültiges Format überprüft wurde)
            write_to_log "es wurde ein ungültiges Datum übergeben: '${2:-}'"
            exit 2
        else

            # das Datum einstellen:
            if date -s "${2:6:4}-${2:3:2}-${2:0:2}" +"%d.%m.%Y"; then
                write_to_log "Datum eingestellt auf ${2}"
                reload_services chrony
            else
                # date -s hat Fehlermeldung zurückgegeben
                # (nur dieser Fehler kann auftreten, wenn über remote.php aufgerufen)
                write_to_log "ERROR: Datum konnte nicht gesetzt werden."
                exit 3
            fi
        fi
    ;;

    deliveready)
        # ruft deliveready.sh auf, das Spuren eines Testboot beseitigt.
        printf '%s\n' "++  [${0##*/}]: Aufruf von 'deliveready.sh'..."
        "${scripts_repo}"/deliveready.sh "remote.sh_says_hi" >> "${logfile}" 2>&1 || errorcode=${?}

        # Auf Fehler überprüfen:
        if [[ ${errorcode:-0} -eq 0 ]]; then
            printf '%s\n' "++  [${0##*/}]: ERFOLG: deliveready.sh wurde erfolgreich abgearbeitet!"
        else
            printf '%s\n' "++  [${0##*/}]: FEHLER: deliveready.sh hat Fehlercode '${errorcode}' zurückgegeben!"
        fi
    ;;

    fix_permissions)
        # Korrigiert die Zugriffsrechte im Samba-Share (Datei 644, Directory 755) für einen Pfad, der als 2.Argument übergeben werden muss.
        # Wird aufgerufen, wenn der Zugriff auf eine Datei in /media/dataPartition/data über den Browser einen 403 erzeugt.

        errorcode="0"

        # nur Ausführen, falls 2. Argument übergeben wurde:
        if [[ -n "${2:-}" ]]; then

            # den absoluten Pfad zu Datei/Verzeichnis erstellen:
            pfad="/media/dataPartition/data/${2#/Daten/}"

            if [[ -f "${pfad}" ]]; then
                # 2. Argument ist Datei -> Zugriffsrechte 644 ändern:
                chmod 644 "${pfad}" || errorcode="1"
                type="eine Datei"
                # prüfen, ob die Datei ein Root-Zertifikat ist:
                regex='^NAShorn-Zertifikat\.(crt|pem)$'
                if [[ ${2#/Daten/} =~ ${regex} ]]; then
                  # für die root-Zertifikate "root" als Eigentümer zuweisen:
                  chown root:root "${pfad}"
                else
                  # für alle anderen Dateien "nashorn" als Eigentümer zuweisen:
                  chown nashorn:nashorn "${pfad}"
                fi
            elif [[ -d "${pfad}" ]]; then
                # 2. Argument ist Verzeichnis -> Zugriffsrechte 755 ändern:
                chmod 755 "${pfad}" || errorcode="1"
                type="ein Verzeichnis"
                chown nashorn:nashorn "${pfad}"
            else
                # übergebener Pfad ist weder Datei noch Verzeichnis - Fehlercode setzen:
                errorcode="1"
            fi

        else

            # kein Pfad als 2. Argument übergeben, daher errorcode auf 1 setzen:
            write_to_log "fix_permissions: kein Pfad übergeben!."
            errorcode="1"

        fi

        # Nachricht ins Log, aber nur falls ${type} nicht leer und errorcode==0 ist:
        if [[ -n ${type:-} && ${errorcode:-0} == "0" ]]; then
            write_to_log "fix_permissions: Zugriffsrechte für ${type} korrigiert."
        elif [[ -n ${type:-} && ${errorcode:-0} == "1" ]]; then
            write_to_log "fix_permissions: Zugriffsrechte für ${type} konnten nicht korrigiert werden."
        elif [[ -z ${type:-} && ${errorcode:-0} == "1" ]]; then
            write_to_log "fix_permissions: übergebener Pfad ist keine Datei und auch kein Verzeichnis."
        fi

        # Script beenden und Fehlercode zurückgeben (Fehlercode wird in  remote.php überprüft!)
        exit ${errorcode}
    ;;

    idevicebackup)
        # Backup eines über USB angeschlossenen iDevice:
        write_to_log "idevicebackup wurde aufgerufen."

        # iDeviceBackup-Lock setzen, um doppelten Aufruf zu verhindern:
        lock_status "iDeviceBackup" || { write_to_log "iDeviceBackup-Lock konnte nicht gesetzt werden oder ist bereits gesetzt! Abbruch!"; exit 1; }

        write_to_log "Aufruf von 'idevicebackup_wrapper.sh':"
        ${scripts_repo}/idevicebackup_wrapper.sh "remote.sh_says_hi" >> "${logfile}" 2>&1

        unlock_status "iDeviceBackup" || write_to_log "Notice: iDeviceBackup-Lock konnte nicht entfernt werden."
    ;;

    poweroff)
        # NAShorn ausschalten:
        write_to_log "'NAShorn herunterfahren' über Webfrontend aufgerufen."

        # kein Poweroff während Update:
        if [[ ${statustxt['update_in_progress']} == "yes" ]]; then
            write_to_log "Update-Lock ist gesetzt. Poweroff nicht möglich solange Update läuft. Poweroff wird nicht aufgerufen."
            exit 1
        fi
        # Poweroff-Lock setzen, um weitere Poweroffs zu verhindern:
        lock_status "Poweroff" || write_to_log "Poweroff-Lock konnte nicht gesetzt werden, aber Poweroff wird trotzdem ausgeführt."
        # 5 Sekunden warten vor Poweroff, damit Javascript die Änderung einlesen kann:
        sleep 5
        poweroff >> "${logfile}" 2>&1
    ;;

    reboot)
        # NAShorn neu starten:
        write_to_log "'NAShorn neu starten' über Webfrontend aufgerufen."
        reboot >> "${logfile}" 2>&1
    ;;

    remove_locks)
        # entfernt alle Locks aus der Statusdatei /var/www/status.txt:
        write_to_log "Aufruf von 'remote.php?remove_locks'. Alle Locks werden aus status.txt entfernt"
        # alle Locks entfernen:
        remove_locks
    ;;

    renew_rootcert)
        # NAShorn-Root-Zertifikat (und auch Webserver-Zertifikat) neu erstellen:
        write_to_log "Aufruf von 'remote.php?renew_rootcert'. NAShorn-Root-Zertifikat wird über 'reboot_jobs.sh' gelöscht und neu erstellt:"
        ${scripts_repo}/reboot_jobs.sh "renew_rootcert" >> "${logfile}" 2>&1
    ;;

    run_all_updates)
      # setzt NAShorn zurück, indem alle Update-Routinen ausgeführt werden:
      write_to_log "'run_all_updates' über Webfrontend aufgerufen." \
                   "Setze 'updated_until' zurück auf den Default Wert, damit alle Update-Funktionen ausgeführt werden."
      # $statustxt[updated_until] löschen und damit auf den Default-Wert zurücksetzen (default wird in 'read_status_txt()' deklariert):
      read_status_txt
      unset -v "statustxt[updated_until]"
      write_status_txt
      # Update im Hintergrund ausführen, damit der aktuelle Aufruf dieses Skripts beendet wird:
      write_to_log "'updated_until' wurde zurückgesetzt. Rufe nun 'remote.sh update Webfrontend' auf:"
      ${scripts_repo}/remote.sh "update" "Webfrontend" >> "${logfile}" 2>&1 &
    ;;

    restart_itunes_server)
        # owntone neu starten:
        write_to_log "'iTunes-Server neustarten' über Webfrontend aufgerufen."
        reload_services owntone
    ;;

    update)
        # aktualisiert die NAShorn-Software:

        # prüfen von wo aus dieses Update aufgerufen wurde:
        if [[ ${2:-} =~ ^(AutoUpdate|Cronjob|Webfrontend)$ ]]; then
          # 2. Parameter ist korrekt übergeben. Diesen der Variablen $invoked_by zuweisen:
          invoked_by="${2}"
        else
          # 2. Parameter unkorrekt oder leer. Vermutlich vom Terminal aus aufgerufen:
          invoked_by="Terminal"
        fi

        # Logausgabe, von wo aus 'remote.sh update' aufgerufen wurde:
        write_to_log "'NAShorn-Update' von '${invoked_by}' aufgerufen."

        # Wenn das Update über nashorn_update cronjob aufgerufen wurde, ist das Update-Lock vom vorherigen Aufruf über Webfrontend noch gesetzt.
        # In allen anderen Fällen muss das Update-Lock gesetzt werden, um weitere Update-Aufrufe zu verhindern (Abbruch, falls das Lock schon gesetzt ist):
        if [[ ${invoked_by} != "Cronjob" ]]; then
          lock_status "Update" || { write_to_log "Update-Lock konnte nicht gesetzt werden. Abbruch des Updates."; exit 1; }
        fi

        # Zeitstempel der letzten Suche nach Aktualisierungen speichern.
        # Der Name der folgenden Variable ist etwas irreführend: 'date_last_update' enthält den Zeitstempel der letzten Suche(!)
        # nach einer Aktualisierung (nicht der letzten Aktualisierung). Bis einschließlich Version 1.8.1 war der Name zutreffend.
        # Aus Einfachheitsgründen bleibt der Variablenname trotz irreführendem Namen ab v1.9.0 so bestehen:
        read_status_txt
        statustxt[date_last_update]="$(date +"%d.%m.%Y, %H:%M:%S")"
        write_status_txt

        # auf Update der NAShorn-Software prüfen und Update nur dann ausführen, falls check_for_update keinen Fehler zurückgibt.
        # (kein Fehler bedeutet: update available und repo '${user_config[url_repo_nashorn]}' integrity verified):
        if ${scripts_repo}/check_for_update.sh >> "${logfile}" 2>&1; then

          # User-Konfiguration einlesen (für branchname):
          read_user_config

          # Repo-Ordner ist vorhanden: aktuelle Version pullen oder bei Fehler das Repo clonen:
          write_to_log "Nashorn Repository aktualisieren... "

          # Zu der in der User-Konfiguration angegebenen Branch wechseln und das Repo aktualisieren:
          checkout_and_pull_repo "nashorn" "${user_config[branch]}"

          # das aktualisierte NAShorn-Update-Skript aufrufen:
          write_to_log "Update Skript aufrufen."
          ${scripts_repo}/update_nashorn.sh "remote.sh_says_hi" "${invoked_by}" >> "${logfile}" 2>&1

        elif [[ ${invoked_by} != "AutoUpdate" ]]; then

          # Update wurde manuell angestoßen (über Webfrontend (Cronjob) oder Terminal) und im Repo wurde keine neue Version gefunden.
          # In diesen Fällen soll ohne Branchwechsel(!) die installierte Version des Update-Skripts ausgeführt werden:
          write_to_log "Update Skript aufrufen."
          ${scripts_repo}/update_nashorn.sh "remote.sh_says_hi" "${invoked_by}" >> "${logfile}" 2>&1

        else

          # Der Update-Aufruf erfolgte über Autoupdatecheck und im Repo wurde keine neue Version gefunden.
          # In diesem Fall sollen nur unattended-upgrades ausgeführt werden:
          run_unattended_upgrade >> "${logfile}" 2>&1

          # ebenfalls soll yt-dlp gegebenenfalls aktualisiert werden:
          update_yt-dlp >> "${logfile}" 2>&1

        fi

        # Nur wenn nicht über Webfrontend aufgerufen: Update-Lock entfernen, da Update beendet ist
        if [[ ${invoked_by} == "Webfrontend" ]]; then
          write_to_log "Aufruf über ${invoked_by}: Update-Lock bleibt gesetzt, da gleich Cronjob 'nashorn_update' gestartet wird."
        else
          unlock_status "Update" || write_to_log "Notice: Update-Lock konnte nicht entfernt werden."
        fi
    ;;

    youtube-dl)
        # ruft das Skript 'video_downloader.sh' auf, um ein Video mit Hilfe von yt-dlp herunterzuladen
        # als ${2} wird von remote.php:
        #      - entweder ein Link übergeben, der in video_downloader.sh an yt-dlp als Videolink weitergereicht wird
        #      - oder 'videolink_history_delete' übergeben - dann wird video_downloader.sh die History löschen
        # als ${3} wird von remote.php "Audio" übergeben, falls nur Audio heruntergeladen werden soll

        ${scripts_repo}/video_downloader.sh "${2:-}" "${3:-Video}" >> "${logfile}" 2>&1
    ;;

    zip_logfiles)
        # erstellt ein ZIP-Archiv mit den Nashorn-Logfiles in /var/www/
        # (als ${2} wird von remote.php eine Datums-Extension angegeben, die an den Dateinamen angehängt wird)

        write_to_log "'Abruf der Log-Dateien' über Webfrontend aufgerufen."

        # eventuell in vorherigem Aufruf erstellte Dateien löschen:
        rm -rf "/var/tmp/nashorn-logfiles_"*

        write_to_log "'/var/log.hdd/nashorn' und '/var/log/nashorn' zusammenführen..."
        # Logfiles von /var/log.hdd/nashorn (SD-Karte) in temp-Ordner rsyncen:
        rsync -a "/var/log.hdd/nashorn/" "/var/tmp/nashorn-logfiles_${2:-}" >> "${logfile}" 2>&1
        # Logfiles von /var/log/nashorn (ZRAM) in temp-Ordner rsyncen:
        rsync -a "/var/log/nashorn/" "/var/tmp/nashorn-logfiles_${2:-}" >> "${logfile}" 2>&1

        # syslog, syslog.1, kern.log, kern.log.1 hinzufügen:
        write_to_log "syslog(.1) und kern.log(.1) hinzufügen..."
        cp -a /var/log/{syslog,kern.log} /var/log.hdd/{syslog,kern.log}.1 "/var/tmp/nashorn-logfiles_${2:-}"

        # idevicebackup.log hinzufügen, falls vorhanden (nur vorhanden, wenn schon ein idevice gesichert worden ist):
        idevicebackup_log="/media/dataPartition/internal/idevicebackup.log"
        if [[ -f "${idevicebackup_log}" ]]; then
          write_to_log "${idevicebackup_log} hinzufügen..."
          cp -a "${idevicebackup_log}" "/var/tmp/nashorn-logfiles_${2:-}"
        else
          write_to_log "noch kein idevice-Backup erstellt, daher kein idevicebackup.log."
        fi

        # eventuell vorhandene alte Logfile-Archive aus dem Webroot-Ordner löschen:
        rm -rf "/var/www/nashorn-logfiles_"*

        # Ordner mit Logfiles zippen:
        write_to_log "Creating ZIP-Archive 'nashorn-logfiles_${2:-}.zip'..."
        if cd "/var/tmp"; then
          zip -rq "nashorn-logfiles_${2:-}.zip" "nashorn-logfiles_${2:-}" >> "${logfile}" 2>&1
          # gezippte Logfiles nach /var/www kopieren:
          mv "/var/tmp/nashorn-logfiles_${2:-}.zip" /var/www/
          # temporäre Dateien wieder löschen:
          rm -rf "/var/tmp/nashorn-logfiles_"*
          write_to_log "SUCCESS: 'nashorn-logfiles_${2:-}.zip' successfully created and stored in webroot.."
        else
          write_to_log "ERROR: Could not change to temp folder '/var/tmp' for zipping logfiles!"
        fi
    ;;

    *)
        # einfach gar nix machen, falls unbekanntes Argument übergeben wurde:
        : # der Doppelpunkt bedeutet TRUE und muss stehen bleiben!
    ;;

esac

exit 0
