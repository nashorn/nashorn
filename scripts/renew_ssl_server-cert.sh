#!/usr/bin/env bash

########################################################
#
# dieses Skript erneuert das Webserverzertifikat, falls
# es in weniger als einem Monat abläuft.
#
# Argumente:
#     - ${1}: wenn gesetzt auf "rootcert_renewed" oder "--force" wird ein Erneuern des Webserverzertifikates erzwungen.
#             - "rootcert_renewed" wird von 'reboot_jobs' benutzt beim erstem Bootvorgang beim Kunden und bei 'remote.php?renew_rootcert'
#             - "--force" wird bei Upgrade auf Debian Bullseye benutzt
#
# (c) 2020 Freirechner GbR
#
########################################################

# zum debuggen:
#set -euo pipefail

# NAShorn Funktionen einbinden und Nameserver, IP und FQDN ermitteln
. "/root/nashorn/scripts/functions_nashorn"
declare_ip_fqdn

write_to_log "überprüfen, ob Webserver-Zertifikat erneuert werden muss:"

# Gültigkeit des Zertifikats in unix epoch Zeit ermitteln:
cert_valid=$(openssl x509 -in "${ca}/servercert.pem" -noout -enddate 2>/dev/null || : )
cert_valid=$(date +"%s" -d "${cert_valid#*=}" 2>/dev/null || date +"%s")

# Timestamp "in einem Monat" in unix epoch Zeit:
next_month=$(date +"%s" -d "next month")

# das Webserver-Zertifikat könnte aus verschiedenen Gründen neu erstellt werden müssen. Die diversen Gründe werden im folgenden geprüft:
if [[ ${cert_valid} -lt ${next_month} ]]; then
  # Webserver-Zertifikat läuft in weniger als 1 Monat ab:
  write_to_log "Webserver-Zertifikat läuft in weniger als einem Monat ab."
  renew_certificate=1
fi

# die subjectAltNames aus dem Zertifikat auslesen:
cert_subjectaltnames=$(openssl x509 -in "${ca}/servercert.pem" -noout -ext subjectAltName 2>/dev/null | tail -n1 || echo " ")

for domain in ${fqdn}; do
  if [[ ${cert_subjectaltnames} != *[[:space:]]DNS:${domain},* ]]; then
    # der FQDN ist nicht in den subjectAltNames im Zertifikat enthalten:
    write_to_log "der FQDN '${domain}' ist nicht in den subjectAltNames im Zertifikat enthalten."
    renew_certificate=1
  fi
done

if [[ -n ${ip:-} ]]; then
  if [[ ${cert_subjectaltnames} != *[[:space:]]DNS:${ip},* ]]; then
    # die IP ist nicht in den subjectAltNames im Zertifikat enthalten:
    write_to_log "die IP '${ip}' ist nicht in den subjectAltNames im Zertifikat enthalten."
    renew_certificate=1
  fi
else
  # shellcheck disable=SC2016
  write_to_log 'Variable ${ip} ist leer oder nicht deklariert.'
fi

case ${1:-} in
  "rootcert_renewed" )
    # das Root-Zertifikat wurde soeben erneuert, daher MUSS nun auch das Webserver-Zertifikat neu erstellt werden:
    write_to_log "das Root-Zertifikat wurde erneuert."
    renew_certificate=1
  ;;
  "--force" )
    # erzwungene Erneuerung des Webserver-Zertifikates über Option '--force':
    write_to_log "Option '--force' erkannt."
    renew_certificate=1
  ;;
  ?* )
    # unbekannte Option übergeben --> wird ignoriert:
    write_to_log "Ignoriere unbekannte Option '${1:-}'."
  ;;
esac

if ! openssl verify -x509_strict "${ca}/servercert.pem" >/dev/null; then
  # openssl hat einen Fehler erzeugt beim Verifizieren des Zertifikats:
  write_to_log "openssl verify stuft das Webserver-Zertifikat als ungültig ein."
  renew_certificate=1
fi

# Überprüfung abgeschlossen. Falls die Variable ${renew_certificate} gesetzt ist, gibt es mindestens einen Grund das Webserver-Zertifikat zu erneuern:
if [[ -n ${renew_certificate:-} ]]; then

  # Nachricht ins Log schreiben, dass Zertifikat erneuert wird:
  write_to_log "Zertifikat wird erneuert."

  # openssl-server.cnf aktualisieren mit aktuellen FQDN(s) und IP:
  add_ip_fqdn_to_openssl_server_cnf

  # CSR erstellen:
  write_to_log "Webserver-CSR erstellen..."
  openssl req -config "${ca}/openssl-server.cnf" -newkey rsa:4096 -sha256 -nodes -out "${ca}/servercert.csr" -outform PEM
  # CSR von der CA signieren lassen:
  write_to_log "Webserver-CSR von der CA signieren lassen..."
  openssl ca -batch -create_serial -config "${ca}/openssl-ca.cnf" -policy policy_anything -extensions usr_cert -out "${ca}/servercert.pem" -infiles "${ca}/servercert.csr"

  # Symlinks für IP und alle FQDNs auf Webserver-Zertifikat und Schlüssel in '/etc/cups/ssl/' erstellen:
  create_cups_cert_symlinks

  # eigene Domains und IP als 'server_name' in nginx Konfiguration verwenden:
  add_ip_fqdn_to_nashorn_local

  # nginx das neue Zertifikat einlesen lassen:
  write_to_log "nginx anweisen, die Konfiguration und damit das Zertifikat neu einzulesen..."
  nginx -s reload

else

  # Zertifikat ist noch länger als einen Monat gültig:
  write_to_log "Zertifikat ist noch länger als einen Monat gültig und FQDN und IP sind in subjectAltNames enthalten."

fi

# Damit sind wir hiermit durch:
write_to_log "Ende renew_ssl_server-cert.sh"

exit 0
