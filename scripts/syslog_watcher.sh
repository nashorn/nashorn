#!/usr/bin/env bash

########################################################
#
# dieses Skript erstellt Erinnerungen, falls in /var/log/syslog
# ein Eintrag auftaucht, der (fail|err|warn) enthält
#
# Werden regexe aus der Ignorierliste erkannt, wird keine Erinnerung erstellt werden.
# Die Ignorierliste findet sich unter: "${configs_repo}/syslog_watcher_ignore.conf"
#
# dieses Skript wird mit Systemstart gestartet und überwacht das syslog.
# Deaktivieren:
#    - entweder durch auskommentieren in /etc/cron.d/nashorn_cronjobs und neu starten
#    - mit `ps -aux | grep syslog_watcher` Prozess-ID rausinden
#           und dann mit `kill Prozess-ID` beenden
#
# (c) 2020 Freirechner GbR
#
########################################################

# zum debuggen:
#set -x

#set -euo pipefail

# Einbinden von functions_nashorn:
. /root/nashorn/scripts/functions_nashorn


trap_term() {
  # Ausstiegsfunktion, falls dieses Skript unterbrochen wird (z.B. mit pkill während eines Updates)
  write_to_log "syslog_watcher.sh terminated"
  exit 0
}

# kurze Ausgabe ins Logfile, dass syslog_watcher gestartet wurde:
write_to_log "syslog_watcher gestartet."

# Einbinden der regexe, die ignoriert werden sollen:
# shellcheck source=../configs/syslog_watcher_ignore.conf
. "${configs_repo}"/syslog_watcher_ignore.conf

# set Trap, if script gets terminated (e.g. during an update):
trap 'trap_term' TERM

# Suchbegriff als regex:
regex='(fail|error|warn)'
regex_dnscrypt='dnscrypt-proxy\[[[:digit:]]+\]: \[.+\] \[ERROR\] No useable certificate found'
regex_logrotate='logrotate\[[[:digit:]]+\]: error: ([^:]+:([[:digit:]]+|prerotate)|found error)'

# Groß-/Kleinschreibung beim Pattern-Matching ignorieren, damit z.B. sowohl ERROR als auch error gefunden wird:
shopt -s nocasematch

while read -r line; do

  if [[ ${line} =~ ${regex} ]]; then

    # prüfen, ob dnscrypt-proxy einen Zertifikatsfehler meldet. Das bedeutet, die Zeit ist falsch eingestellt und nashorn ist offline
    if [[ ${line} =~ ${regex_dnscrypt} ]]; then

      # dnscrypt-proxy kann das Zertifikat nicht überprüfen. Vermutlich weil nashorn lange ausgeschaltet war und noch in der Vergangenheit lebt.
      write_to_log "${line}"
      write_to_log "Zeit scheint falsch eingestellt zu sein. IP eines Zeitservers zu chrony hinzufügen..."

      # die IP eines Zeitservers aus 0.de.pool.ntp.org ermitteln (Namensauflösung erldigt quad9 für uns):
      timeserverip=$(nslookup 0.de.pool.ntp.org 9.9.9.9 | grep -s Address 2>/dev/null | tail -n 1)
      # shellcheck disable=SC2086
      timeserverip=$(printf '%s' ${timeserverip#*[[:space:]]})

      # die IP des NTP-Servers zu chronyd hinzufügen:
      chronyc add server "${timeserverip}"

      write_to_log "IP eines Zeitservers von 0.de.pool.ntp.org manuell zu chrony hinzugefügt."
      continue

    fi

    # prüfen, ob logrotate einen Fehler wegen verschandeltem configfile meldet. Das bedeutet, dass armbian-hardware-optimization quer
    # gelaufen ist und die logrotate-configuration wiederhergestellt werden muss
    # NOTIZ: dies kann eigentlich nur bis NAShorn v1.9.0 passieren, da mit v1.9.0 'armbian-hardware-optimization.service' deaktiviert wurde.
    if [[ ${line} =~ ${regex_logrotate} ]]; then
      # da bei fehlerhaften Configfiles in /etc/logrotate.d/ viele "error"-Zeilen mit selbem Zeitstempel im Syslog auftauchen,
      # dient der Zeitstempel der letzten Statusänderung der Dateien unter /etc/logrotate.d/ als Indikator, ob der Fehler schon
      # erfasst und korrigiert wurde. Wird kein Konfigfile gefunden, dessen Status sich vor weniger als 1 Minute geändert hat,
      # gehen wir davon aus, dass der Fehler schon korrigiert wurde (bedeutet: copy_logrotate_configs wurde kürzlich ausgeführt).
      if [[ -z $(find /etc/logrotate.d/ -cmin -1) ]]; then
        # Der Status keines der logrotate config files wurde in der letzten Minute verändert, also ist ein neuer Fehler aufgetreten
        # und die logrotate-konfigs müssen kopiert werden:
        write_to_log "logrotate schießt quer - stelle die Logrotate-Konfigurationsdateien wieder her..."
        copy_logrotate_configs
      fi
      # weitermachen mit der nächsten (fail|warn|error)-Zeile im Syslog:
      continue
    fi

    # prüfen, ob $line mit einem der syslog_ignore_regex übereinstimmt - falls ja: nächste Iteration des while loops:
    for (( i=0; i<${#ignore[@]}; i++ )); do
      [[ ${line} =~ ${ignore["${i}"]} ]] && continue 2
    done

    # Timestamp erzeugen, für Summary des vtodos:
    timestamp=$(date +"%d.%m.%y_%H:%M")

    # Summary spezifizieren, ja nach Fail, Warn, Err:
    case ${line} in
      *fail* )
        level="Fail"
      ;;
      *error* )
        level="Error"
      ;;
      *warn* )
        level="Warn"
      ;;
    esac

    # die gefundene syslog-Meldung ins logfile des syslog_watchers schreiben:
    printf '%s\n' "${line}" >> /var/log/nashorn/syslog_watcher.log

    # falls sich Nextcloud im Maintenance-Modus befindet, kein vtodo erstellen. Stattdessen weiter mit nächstem Loop der while-Schleife:
    if grep -q "^[[:blank:]]*'maintenance'[[:blank:]]*=>[[:blank:]]*true,[[:blank:]]*$" "${www}/nextcloud/config/config.php"; then
      continue 1
    fi

    # vtodo erstellen mit $level und $timestamp als Summary und Syslog-Eintrag als Description:
    # (wird im Hintergrund ausgeführt, daher wird dieses Skript nicht abgebrochen, falls create_vtodo.sh einen Fehler verursacht)
    "${scripts_repo}/create_vtodo.sh" "${level} ${timestamp}" "${line}" &

  fi

# WICHTIG:
# falls sich die folgende tail-Anweisung ändert, muss in 'functions_nashorn_update' das pkill-Kommando zum Beenden dieses tails angepasst werden!
# Notiz: Argument "-n 0" oder andere Zeilenangabe (0-999) muss hierbleiben, damit pkill-Kommando während eines Updates greift.
done < <(tail -n 0 -f "/var/log/syslog")

# Hier kommen wir nur hin, wenn das tail der while-Schleife hierdrüber beendet wird (z.B. über pkill während nashorn_update):
write_to_log "syslog_watcher exited due to termination of the tail command in the while loop."
exit 0
