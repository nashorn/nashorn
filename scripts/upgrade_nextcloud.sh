#!/usr/bin/env bash

###
### upgrade_nextcloud.sh
###

# for debugging:
#set -x

set -Euo pipefail

. /root/nashorn/scripts/functions_nashorn

# in case of an error set fail flag and return from running function:
trap "fail=1;" ERR

# verify that first given argument is a two digit number or exit with an error:
upgrade_to_version="${1:-not_given}"
if [[ ! ${upgrade_to_version} =~ ^[[:digit:]]{2}$ ]]; then
  error_exit "No version number or invalid version number of nextcloud given ('${upgrade_to_version}')." \
             "USAGE: ./update_nextcloud.sh <version_number>" \
             "Exiting."
fi

# verify that first given argument is next major version of currently installed nextcloud
installed_version="$(${occ} --version)"
installed_version="${installed_version##* }"
installed_version="${installed_version%%.*}"
if [[ ${upgrade_to_version} -le ${installed_version} ]]; then
  # There is nothing to do, as the installed version is the same or ahead of the one in the given argument.
  write_to_log "There is nothing to do as Nextcloud is already installed in version '${installed_version}'."
  # make sure, the correct version number is in status.txt (in case status.txt was erased):
  read_status_txt
  if [[ ${statustxt[nextcloud_version]} != "${installed_version}" ]]; then
    outdated="${statustxt[nextcloud_version]}"
    statustxt[nextcloud_version]="${installed_version}"
    write_status_txt
    write_to_log "I updated the version number in status.txt (${outdated}) as it did not reflect the installed version (${installed_version})."
  fi
  # exit without error as everything is perfectly fine:
  exit 0
elif [[ ${upgrade_to_version} -gt $(( installed_version + 1 )) ]]; then
  # this should never happen: given version is more than 1 version ahead of the installed version.
  # exit with an error as only upgrades to next major version are supported:
  error_exit "Only upgrades to next major version are supported." \
             "installed_version: ${installed_version} <---> upgrade_to_version: ${upgrade_to_version}" \
             "Exiting."
fi

write_to_log "preparing for upgrading nextcloud: declaring variables..."
# set paths to nextcloud directory and temporary directory for upgrading nextcloud:
nextcloud_dir="${www}/nextcloud"
temp_dir_un="/media/dataPartition/upgrade_nextcloud_temp"

# filename of archive for nextclouds next major version:
nextcloud_archive="latest-${upgrade_to_version}.tar.bz2"

# list of tasks (these actually are the functions that will be run later to upgrade nextcloud)
# ATTENTIION: the function 'remove_temp_dir_un' in any case needs to stay the last one in this list, as with
#             deleting the temp dir also the memory file with saved tasklist array index gets deleted!
tasklist=(create_temp_dir_un \
          download_and_verify_archive \
          extract_archive \
          adjust_permissions \
          enable_maintenance_mode \
          copy_config_php \
          rsync_old_nextcloud \
          create_database_dump \
          remove_old_nextcloud \
          rsync_new_nextcloud \
          move_new_nextcloud \
          upgrade_nextcloud \
          restore_log_level \
          manual_steps \
          disable_maintenance_mode \
          write_major_version_to_status_txt \
          remove_temp_dir_un \
)

create_temp_dir_un() {
  # create temporary folder and cd into it:
  write_to_log "creating temporary folder and 'cd' into it..."
  mkdir -p "${temp_dir_un}"
  # shellcheck disable=SC2164
  cd "${temp_dir_un}"
}

download_and_verify_archive() {
  # download Nextcloud archive, according SHA512 checksum, PGP signature key and signature and verify archive
  # ATTENTION: don't resume downloads, because then files do not get downloaded again if checksum/signature is invalid!

  write_to_log "downloading Nextcloud '${nextcloud_archive}', checksum and PGP signature..."
  # download Nextcloud archive:
  curl -sSf -O "https://download.nextcloud.com/server/releases/${nextcloud_archive}"
  # download sha512 checksum:
  curl -sSf -O "https://download.nextcloud.com/server/releases/${nextcloud_archive}.sha512"
  # download PGP signature
  curl -sSf -O "https://download.nextcloud.com/server/releases/${nextcloud_archive}.asc"
  # download Nextcloud PGP signature key
  curl -sSf -O "https://nextcloud.com/nextcloud.asc"

  # verify sha512 checksum (only archive, ignore metadata file which was introduced with Nextcloud 30):
  write_to_log "verifying checksum of downloaded archive:"
  sha512sum --check <(grep "${nextcloud_archive}" "${nextcloud_archive}.sha512")

  # verify PGP signature:
  write_to_log "importing signature key and verifying PGP signature of downloaded archive:"
  # import key:
  gpg --import --quiet nextcloud.asc
  # verify signature:
  gpg --verify "${nextcloud_archive}.asc" "${nextcloud_archive}"
}

extract_archive() {
  # extract archive
  write_to_log "extracting archive '${nextcloud_archive}'..."
  tar -xjf "${nextcloud_archive}"
  # # attempt to remove CAN_INSTALL file only, if it is existing to prevent endless error loop:
  if [[ -e "nextcloud/config/CAN_INSTALL" ]]; then
    write_to_log "removing Nextclouds CAN_INSTALL file..."
    rm "nextcloud/config/CAN_INSTALL"
  fi
}

adjust_permissions() {
  # assing owner and permissions for new nextcloud folder

  write_to_log "assigning owner and adjusting permissions..."
  # assign webserver user as owner of the extracted archive:
  chown -R www-data:www-data "nextcloud"

  # permissions need to be 640 for files and 750 for folders
  # set permissions for everything in nextcloud folder to 640:
  chmod -R 640 nextcloud
  # adjust permissions for folders to 750:
  chmod 750 nextcloud
  find "nextcloud" -type d -exec chmod 750 {} \;
}

enable_maintenance_mode() {
  # enable maintanance mode and wait 10 seconds for running requests to be completed

  write_to_log "enabling Nextclouds maintenance mode:"
  ${occ} maintenance:mode --on
}

copy_config_php() {
  # copy nextclouds config.php to new nextcloud folder:
  write_to_log "copying Nextclouds config.php to new Nextcloud folder..."
  cp -a "${nextcloud_dir}/config/config.php" "${temp_dir_un}/nextcloud/config/"
}

rsync_old_nextcloud() {
  # rsync old nextcloud folder to temporary folder
  write_to_log "rsyncing old nextcloud to temporary directory..."
  rsync --archive --delete "${nextcloud_dir}" "${temp_dir_un}/nextcloud_old/"
}

create_database_dump() {
  # create gzipped dump of nextcloud database in temporary folder
  write_to_log "creating gzipped dump of nextcloud database in temporary folder..."
  mysqldump --single-transaction -S "/run/mysqld/mysqld.sock" "nextcloud" | gzip > "${temp_dir_un}/nextcloud_${installed_version}_sql.bak"
}

remove_old_nextcloud() {
  # remove old nextcloud folder from webroot
  write_to_log "removing old nextcloud from webroot..."
  # attempt to remove old nextcloud folder only, if it is (still) there to prevent endless error loop:
  if [[ -e "${nextcloud_dir}" ]]; then
    rm -r "${nextcloud_dir}"
  fi
}

rsync_new_nextcloud() {
  # rsync new nextcloud folder temporarily to /var/nextcloud
  write_to_log "rsyncing new nextcloud to '/var/nextcloud' (move to webroot is next task)..."
  rsync --archive --delete "${temp_dir_un}/nextcloud" "/var/"
}

move_new_nextcloud() {
  # move new nextcloud folder to webroot
  write_to_log "moving new nextcloud from '/var/nextcloud' to webroot '${www}/'..."
  mv "/var/nextcloud" "${nextcloud_dir}"
}

upgrade_nextcloud() {
  # launch upgrade from the command line using occ:
  write_to_log "launching the upgrade from the command line using 'occ':"
  ${occ} upgrade
}

restore_log_level() {
  # restore default loglevel (in case of an interrupted upgrade process, it may still be set to 'debug').
  write_to_log "restoring loglevel to 'warning':"
  ${occ} log:manage --level 'warning'
}

manual_steps() {
  # perform manual steps after upgrade
  write_to_log "performing manual steps after upgrade:"

  # add missing indices:
  write_to_log "1. add missing indices to tables (for Nextcloud >= 13.0):"
  ${occ} db:add-missing-indices --no-interaction

  # switch to bigint for file ID's in the file cache table:
  write_to_log "2. switch to bigint for file ID's in the file cache table (for Nextcloud >= 13.0):"
  ${occ} db:convert-filecache-bigint --no-interaction

  if [[ ${upgrade_to_version} -ge 19 ]]; then
    # add missing columns for Nextcloud >= 19:
    write_to_log "3. add missing columns to tables (for Nextcloud >= 19.0):"
    ${occ} db:add-missing-columns --no-interaction
  fi

  if [[ ${upgrade_to_version} -ge 20 ]]; then
    # add missing primary keys for Nextcloud >= 20.0.2 (command was registered with Nextcloud 20.0.2):
    write_to_log "4. add missing primary keys to tables (for Nextcloud >= 20.0.2):"
    ${occ} db:add-missing-primary-keys --no-interaction
  fi

  if [[ ${upgrade_to_version} -ge 28 ]]; then
    # invoke 'maintenance:repair --include-expensive' for mimetype migrations (for Nextcloud 28 >= 28.0.9 or Nextcloud >= 29.0.3):
    write_to_log "+  5. invoking 'maintenance:repair --include-expensive' for mimetype migrations (for Nextcloud 28 >= 28.0.9 and Nextcloud >= 29.0.3):"
    ${occ} maintenance:repair --include-expensive --no-interaction
  fi
}

disable_maintenance_mode() {
  # disable maintanance mode
  write_to_log "disabling Nextclouds maintenance mode:"
  ${occ} maintenance:mode --off
}

write_major_version_to_status_txt() {
  # write new nextcloud major version number to statustxt array
  write_to_log "writing new nextcloud major version number to status.txt..."
  new_installed_version="$(${occ} --version)"
  new_installed_version="${new_installed_version##* }"
  new_installed_version="${new_installed_version%%.*}"
  read_status_txt
  statustxt[nextcloud_version]="${new_installed_version}"
  write_status_txt
}

# this needs to stay the last function in the list, as along with the temporary
# folder also the memory file with saved tasklist array index gets deleted!
remove_temp_dir_un() {
  # remove temporary directory
  write_to_log "removing temporary directory '${temp_dir_un}'..."
  # attempt to remove temporary directory only, if it is (still) there to prevent endless error loop:
  if [[ -e "${temp_dir_un}" ]]; then
    rm -r "${temp_dir_un}"
  fi
}

attempt=0
upgrade_successful="no"

while [[ ${upgrade_successful} != "yes" ]]; do

  # read memory file with saved tasklist array index with which to continue or set default starting point:
  if [[ ! -f ${temp_dir_un}/continue_with_task.txt ]]; then
    # no saved memory file found - assign default starting point:
    continue_with_task="0"
  else
    # memory file with saved state found - use that as starting point:
    continue_with_task=$(<"${temp_dir_un}/continue_with_task.txt")
  fi

  # loop through tasklist and execute tasks that haven't been run yet:
  for (( i=continue_with_task; i<${#tasklist[@]}; i++ )); do

    # reset fail indicator:
    fail=0
    # count attempts of runs of the task:
    attempt=$(( attempt + 1 ))
    ${tasklist[${i}]}

    if [[ ${fail} -eq 1 ]]; then
      # there was an error when executing the task:

      if [[ ${attempt} -lt 5 ]]; then
        # less than 5 attempts - retry in 10 seconds:
        write_to_log "There was an error when running task '${tasklist[${i}]}'." \
                     "Trying again in 10 seconds..."
        sleep 10
        # exit loop to try again:
        break
      else
        # 5th unsuccessful attempt - give up:
        error_exit "failed 5 times with task '${tasklist[${i}]}'. Giving up!"
      fi

    else
      # no error: preparations for next task (if this was not the last one ('remove_temp_dir()'))

      if [[ ${tasklist[${i}]} != "remove_temp_dir_un" ]]; then
        # write index of next task to memory file:
        printf '%s' "$(( i + 1 ))" > "${temp_dir_un}/continue_with_task.txt"
        # reset attempt counter for next task:
        attempt=0
      fi

    fi

  done # inner loop - looping through all elements of tasklist

  if [[ ${fail} -eq 0 ]]; then
    # YES - all tasks completed without a single error! Upgrade was successful!
    upgrade_successful="yes"
  fi

done  # outer loop - is run until ${upgrade_successful} is set to "yes"

# if we get here - upgrade was successful!
write_to_log "Nextcloud upgrade to 'Nextcloud ${upgrade_to_version}' successful!"

exit 0
