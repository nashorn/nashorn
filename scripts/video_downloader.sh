#!/usr/bin/env bash

#################################################################
#
# This script downloads a video from a website
#
# Usage: ./video_downloader.sh "URI" "Video|Audio"
#
# 1. argument (mandatory!) is:
#     - either  URL to website containing video to be downloaded
#     - or "videolink_history_delete" to delete history completely or partly (see 2. argument)
# 2. argument is :
#     - either "Video" or "Audio" to extract audio and delete video after download
#     - or, if first argument is "videolink_history_delete":
#        - a number of days (0-999). Records in the database older than this number of days
#          will be deleted. If number of days == 0 the database will be wiped out
#
# NOTICE: download of YouTube-playlists currently not possible at least until this found its way to Buster-Backports:
#         https://github.com/ytdl-org/youtube-dl/issues/26509
# NOTICE: option -i|--ignore-errors is needed when downloading playlists, otherwise download stops, if one video is not available
#
#################################################################

# for debugging:
#set -euo pipefail


# include functions:
. /root/nashorn/scripts/functions_nashorn

# path to "database"-file with records for each download which will be included in ytdl.php:
ytdltxt="/var/www/ytdl.txt"
# make sure "database"-file with download-history is existing:
[[ -f ${ytdltxt} ]] || touch "${ytdltxt}"

# either first argument (url) was given, or exit after log-message:
[[ -n ${1:-} ]] || {
  write_to_log "keine URL übergeben. Download wird nicht gestartet. Rufe yt-dlp nicht auf."
  exit 0
}

# delete download history partly or completely:
if [[ ${1} == "videolink_history_delete" ]]; then

  period=${2:-}
  regex='^[[:digit:]]{1,3}$'
  [[ ${period:-} =~ ${regex} ]] || period="0"

  if [[ ${period} -eq 0 ]]; then

    # completely delete "database" with download-history ($ytdltxt) and exit this script:
    printf '%s' "" > "${ytdltxt}"
    write_to_log "Download-History (Datenbank) des Video-Downloaders '${ytdltxt}' komplett gelöscht."

  else

    # only delete records older than $period days from the "database" with the download history (ytdl.txt):
    period_seconds=$(( period * 24 * 60 * 60 ))
    now=$(date +%s)
    to_old=$(( now - period_seconds ))

    # read database line by line and check for to old timestamps:
    while read -r line; do

      # get id of database record (first element in record):
      id=${line%%|_|_|*}
      # get unix timestamp from database record (id without the last two digits):
      timestamp=${id:0:-2}

      # either id is younger than $to_old or add id to array of elements to be deleted:
      [[ ${timestamp} -gt ${to_old} ]] || to_be_deleted+=("${id}")

    done < "${ytdltxt}"

    # if $to_be_deleted is set: delete all records collected in array.
    # NOTICE: do this seperate from the loop with collecting the to be deleted ids from the database. Might interfere!
    if [[ -n "${to_be_deleted[*]}" ]]; then

      # counter for logging how many records have been deleted:
      count=0

      # loop through arrays with ids to be deleted and delete record with id from database:
      for id in "${to_be_deleted[@]}"; do
        sed -i "/${id}|_|_|/d" "${ytdltxt}"
        count=$(( count + 1 ))
      done

      # write info to log:
      write_to_log "deleted ${count} records from 'database' with video download history."

    fi

  fi

  # exit this script, because we are done with cleaning up the database:
  exit 0

fi

update_database_record() {
  # updates or creates a certain database record for this video download identified through ID:

  #  structure of database records:
  #  ID|status_code|kind|date_start|date_end|error_code|title|unused_1|unused_2|URL
  #
  #  ID = timestamp + 2 random digits (see above, also used to retrieve time of download start in ytdl.php)
  #  status_code ( status_codes >= 90 are error codes):
  #          0 = URL passed to this script and script started
  #          5 = title downloaded and in database
  #         10 = Download starting (next command is yt-dlp)
  #         20 = Download completed
  #         90 = unsupported URL
  #         91 = Video (or title) could not be downloaded due to an error
  #         92 = AtomicParsley could not embed thumbnail (Error message from yt-dlp: "ERROR: ")
  #  kind = "Video" or "Audio" ( passed to this script as parameter ${2} )
  #  date_start = timestamp of start of this script
  #  date_end = date of youtbe-dl exit (either download successfully completed or exit with an error)
  #  error_message = error message, if download could not be completed due to an error (yt-dlp exited with an error)
  #  video_title = title of the video/audio to be downloaded
  #  video_filename = filename under which the video/audio will be saved
  #  unused: currently unused, but eventually needed in upcoming versions
  #  URL = URL to video ( passed to this script as paramter ${1} )

  # check for database record with ID:
  if grep "^${id}|_|_|.*$" "${ytdltxt}" >/dev/null 2>&1; then
    # found a database record with this ID. Let's update it:
    sed -i "s#^${id}|_|_|.*#${id}|_|_|${status_code}|_|_|${kind}|_|_|${date_start}|_|_|${date_end}|_|_|${error_message}|_|_|${video_title}|_|_|${video_filename}|_|_|${unused}|_|_|${url}#" "${ytdltxt}"
  else
    # no database record with this ID found. Let's create it:
    printf '%s|_|_|%s|_|_|%s|_|_|%s|_|_|%s|_|_|%s|_|_|%s|_|_|%s|_|_|%s|_|_|%s\n' "${id}" "${status_code}" "${kind}" "${date_start}" "${date_end}" "${error_message}" "${video_title}" "${video_filename}" "${unused}" "${url}" >> "${ytdltxt}"
  fi
}

# Set database records:
# ID for this video created from unix epoch and 2 random numbers (for ${ytdltxt}):
id=$(( ( RANDOM + 1 ) * 10 ))
id="$(date +%s)${id:0:2}"

# start with status_code 0:
status_code="0"

# assign $kind (Audio/Video) and make sure, it is a valid value:
kind=${2:-}
[[ -n ${2:-} && ${2:-} =~ ^(Video|Audio)$ ]] || kind="Video"

date_start=$(date +'%d.%m.%Y, %H:%M:%S')
date_end="date_end"
error_message="error_message"
video_title="video_title"
video_filename="video_filename"
unused="unused"
url=$(trim_leading_trailing_spaces "${1}")

# create record for this video in ytdl.txt:
update_database_record

# path to configfile with supported sites:
ytdl_configfile="/media/dataPartition/data/.media-downloader-config"
# copy config-file, if it is not existing:
[[ -f ${ytdl_configfile} ]] || {
  cp -a "${configs_repo}/${ytdl_configfile##*/}" "${ytdl_configfile}"
  chown nashorn:nashorn "${ytdl_configfile}"
  write_to_log "Configfile '${ytdl_configfile##*/}' kopiert, da nicht vorhanden unter /media/dataPartition/data/"
}

# assign url to test-variable (will be manipulated to test for supported sites):
test=${url}
# cut protocol ('http(s)://') from beginning of url:
test=${test#*://}

# check whether url is from a supported website:
while read -r line; do
  # don't process empty lines or comments (lines starting with a number sign (#)):
  [[ ${line:-} == "" || ${line:-} == "#"* ]] && continue
  if [[ ${test} == ${line:-}* || ${test#*.} == ${line}* ]]; then
    # Match: url is within supported sites. Set flag and exit the while loop:
    supported="yes"
    break
  fi
done < "${ytdl_configfile}"

# exit, if site is not within list with suppported sites:
if [[ ${supported:-} != "yes" ]]; then
  # change record, that URL is unsupported (status_code 90):
  status_code="90"
  update_database_record
  # write to log:
  if [[ ${debug} -eq 1 ]]; then
    write_to_log "url '${url}' is not within supported sites!"
    write_to_log "(teststring was '${test}')"
  else
    write_to_log "(ID ${id}) url is not within supported sites!"
  fi
  write_to_log "Exiting."
  exit 0
fi
# write to log:
if [[ ${debug} -eq 1 ]]; then
  write_to_log "url '${url}' is within supported sites."
  write_to_log "(teststring was '${test}')"
  write_to_log "(match was '${line}')"
else
  write_to_log "(ID ${id}) url is within supported sites."
fi

# get media_downloader_bandwidth from .user-config and verify a syntactically correct value:
read_user_config
regex_bandwidth='^[1-9][0-9]*(\.[0-9]+)?[kKmMgGtT]?$'
if [[ ${user_config[media_downloader_bandwidth]:-unlimited} =~ ${regex_bandwidth} ]]; then
  limit_rate="--limit-rate ${user_config[media_downloader_bandwidth]}"
fi

# get title and filename of video/audio to be downloaded:
write_to_log "(ID ${id}) getting title, thumbnail and filename of video/audio to be downloaded..."
options="--quiet --no-warnings"
#options="-v"
options="${options} --no-call-home --get-title --get-thumbnail --get-filename --restrict-filenames"
# we only want to download the first item, if the url is a playlist
# (otherwise gib trouble may occur afterwards, if metadata can't be injected by yt-dlp)
options="${options} --playlist-items 1"
# also add format options to retrieve same filename as later when actually downloading the video:
options="${options} -f 'bestvideo[height<=1080][ext=mp4]+bestaudio[ext=m4a]/best[height<=1080][ext=mp4]/best[height<=1080]/best'"
# also use limit-rate, if defined in .user-config:
options="${options} ${limit_rate:-}"
if [[ ${debug} -eq 1 ]]; then
  write_to_log "command: yt-dlp ${options} '${url}'"
else
  write_to_log "(ID ${id}) command: yt-dlp ${options} 'DUMMY_URL'"
fi
temp=$(su -l nashorn -c "yt-dlp ${options} '${url}' 2>&1 || :")
temp=$(trim_leading_trailing_spaces "${temp}")
if [[ ${temp} == "ERROR: "* ]]; then
  error_message="${temp}"
  date_end=$(date +'%d.%m.%Y, %H:%M:%S')
  status_code="91"
  update_database_record
  write_to_log "(ID ${id}) Error getting videotitle and filename!"
  exit 0
else
  mapfile -t temp_array <<<"${temp}"

  video_title="${temp_array[0]}"
  # replace funky characters with underscore:
  video_title="${video_title//[#*?&]/_}"
  if [[ -n ${temp_array[2]} ]]; then
    # yt-dlp gave link to thumbnail:
    video_thumbnail="${temp_array[1]}"
    video_filename="${temp_array[2]}"
  else
    # yt-dlp did not give link to thumbnail:
    video_thumbnail="not_found"
    video_filename="${temp_array[1]}"
  fi

  # change kind to Audio in case file extension is one of mp3, m4a, aac:
  audio_regex='^(mp3|m4a|aac)$'
  if [[ ${video_filename##*.} =~ ${audio_regex} ]]; then
    kind="Audio"
  fi

  status_code="5"

  if [[ ${debug} -eq 1 ]]; then
    write_to_log "video-title    : '${video_title}'"
    write_to_log "video-thumbnail: '${video_thumbnail}'"
    write_to_log "video-filename : '${video_filename}'"
  fi

  update_database_record
  # unsetting options (needed again later) and not any longer needed variables:
  unset -v options temp temp_array
fi

# create options for yt-dlp:
# use current time as timestamp, don't contact yt-dlp server, no funky characters in filename:
options="--no-mtime --no-call-home --restrict-filenames"
# we only want to download the first item, if the url is a playlist
# (otherwise gib trouble may occur afterwards, if metadata can't be injected by yt-dlp)
options="${options} --playlist-items 1"
# download best format available, but not better than 1080p (to keep filesize reasonable)
# (NOTICE: prefer mp4 over other formats)
options="${options} -f 'bestvideo[height<=1080][ext=mp4]+bestaudio[ext=m4a]/best[height<=1080][ext=mp4]/best[height<=1080]/best'"
# embed metadata:
options="${options} --add-metadata"
# also use limit-rate, if defined in .user-config:
options="${options} ${limit_rate:-}"
# save file to folder "video" as standard (might get changed in the next if-clause for audio_only):
folder="/media/dataPartition/data/filme"

# extract audio, if 'only_audio' flag is set:
if [[ ${kind} == "Audio" ]]; then
  options="${options} --extract-audio --audio-format 'm4a'" # --audio-quality '128K'"
  # save file to folder "musik" since it is audio only:
  folder="/media/dataPartition/data/musik"
fi

# make sure folder to store the download exists and is owned by user 'nashorn:nashorn':
mkdir -p "${folder}"
chown -R nashorn:nashorn "${folder}"

# embed thumbnail only for mp4/m4a/mp3 and audio files
# (file-extension for audio-files might initially be different from m4a, because ytdl converts after download):
regex='^(mp4|m4a|mp3)$'
if [[ ${video_filename##*.} =~ ${regex} || ${kind} == "Audio" ]]; then
  # ytdl is able to embed thumbnail for mp4/m4a/mp3:
  options="${options} --embed-thumbnail"
  video_thumbnail="embedded_by_ytdl"
else
  # download thumbnail (use .tmp and convert to png) to embed it to the video after the video has been downloaded and catch error:
  if ! su -l nashorn -c "curl -sS --output '${folder}/${video_filename%.*}.tmp' '${video_thumbnail}'"; then
    write_to_log "(ID ${id}) video_thumbnail konnte nicht heruntergeladen werden."
    video_thumbnail="not_found"
  else
    if [[ ${debug} -eq 1 ]]; then
      write_to_log "video_thumbnail successfully downloaded to '${folder##*/}/${video_filename%.*}.tmp'"
    else
      write_to_log "(ID ${id}) video_thumbnail successfully downloaded to '${folder##*/}/DUMMY.tmp'"
    fi
    # convert thumbnail to png:
    if su -l nashorn -c "ffmpeg -i '${folder}/${video_filename%.*}.tmp' '${folder}/${video_filename%.*}.png' >/dev/null 2>&1"; then
      # Conversion of video_thumbnail to png was successful:
      video_thumbnail="${video_filename%.*}.png"
    else
      # Thumbnail could not be converted to png. Delete file with unsuccessful attempt and proceed as if thumbnail not found:
      write_to_log "(ID ${id}) video_thumbnail could not be converted to PNG. Deleting thumbnail and proceeding as if thumbnail not found."
      rm -f "${folder}/${video_filename%.*}.png"
      video_thumbnail="not_found"
    fi
  fi
  rm -f "${folder}/${video_filename%.*}.tmp"
fi

options="${options} -o '${folder}/${video_filename}'"
# suppress most of yt-dlp's output (for debugging: use second line instead of first and change error_message subshell below as well):
options="${options} --quiet --no-warnings"
#options="${options} -v"

if [[ ${debug} -eq 1 ]]; then
  write_to_log "Downloading ${kind} '${url}'..."
  write_to_log "command: yt-dlp ${options} '${url}'"
else
  write_to_log "(ID ${id}) Downloading ${kind}..."
  # cut filename from options for less verbose (privacy) logging:
  write_to_log "(ID ${id}) command: yt-dlp ${options%% -o *} 'DUMMY_URL'"
fi

# change record, that download will be started:
status_code="10"
update_database_record

# call yt-dlp to download the video and catch error message (for debugging: use second line instead of first and also change options a couple of lines above):
error_message=$(su -l nashorn -c "yt-dlp ${options} '${url}' 2>&1 >/dev/null || :")

if [[ ${kind} == "Audio" ]]; then
  # audio is remuxed from whatever format to m4a and saved as *.m4a by yt-dlp. We need to adapt the filename for the history:
  video_filename="${video_filename%.*}.m4a"
fi

# timestamp of yt-dlp exit:
date_end=$(date +'%d.%m.%Y, %H:%M:%S')

# check error message:
case ${error_message:-} in

  "ERROR: "?*)
    # yt-dlp reported an error - video could not be downloaded:
    status_code="91"
    update_database_record
    if [[ ${debug} -eq 1 ]]; then
      write_to_log "Beim Download von '${url}' ist folgender Fehler aufgetreten:" "'${error_message}'"
    else
      write_to_log "(ID ${id}) yt-dlp hat eine Fehlermeldung zurückgegeben."
    fi
  ;;

  "ERROR: ")
    # yt-dlp exited with an empty error message most likely meaning that
    # atomicparsley could not add thumbnail to mp4/m4a
    # (NOTICE: future versions of yt-dlp might not have this problem
    #  see: https://github.com/ytdl-org/youtube-dl/pull/25717)
    for file in "${folder}/${video_filename%.*}"*; do
      if [[ ${file##*.} != "${video_filename##*.}" ]]; then
        # convert image (possibly webm) to png:
        su -l nashorn -c "ffmpeg -i '${file}' '${file%.*}.png' >/dev/null 2>&1"
        if ! su -l nashorn -c "AtomicParsley '${file%.*}.${video_filename##*.}' --overWrite --artwork '${file%.*}.png'"; then
          status_code="92"
          error_message=$(tail -n1 "${logfile}")
          if [[ ${debug} -eq 1 ]]; then
            write_to_log "yt-dlp hat leere Fehlermeldung zurückgegeben (AtomicParsley?/ffmpeg?): '${url}'"
          else
            write_to_log "(ID ${id}) yt-dlp hat leere Fehlermeldung zurückgegeben (AtomicParsley?/ffmpeg?)."
          fi
        else
          # download successfully completed:
          status_code="20"
          error_message="error_message"
          if [[ ${debug} -eq 1 ]]; then
            write_to_log "Download erfolgreich (AtomicParsley): '${url}'"
          else
            write_to_log "(ID ${id}) Download erfolgreich (AtomicParsley)."
          fi
        fi
        rm -f "${file}" "${file%.*}.png"
        update_database_record
        break
      fi
    done
  ;;

  *)
    # download of video/audio from ytdl successfully completed.
    # if videofile is not a mp4/m4a container, we want to embed thumbnail:
    if [[ ! ${video_filename##*.} =~ ^(mp4|m4a)$ ]]; then
      # Convert Video to mp4 and embed video_thumbnail if possible:
      # create options for conversion with ffmpeg:
      if [[ ${video_thumbnail} =~ (not_found|embedded_by_ytdl) ]]; then
        # ffmpeg options for remuxing to mp4 without embedding thumbnail:
        ffmpeg_options="-i '${folder}/${video_filename}' -c copy -movflags +faststart"
      else
        # ffmpeg options for remuxing to mp4 and embedding thumbnail:
        ffmpeg_options="-i '${folder}/${video_filename}' -i '${folder}/${video_thumbnail}' -map 0 -map 1 -c copy -c:v:1 png -disposition:v:1 attached_pic -movflags +faststart"
        embedding_thumbnail=" and embedding thumbnail"
      fi
      write_to_log "(ID ${id}) remuxing video to mp4 container${embedding_thumbnail:-}..."
      if [[ ${debug} -eq 1 ]]; then
        write_to_log "command: ffmpeg ${ffmpeg_options} '${folder}/${video_filename%.*}.mp4'"
      fi
      # do the remuxing to an mp4 container (eventually embedding thumbnail) and catch errors:
      if su -l nashorn -c "ffmpeg ${ffmpeg_options} '${folder}/${video_filename%.*}.mp4' > /dev/null 2>&1"; then
        # video successfully converted to mp4 container. Delete original video and change $video_filename
        rm -f "${folder}/${video_filename}"
        video_filename="${video_filename%.*}.mp4"
        write_to_log "(ID ${id}) Video successfully remuxed to mp4 container!"
      else
        # video could not be converted to mp4 container. Delete unsuccessful conversion file:
        rm -f "${folder}/${video_filename%.*}.mp4"
        # try to embed the thumbnail to the videofile (which my work or not, we don't know because different container):
        ffmpeg_options="-i '${folder}/${video_filename}' -i '${folder}/${video_thumbnail}' -map 0 -map 1 -c copy -c:v:1 png -disposition:v:1 attached_pic"
        write_to_log "(ID ${id}) trying to embed the thumbnail to a non mp4-videocontainer..."
        if [[ ${debug} -eq 1 ]]; then
          write_to_log "command: ffmpeg ${ffmpeg_options} '${folder}/${video_filename%.*}.temp.${video_filename##*.}'"
        fi
        if su -l nashorn -c "ffmpeg ${ffmpeg_options} '${folder}/${video_filename%.*}.temp.${video_filename##*.}' > /dev/null 2>&1"; then
          # embedding thumbnail was successfull! Rename temp video-file with embedded thumbnail to original filename:
          mv "${folder}/${video_filename%.*}.temp.${video_filename##*.}" "${folder}/${video_filename}"
        else
          # remove file with unsuccessfull attempt to embed thumbnail:
          rm -f "${folder}/${video_filename%.*}.temp.${video_filename##*.}"
          write_to_log "(ID ${id}) Thumbnail could not be embedded to non mp4 container"
        fi
      fi

      # remove $video_thumbnail:
      rm -f "${folder}/${video_thumbnail}"

    fi

    # download was successfull no matter, if video-container could be converted or thumbnail could be embedded:
    status_code="20"
    error_message="error_message"
    update_database_record
    if [[ ${debug} -eq 1 ]]; then
      write_to_log "Download erfolgreich: '${url}'"
    else
      write_to_log "(ID ${id}) Download erfolgreich."
    fi
  ;;

esac

exit 0
