#!/usr/bin/env bash

#
# This script checks once a minute the power status of /dev/sda and /dev/sdb and
# writes a message to stdout, if either /dev/sda or /dev/sdb are active.
#
# Invoke either with cronjob and pipe stdout to a file or use:
# nohup ./check_hdd_power_status.sh 2>&1 &
#
# When running in background, stop the script with killing the according PID.
# When running in foreground (not via cronjob or nohup), use CTRL-C to exit the script.
#

set -euo pipefail

i=0

printf '%s\n' "$(date +"%d.%m.%Y, %H:%M:%S") --> Monitoring power status of /dev/sda and /dev/sdb" \
              "                         Stop monitoring with 'kill ${$}'"

while [[ ${i:-0} -eq 0 ]]; do

  # status der beiden Festplatten ermitteln (unkown, active/idle, standby, sleeping):
  status_sda=$(hdparm -C /dev/sda)
  status_sdb=$(hdparm -C /dev/sdb)

  # Text generieren, je nachdem ob sda active oder standby ist:
  if [[ ${status_sda:-unkown} =~ .*(active|unkown).* ]]; then
    active="sda"
  fi

  # Text generieren, je nachdem ob sda active oder standby ist:
  if [[ ${status_sdb:-unkown} =~ .*(active|unkown).* ]]; then
    active="${active:-   } sdb"
  fi

  # Falls active-Variable gesetzt ist --> Ausgabe von Zeitstempel und aktiven Laufwerken:
  if [[ -n ${active:-} ]]; then

    printf '%s\n' "$(date +"%d.%m.%Y, %H:%M:%S") --> active: ${active:-error-in-script}"

    # active-Variable zurücksetzen für nächsten Durchlauf:
    unset -v active

  fi

  # 60 Sekunden auf der faulen Haut liegen:
  sleep 60

done

# hier kommen wir nie hin:
exit 0
