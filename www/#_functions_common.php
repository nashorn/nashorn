<?php
// gemeinsame Funktionen von index.php und ytdl.php

function add_timestamp_to_filename($filename) {
  // fügt den Zeitstempel der letzten Modifizierung einer Datei dem Dateinamen hinzu
  // wird benötigt, damit Browser geänderte Dateien (z.B.: css/js) einlesen und nicht die Version
  // aus ihrem Cache benutzen (obwohl die Datei auf dem Server geändert wurde (z.B. nashorn.css).
  // Damit das funktioniert ist auch noch folgende rewrite-Regel in der nginx-Konfiguration nötig:
  //    location ~ ^/.+-\d\d\d\d\d\d\d\d\d\d\.[^\.]+$ { rewrite ^/(.+)-\d+\.(.+)$ /$1.$2; }

  // Falls Pfad nicht relativ zum webroot ist wird zuerst der übergebene Pfad (relativ zu Script) zu
  // "absolutem" Pfad (relativ zu webroot) umgewandelt:
  if ( substr( $filename, 0, 1 ) !== '/' ) {
    $script_dir = substr( $_SERVER['SCRIPT_NAME'], 0, ( strrpos( $_SERVER['SCRIPT_NAME'], '/' ) + 1 ) );
    $filename = $script_dir . $filename;
  }

  // Zeitstempel der letzten Dateimodifikation:
  $mtime = filemtime( $_SERVER['DOCUMENT_ROOT'] . $filename );
  // Dateiname mit eingefügtem Zeitstempel zurückgeben:
  return preg_replace( '{\\.([^./]+)$}', "-$mtime.\$1", $filename );
}



function create_backup_hour_options() {
  // gibt die einzelnen Optionen (00:00 Uhr bis 23:00) als Pulldown-Menü für das Backup_Hour-Formular auf index.php aus:
  // NOTIZ: diese Funktion wird nur in index.php verwendet und nicht in ytdl.php und ist daher keine gemeinsame Funktion.
  //        Sie ist trotzdem hier, damit sie nicht unnötigerweise alle 3 Sekunden durch ajax.php eingelesen wird.

  global $status;

  $temp_string = '';

  for($i=0;$i<24;$i++) {

    if ( $i < 10 ) {
      // eine Null voranstellen, falls $i einstellig ist:
      $hour = '0'.$i;
    } else {
      // $hour muss String sein, daher eine leere Zeichenkette voranstellen, um zweistellige Integer in String umzuwandeln:
      $hour = ''.$i;
    }
    // Ausgabe des html-Codes der einzelnen Optionen (Uhrzeiten):
    if ( $i == 0 ) {
      $temp_string .= '<option ';
    } else {
      $temp_string .= '              <option ';
    }
    if ( $hour === $status['backup_hour'] ) {
      // die eingestellte Uhrzeit soll vorausgewählt sein:
      $temp_string .= 'selected="selected" ';
    }
    $temp_string .= 'value="'.$hour.'">'.$hour.':00 Uhr</option>'.PHP_EOL;

  } // Ende der for-Schleife für Ausgabe der einzelnen Optionen

  return $temp_string;

}

?>
