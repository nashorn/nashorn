<?php
// Funktionen für index.php von NAShorn


function declare_ip_fqdn() {
  // füllt die Variablen $ip und $fqdn mit IP und FQDN
  // Notiz: $fqdn ist durch Leerzeichen getrennte Liste mit Domainnamen

  // global deklarieren:
  global $ip, $fqdn, $network_device, $status;

  // Für Board passendes Netzwerkdevice zuweisen:
  $network_device = 'br0';  // default espressobin
  if ( $status['board'] === 'odroidhc4' ) {
    $network_device = 'eth0';
  }

  // IP ermitteln und in Variable ablegen:
  $ip = $_SERVER['SERVER_ADDR'];
  // regex, der auf IPv4 überprüft:
  $regex = '/^((25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])$/';
  // überprüfen, ob $ip eine IPv4-Adresse ist:
  if ( ! preg_match ($regex, $ip) ) {
    // Leider liefert PHP keine IPv4-Adresse, daher IP-Adressen vom System einsammeln:
    $temp = exec('hostname --all-ip-addresses;');
    // eine IP nach der anderen durchgehen und Schleife abbrechen, falls eine IPv4-IP gefunden wurde:
    $names = preg_split('/[[:space:]]+/', $temp);
    foreach ( $names as $name ) {
      if ( preg_match ($regex, $name) ) {
        $ip = $name;
        break;
      }
    }
  }

  // FQDN ermitteln und in Variable durch Leerzeichen voneinander getrennt ablegen:
  $fqdn = exec('hostname;');
  $fqdn = trim($fqdn);
  $fqdn = $fqdn.' '.$fqdn.'.local'; // nashorn nashorn.local
  $temp = shell_exec('dns=$(resolvectl dns '.$network_device.'); nslookup '.$ip.' $(printf "%s" ${dns##*: }) | cut -d " " -f 3');
  $names = preg_split('/[[:space:]]+/', $temp);
  foreach ( $names as $name ) {
    if ( $name === '' ) { continue; }
    // make sure $name is not already in $fqdn and make sure, it is a name like *.*:
    if ( strpos($fqdn, $name) === false && preg_match('/^.+\..+\.$/', $name) ) {
      // add domain name, but without last dot:
      $fqdn = $fqdn.' '.substr($name, 0 , -1);
    }
  }
}



function create_ajax() {
  // erstellt globalen String $ajax, der per AJAX dynamisch nachgeladen wird (siehe: 'nashorn.js', 'ajax.php').
  // Die einzelnen Elemente werden im String durch den Delimiter "|" getretnnt.
  // Der String wird später (in 'index.php' und 'nashorn.js') in ein Array aufgeteilt.

  // globale Variablen deklarieren:
  global $ajax, $fqdn, $ip, $network_device, $status;

  // belegter Platz und Gesamtgröße der 'dataPartition'-Partition ermitteln:
  $df_dataPartition = exec('df -BGB --output=pcent,used,size /media/dataPartition | tail -n1');
  $df_dataPartition = preg_split( '/[[:space:]]+/', trim( $df_dataPartition ) );
  $pcent_dataPartition = substr( $df_dataPartition['0'], 0, -1 );
  $used_dataPartition = number_format( substr( $df_dataPartition['1'], 0, -2 ), 0, ',', '.' );
  $size_dataPartition = number_format( substr( $df_dataPartition['2'], 0, -2 ), 0, ',', '.' );

  // belegter Platz und Gesamtgröße der 'backup'-Partition ermitteln:
  $df_backup = exec('df -BGB --output=pcent,used,size /media/backup | tail -n1');
  $df_backup = preg_split( '/[[:space:]]+/', trim( $df_backup ) );
  $pcent_backup = substr( $df_backup['0'], 0, -1 );
  $used_backup = number_format( substr( $df_backup['1'], 0, -2 ), 0, ',', '.' );
  $size_backup = number_format( substr( $df_backup['2'], 0, -2 ), 0, ',', '.' );

  // defaults für Backup-/Poweroff-/Update-Link erstellen (wird evtl. in den nächsten 4 'if'-Konditionen geändert):
  // NOTIZ: die Links für die noscript-Abteilungen werden in index.php erstellt.
  $backup_link = '<a href="remote.php?backup_now&js=yes" target="dummy_result_form" class="buttonlike"><span class="typcn typcn-database"></span>internes Backup jetzt</a>';
  $idevicebackup_link = '<a href="remote.php?idevicebackup&js=yes" target="dummy_result_form" class="buttonlike"><span class="typcn typcn-arrow-repeat"></span>iPhone oder iPad sichern</a>';
  $poweroff_link = '<a href="remote.php?poweroff&js=yes" target="dummy_result" class="buttonlike" onclick="return confirm(\'Willst du nashorn ausschalten?\')"><span class="typcn typcn-power"></span><b>nas</b>horn herunterfahren</a>';
  $update_link = '<a href="remote.php?update&js=yes" target="dummy_result" class="buttonlike"><span class="typcn typcn-download"></span><b>nas</b>horn-Update</a>';

  // Backup läuft gerade: Backup-/iDeviceBackup-/Update-Link deaktivieren (Poweroff-Button bleibt hierbei aktiv):
  if ( $status['backup_in_progress'] === "yes" ) {
    $backup_link = '<span class="buttonlike disabled"><span class="typcn typcn-database"></span>internes Backup läuft gerade</span>';
    $idevicebackup_link = '<span class="buttonlike disabled"><span class="typcn typcn-arrow-repeat"></span>nicht während Backup</span>';
    $update_link = '<span class="buttonlike disabled"><span class="typcn typcn-download"></span>nicht während Backup</span>';
  }
  // Update läuft gerade: Backup-/iDeviceBackup-/Poweroff-/Update-Link deaktivieren:
  if ( $status['update_in_progress'] === "yes" ) {
    $backup_link = '<span class="buttonlike disabled"><span class="typcn typcn-database"></span>nicht während Aktualisierung</span>';
    $idevicebackup_link = '<span class="buttonlike disabled"><span class="typcn typcn-arrow-repeat"></span>nicht während Aktualisierung</span>';
    $poweroff_link = '<span class="buttonlike disabled"><span class="typcn typcn-power"></span><b>nas</b>horn herunterfahren</span>';
    $update_link = '<span class="buttonlike disabled"><span class="typcn typcn-download"></span><b>nas</b>horn wird gerade aktualisiert</span>';
  }
  // Poweroff läuft gerade: Backup-/iDeviceBackup-/Poweroff-/Update-Link deaktivieren:
  if ( $status['poweroff_in_progress'] === "yes" ) {
    $backup_link = '<span class="buttonlike disabled"><span class="typcn typcn-database"></span>nicht während des Herunterfahrens</span>';
    $idevicebackup_link = '<span class="buttonlike disabled"><span class="typcn typcn-arrow-repeat"></span>nicht während des Herunterfahrens</span>';
    $poweroff_link = '<span class="buttonlike disabled"><span class="typcn typcn-power"></span><b>nas</b>horn wird heruntergefahren</span>';
    $update_link = '<span class="buttonlike disabled"><span class="typcn typcn-download"></span>nicht während des Herunterfahrens</span>';
  }
  // iDeviceBackup läuft gerade: Backup-/iDeviceBackup-/Poweroff-/Update-Link deaktivieren:
  if ( $status['idevicebackup_in_progress'] === "yes" ) {
    $backup_link = '<span class="buttonlike disabled"><span class="typcn typcn-database"></span>nicht während iPhone/iPad-Sicherung</span>';
    $idevicebackup_link = '<span class="buttonlike disabled"><span class="typcn typcn-arrow-repeat"></span>iPhone/iPad-Sicherung läuft gerade</span>';
    $poweroff_link = '<span class="buttonlike disabled"><span class="typcn typcn-power"></span>nicht während iPhone/iPad-Sicherung</span>';
    $update_link = '<span class="buttonlike disabled"><span class="typcn typcn-download"></span>nicht während iPhone/iPad-Sicherung</span>';
  }
  // idevicebackup_link deaktivieren, wenn kein iDevice verbunden ist (dann gibt es keine idevice_id):
  exec('idevice_id > /dev/null 2>&1;', $output, $retval);
  if ( $retval !== 0 ) {
    $idevicebackup_link = '<span class="buttonlike disabled"><span class="typcn typcn-arrow-repeat"></span>iPhone oder iPad nicht angeschlossen</span>';
  }

  // bei internem Backup: nur dann ' Uhr' hinzufügen, falls schon ein Backup erstellt wurde, ansonsten steht in status.txt 'noch kein Backup erstellt'
  if ( $status['date_last_complete_backup'] !== "noch kein Backup erstellt" ) {
    $status['date_last_complete_backup'] .= ' Uhr';
  }

  // ebenso bei iDeviceBackup: 'Uhr' nur dann hinzufügen, wenn ein Zeitstempel einer Sicherung existiert (Zeitstempel bedeutet: Sicherung erfolgreich):
  $idevicebackup_date = $status['idevicebackup_date'];
  if ( preg_match ( '/^(?:\d{2}\.){2}\d{4}, (?:\d{2}:){2}\d{2}$/', $status['idevicebackup_date'] ) ) {
    $idevicebackup_date .= ' Uhr';
  }

  // Sichtbarkeit des Blocks "Neustart erforderlich":
  $display_reboot_required = 'none';
  if ( $status['reboot_required'] === "yes" ) {
    $display_reboot_required = 'block';
  }

  // status der Checkbox des Autoupdate-Mechanismus (typcn-...), default = off (media-stop-outline):
  if ( $status['autoupdatecheck'] === "on" ) {
    $autoupdatecheckbox = 'input-checked';      // wird in index.php und nashorn.js ergänzt zu 'typcn-input-checked'
  } else {
    $autoupdatecheckbox = 'media-stop-outline'; // wird in index.php und nashorn.js ergänzt zu 'typcn-media-stop-outline'
  }

  // default für die Sichtbarkeit des Containers mit Benachrichtigungen:
  $display_message_container = 'none';

  // Sichtbarkeit der Benachrichtigungs-Setup-Box für Autoupdate (default = nicht sichtbar):
  $display_setup_autoupdate = 'none';
  $regex = '/^(on|off)$/';
  if ( ! preg_match ($regex, $status['autoupdatecheck']) ) {
    // falls autoupdatecheck weder "on" noch "off" - Container mit Benachrichtugungen und setup_autoupdate sichtbar machen:
    $display_message_container = 'block';
    $display_setup_autoupdate = 'block';
  }

  // Versionsnummer mit Link zu changelog.md erzeugen (inklusive Datum):
  $software_version_link = '<a href="https://codeberg.org/nashorn/nashorn/src/branch/'.$status['software_branch'].'/changelog.md" target="_blank">'.$status['software_version'].'_uu'.$status['updated_until'].'_NC'.$status['nextcloud_version'].' ('.$status['software_branch'].')</a>';

  // Link für Software Commit erzeugen (inklusive Branchname):
  $commit_hash_short = substr( $status['software_commit_hash'], 0, 7 );
  $software_commit_link = '<a href="https://codeberg.org/nashorn/nashorn/src/commit/'.$status['software_commit_hash'].'" target="_blank">'.$commit_hash_short.' <nobr>('.$status['software_commit_date'].')</nobr></a>';

  // yt-dlp Version mit Link zum Changelog auf github:
  if ( $status['ytdl_version'] === "nicht vorhanden" ) {
    $ytdl_version_link = 'nicht vorhanden';
  } else {
    // to yt-dlp changelog @ github:
    $ytdl_version_link = '<a href="https://github.com/yt-dlp/yt-dlp/blob/'.$status['ytdl_version'].'/Changelog.md" target="_blank">'.$status['ytdl_version'].'</a>';
    // to yt-dlp changelog @ Debian:
    //$ytdl_version_link = '<a href="https://metadata.ftp-master.debian.org/changelogs/main/y/yt-dlp/yt-dlp_'.$status['ytdl_version'].'_changelog" target="_blank">'.$status['ytdl_version'].'</a>';
  }

  // Load-Average des Systems:
  $loadavg = exec('a=$(cat /proc/loadavg); a=${a% */*}; printf "%s" "${a}" 2>&1');
  $tempload = explode(" ", $loadavg);
  $number_of_processors = exec('cat /proc/cpuinfo | grep processor | wc -l');
  for ($i = 0; $i <= 2; $i++) {
    // jeden Wert bearbeiten, um die Load Prozessors anzugeben (und nicht die Load eines einzelnen Kerns):
    $tempload[$i] = round( ($tempload[$i] * 100 / $number_of_processors), 0, PHP_ROUND_HALF_DOWN);
  }
  $loadavg = $tempload['0'].' '.$tempload['1'].' '.$tempload['2'];

  // Netzwerktraffic:
  $temp = exec('a=$(cat /proc/net/dev | grep '.$network_device.'); printf "%s " ${a} | cut -d" " -f2,10');
  $network_traffic = preg_split('/[[:space:]]+/', $temp);

  // Uptime:
  $uptime = exec('a=$(uptime -p); printf "%s" "${a#up }"');

  // Variable $ajax initialisieren:
  //  0: IP
  $ajax  = $ip;
  //  1: FQDN
  $ajax .= '|'.$fqdn;
  //  2: Link, um sofort ein internes Backup zu erstellen:
  $ajax .= '|'.$backup_link;
  //  3: Anzahl vollständiger Backups auf der internen Backup-Platte:
  $ajax .= '|'.$status['number_of_backups'];
  //  4: Datum des letzten vollständigen internen Backups:
  $ajax .= '|'.$status['date_last_complete_backup'];
  //  5: Link, um ein Update der Software zu starten (Aufruf von update_nashorn.sh):
  $ajax .= '|'.$update_link;
  //  6: Datum des letzten Aufrufs von update_nashorn.sh
  $ajax .= '|'.$status['date_last_update'].' Uhr';
  //  7: Belegung Haupt-Festplatte (dataPartition, in Prozent - siehe auch 18+19):
  $ajax .= '|'.$pcent_dataPartition;
  //  8: Belegung Archiv-Festplatte (backup, in Prozent - siehe auch 20+21)::
  $ajax .= '|'.$pcent_backup;
  //  9: Software-Commit (verlinkt zum Commit auf Codeberg.org):
  $ajax .= '|'.$software_commit_link;
  // 10: letzter Neustart (angeschaltet seit):
  $ajax .= '|'.$uptime;
  // 11: Link, um nashorn herunterzufahren:
  $ajax .= '|'.$poweroff_link;
  // 12: Flag, um Weiterleitung bei Poweroff per Javascript einzurichten:
  if ( $status['poweroff_in_progress'] === "yes" ) {
    $ajax .= '|poweroff_in_progress';
  } else {
    $ajax .= '|running';
  }
  // 13: Load-Average des Systems:
  $ajax .= '|'.$loadavg;
  // 14: Network Received:
  $ajax .= '|'.$network_traffic['0'];
  // 15: Network Transmitted:
  $ajax .= '|'.$network_traffic['1'];
  // 16: Software Version (human readable, including link to changelog.md on codeberg.org):
  $ajax .= '|'.$software_version_link;
  // 17: yt-dlp Version (including link to changelog):
  $ajax .= '|'.$ytdl_version_link;
  // 18: belegter Platz in GB der 'dataPartition'-Pladde:
  $ajax .= '|'.$used_dataPartition;
  // 19: Gesamtkapazität in GB der 'dataPartition'-Pladde:
  $ajax .= '|'.$size_dataPartition;
  // 20: belegter Platz in GB der 'backup'-Pladde:
  $ajax .= '|'.$used_backup;
  // 21: Gesamtkapazität in GB der 'backup'-Pladde:
  $ajax .= '|'.$size_backup;
  // 22: css-class der autoupdatecheckbox (angehakt oder nicht: typcn-...)
  $ajax .= '|'.$autoupdatecheckbox;
  // 23: Sichtbarkeit des Containers mit Benachrichtigungen:
  $ajax .= '|'.$display_message_container;
  // 24: Sichtbarkeit des initialen Setups für autoupdate:
  $ajax .= '|'.$display_setup_autoupdate;
  // 25: Datum des ältesten internen Backups:
  $ajax .= '|'.$status['date_oldest_backup'];
  // 26: NAShorn-Systemzeit:
  $ajax .= '|'.date('d.m.Y, H:i');
  // 27: Temperatur der SATA-Platte (dataPartition):
  $ajax .= '|'.$status['temperature_sata'];
  // 28: Temperatur der USB-Platte (backup):
  $ajax .= '|'.$status['temperature_usb'];
  // 29: Sichtbarkeit des div-containers mit Neustart-erforderlich Nachricht:
  $ajax .= '|'.$display_reboot_required;
  // 30: Link um die Sicherung eines iDevices zu starten:
  $ajax .= '|'.$idevicebackup_link;
  // 31: Datum der letzten Sicherung eines iDevices:
  $ajax .= '|'.$idevicebackup_date;
  // 32: idevicebackup return code:
  $ajax .= '|'.$status['idevicebackup_return_code'];
  // 33: Sichtbarkeit der Warnung wegen unbekanntem iDevice-Sicherungspasswort:
  $ajax .= '|'.$status['idevicebackup_unknown_password'];
}



function read_status_txt() {
  // liest die Status-Datei 'status.txt' ein und deklariert das assoziative Array $status mit den Werten aus der Status-Datei

  // $status als global deklarieren, um auch außerhalb dieser Funktion darauf zugreifen zu können:
  global $status;

  // set defaults:
  $status['autoupdatecheck'] = 'undefined';
  $status['backup_hour'] = '03';
  $status['backup_in_progress'] = 'no';
  $status['backups_deleted'] = 'no';
  $status['board'] = 'espressobin';
  $status['date_last_complete_backup'] = 'noch kein Backup erstellt';
  $status['date_last_update'] = 'bisher keine Aktualisierung durchgeführt';
  $status['date_oldest_backup'] = 'undefined';
  $status['idevicebackup_date'] = 'noch keine Sicherung erstellt';
  $status['idevicebackup_in_progress'] = 'no';
  $status['idevicebackup_return_code'] = '0';
  $status['idevicebackup_unknown_password'] = 'no';
  $status['logfiles_anonymized'] = 'no';
  $status['nextcloud_version'] = '17';
  $status['number_of_backups'] = '0';
  $status['poweroff_in_progress'] = 'no';
  $status['reboot_required'] = 'no';
  $status['software_branch'] = 'stable';
  $status['software_commit_date'] = '.';
  $status['software_commit_hash'] = '.';
  $status['software_version'] = '0';
  $status['temperature_sata'] = 'unknown';
  $status['temperature_usb'] = 'unknown';
  $status['update_in_progress'] = 'no';
  $status['updated_until'] = '1.0.0';
  $status['ytdl_version'] = 'nicht vorhanden';

  // Status-Datei einlesen, falls sie existiert
  if (is_readable('/var/www/status.txt')) {
    // erzeuge array $status mit einer Zeile der Datei in jedem array-element. Zeilenumbrüche werden abgeschnitten:
    $statusdatei = file('/var/www/status.txt', FILE_IGNORE_NEW_LINES );
  }

  // assoziatives Array mit Variablennamen als Index und entsprechenden Werten erstellen:
  foreach ($statusdatei as $line) {
    $tmp = explode("=", $line);
    $status[$tmp['0']] = $tmp['1'];
  }

}

?>
