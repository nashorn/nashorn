<?php
// Funktionen für ytdl.php von NAShorn


function read_download_history() {

  global $download_history;

  $headline = 0;

  // Status-Datei einlesen, falls sie existiert
  if (is_readable('/var/www/ytdl.txt')) {
  // erzeuge array $status mit einer Zeile der Datei in jedem array-element. Zeilenumbrüche werden abgeschnitten:
    $history = file('/var/www/ytdl.txt', FILE_IGNORE_NEW_LINES );

    // eingelesenes Array rückwärts sortieren (das Neueste soll oben stehen):
    rsort($history);

    // assoziatives Array mit Variablennamen als Index und entsprechenden Werten erstellen:
    foreach ($history as $line) {

      if ( trim($line) !== '' ) {
        if ( $headline === 0 ) {
          // Verlaufsüberschrift wurde noch nicht erstellt. Das geschieht hier:
          $download_history = '<h2 id="history_header">Verlauf</h2>'.PHP_EOL;
          $download_history .= '<div id="history_subheader">Einträge  werden nach 7 Tagen gelöscht.'.PHP_EOL;
          $download_history .= '<a href="remote.php?videolink_history_delete">(Verlauf komplett löschen...)</a></div>'.PHP_EOL;
          // Indikator, dass Verlaufsüberschrift schon erstellt wurde:
          $headline = 1;
        }

      } else {
        continue;
      }

      // Zeile aufsplitten in einzelne Elemente und Array zuweisen:
      $tmp = explode("|_|_|", $line);

      // Array aussagekräftigeren Variablennamen zuweisen:
      $id=$tmp['0'];
      $status_code=$tmp['1'];
      $kind = $tmp['2'];
      $date_start = $tmp['3'];
      $date_end = $tmp['4'];
      $error_message = $tmp['5'];
      $video_title = $tmp['6'];
      $video_filename = $tmp['7'];
      $unused = $tmp['8'];
      $video_url = $tmp['9'];

      // Falls video_title noch unbekannt ist, als Linktext den Link selbst benutzen (abgekürzt auf 100 Zeichen):
      if ( $video_title !== 'video_title' ) {
        $linktext = $video_title;
      }  else {
        if ( substr($video_url, 101) === FALSE ) {
          $linktext = $video_url;
        } else {
          $linktext = substr($video_url, 0, 100).'...';
        }
      }

      // Beginn History-Element (Download Verlauf für diesen Download) zusammensetzen:
      $download_history .= '<div class="history_element">'.PHP_EOL;
      // Der Link zum Video (entweder mit Title oder URL) wird sozusagen als Überschrift angezeigt:
      $download_history .= '<a href="'.$video_url.'" target="_blank">'.$linktext.'</a>'.PHP_EOL;
      // darunter als unsortierte Liste die verschiedenen Ereignisse:
      $download_history .= '<ul>'.PHP_EOL;
      if ( $status_code == 90 ) {
        $download_history .= '<li>URL wird nicht unterstützt.</li>'.PHP_EOL;
      } else {
        $download_history .= '<li>'.$date_start.' '.$kind.'-Download gestartet.</li>'.PHP_EOL;
        switch ( $status_code) {
          case 20:
            // Download erfolgreich abgeschlossen:
            $download_history .= '<li>'.$date_end.' '.$kind.'-Download erfolgreich abgeschlossen.</li>'.PHP_EOL;
            $folder = "filme/";
            if ( $kind === "Audio" ) { $folder = "musik/"; }
            $download_history .= '<li>Speicherort: '.PHP_EOL;
            $download_history .= '  <code><a href="Daten/'.$folder.$video_filename.'">'.$folder.$video_filename.'</a></code>'.PHP_EOL;
            $download_history .= '</li>'.PHP_EOL;
            break;
          case 91:
            // Fehlermeldung von yt-dlp:
            $download_history .= '<li>'.$date_end.' '.$kind.' konnte nicht heruntergeladen werden.</li>'.PHP_EOL;
            $download_history .= '<li>'.$error_message.'</li>'.PHP_EOL;
            break;
          case 92:
            // Fehlermeldung von AtomicParsley:
            $download_history .= '<li>'.$date_end.' '.$kind.' Download erfolgreich, aber Vorschaubild konnte nicht eingebettet werden.</li>'.PHP_EOL;
            $download_history .= '<li>'.$error_message.'</li>'.PHP_EOL;
            break;
        }

      } // Ende der Ereignis-Liste dieses Downloads und Ende History-Element
      $download_history .= '</ul></div>'.PHP_EOL;

    } // Ende Download-History (Ende foreach-Schleife)
    $download_history .= '</div>'.PHP_EOL;

  }
}

?>
