<?php

  $error_code        = '403';
  $error_text_en     = 'Forbidden';
  $error_text_de     = 'Verboten';
  $error_description = 'der Browser darf auf die angeforderte Seite nicht zugreifen';
  $error_typicon     = 'cancel';

  include('error_common.php');
?>
