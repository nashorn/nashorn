<?php

  $error_code        = '404';
  $error_text_en     = 'Not Found';
  $error_text_de     = 'Nicht gefunden';
  $error_description = 'der Browser kann die angeforderte Seite nicht finden';
  $error_typicon     = 'delete';

  include('error_common.php');
?>
