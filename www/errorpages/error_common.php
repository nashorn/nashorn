<?php
  $nashornlink = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
  $datenlink = $nashornlink.'/Daten/';
  $requesturi = $nashornlink.$_SERVER['REQUEST_URI'];

  if ( ( strpos($_SERVER['REQUEST_URI'], '/Daten') ) === 0 ) {
    $go_back = 'Hier geht es zurück zum Datenverzeichnis:<br>'.PHP_EOL;
    $go_back .= '<a href="'.$datenlink.'">'.$datenlink.'</a>'.PHP_EOL;
  } else {
    $go_back = 'Hier geht es zurück zum NAShorn:<br>'.PHP_EOL;
    $go_back .= '<a href="'.$nashornlink.'">'.$nashornlink.'</a>'.PHP_EOL;
  }
?>

<!DOCTYPE html>
<html lang="de">

  <head>
    <title><?php echo $error_code.' - '.$error_text_en; ?></title>
    <!--#include virtual="/common_headers.html.inc" -->
    <link rel="stylesheet" href="/nashorn.css">
  </head>

  <body>
    <!--
      <h1 style="color: green;"><a href="<?php echo $nashornlink; ?>"><img src="/images/logo.png"></a>NAShorn-Server</h1>
    -->
    <div style="font-size: 3em; font-weight: bold; margin: 0.2em 0em; color: #505050;">
      <span class="typcn typcn-<?php echo $error_typicon; ?>" style="color: #f03030; font-size: 2em; vertical-align: middle;"></span>
    </div>
    <div style="font-size: 3em; font-weight: bold; margin: 0.2em 0em 0em 0.4em; color: #505050;">
      <?php echo $error_code.': '.$error_text_de.PHP_EOL; ?>
    </div>
    <div style="font-size: 150%; color: #282828; padding: 0em 0em 0.8em 0.8em;">
      <!-- (<?php echo $requesturi; ?>)<br> --> <br>
      Etwas ist schiefgelaufen: <?php echo $error_description; ?>.
    </div>
    <div style="font-size: 150%; color: #282828; padding-left: 0.8em; line-height: 1.4em">
      <?php echo $go_back; ?>
    </div>
  </body>
</html>
