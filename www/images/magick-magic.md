#Bilder aus PDF

Erzeugt auto-crop-Bilder aus dem Anleitungs-PDF:
```
convert -density 300 "nashorn-anleitung.pdf" -trim -quality 100  "trim-%03d.png"
```
Mit trim, falls Überschriften und Seitenzahlen vorhanden sind:
```
convert -density 300 "nashorn-anleitung.pdf[1-2]" -crop 3000x1330+185+600 -quality 100  "screens-%03d.png"
```
