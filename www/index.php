<?php
  // nashorn-Funktionen einbinden:
  require '#_functions_nashorn.php';
  require '#_functions_common.php';
  read_status_txt();
  declare_ip_fqdn();
  create_ajax();

  // Variable für JavaScript setzen
  // (wird in diesem Script bis jetzt nur bei deaktiviertem JavaScript in einem meta-tag zur Weiterleitung nach Poweroff benutzt):
  $javascript = isset($_GET['js']) ? $_GET['js'] : '';
  if ( ! preg_match ( '(yes|no)', $javascript ) ) {
    $javascript = 'undefined';
  }

  // dafür sorgen, dass Einstellungen aufgeklappt sind, falls gewünscht (index.php?open=settings):
  $open_settings = ' ';
  $open_idevice  = ' ';
  if ( isset( $_GET['open'] ) ) {
    if ( strstr( $_GET['open'], 'settings' ) ) {
      $open_settings = ' open=""';
    }
    if ( strstr( $_GET['open'], 'idevice' ) ) {
      $open_idevice = ' open=""';
    }
  }

  // String $ajax am Delimiter '|' in einzelne Array-Elemente teilen:
  $ajax_array = explode ( '|', $ajax );

  // vereinfachte Namensgebung:
  // (Notiz: Array-Elemente 0 und 1 sind $ip und $fqdn. Die sind global definiert und müssen hier nicht erneut zugewiesen werden)
  $backup_hour_options       = create_backup_hour_options();
  $backup_link               = $ajax_array['2'];
  $number_of_backups         = $ajax_array['3'];
  $date_last_complete_backup = $ajax_array['4'];
  $update_link               = $ajax_array['5'];
  $date_last_update          = $ajax_array['6'];  // Zeitstempel der letzten Suche nach einer Aktualisierung
  $pcent_dataPartition       = $ajax_array['7'];
  $pcent_backup              = $ajax_array['8'];
  $software_commit_link      = $ajax_array['9'];
  $uptime                    = $ajax_array['10'];
  $poweroff_link             = $ajax_array['11'];
  $poweroff_redirect_flag    = $ajax_array['12'];
  $loadavg                   = str_replace(' ', ' / ', $ajax_array['13']).' (%)';
  $network_received          = $ajax_array['14'];
  $network_transmitted       = $ajax_array['15'];
  $software_version_link     = $ajax_array['16'];
  $ytdl_version              = $ajax_array['17'];
  $used_dataPartition        = $ajax_array['18'];
  $size_dataPartition        = $ajax_array['19'];
  $used_backup               = $ajax_array['20'];
  $size_backup               = $ajax_array['21'];
  $autoupdatecheckbox        = $ajax_array['22'];
  $display_message_container = $ajax_array['23'];
  $display_setup_autoupdate  = $ajax_array['24'];
  $date_oldest_backup        = $ajax_array['25'];
  $nashorn_time              = $ajax_array['26'];
  $temperature_sata          = $ajax_array['27'];
  $temperature_usb           = $ajax_array['28'];
  $display_reboot_required   = $ajax_array['29'];
  $idevicebackup_link        = $ajax_array['30'];
  $idevicebackup_date        = $ajax_array['31'];
  $idevicebackup_return_code = $ajax_array['32'];
  $idevicebackup_unknown_password = $ajax_array['33'];

  // defaults der Schaltflächen für die noscript-Abschnitte erstellen (werden evtl. in den nächsten 4 'if'-Konditionen geändert:
  $backup_link_noscript = '<a href="remote.php?backup_now&js=no" class="buttonlike"><span class="typcn typcn-database"></span>internes Backup jetzt</a>';
  $idevicebackup_link_noscript = '<a href="remote.php?idevicebackup&js=no" class="buttonlike"><span class="typcn typcn-arrow-repeat"></span>iPhone oder iPad sichern</a>';
  $poweroff_link_noscript = '<a href="remote.php?poweroff&js=no" class="buttonlike"><span class="typcn typcn-power"></span><b>nas</b>horn herunterfahren</a>';
  $update_link_noscript = '<a href="remote.php?update&js=no" class="buttonlike"><span class="typcn typcn-download"></span><b>nas</b>horn-Update</a>';

  // Schaltflächen für die noscript-Abschnitte gegebenenfalls deaktivieren:
  if ( strncmp($backup_link, '<span', 2 ) === 0 ) {
    $backup_link_noscript = $backup_link;
  }
  if ( strncmp($idevicebackup_link, '<span', 2 ) === 0 ) {
    $idevicebackup_link_noscript = $idevicebackup_link;
  }
  if ( strncmp($poweroff_link, '<span', 2 ) === 0 ) {
    $poweroff_link_noscript = $poweroff_link;
  }
  if ( strncmp($update_link, '<span', 2 ) === 0 ) {
    $update_link_noscript = $update_link;
  }

  // iDeviceBackup: Hinweis mit Fehlercode nur einblenden, falls bei der letzten Sicherung ein Fehler aufgetreten ist:
  $display_idevicebackup_error = 'none';
  if ( $idevicebackup_return_code !== "0" ) {
    $display_idevicebackup_error = 'block';
    // Bei Sicherungsfehler sollen auch die iDevice-Settings geöffnet sein, damit Fehler sichtbar ist.
    $open_idevice = ' open=""';
  }
  // iDeviceBackup: Warnung nur einblenden, falls Sicherungspasswort unbekannt ist:
  $display_idevicebackup_unknown_password = 'none';
  if ( $idevicebackup_unknown_password === "yes" ) {
    $display_idevicebackup_unknown_password = 'block';
  }

  // Farben dem Füllzustand entsprechend definieren:
  $farbe = 'palegreen';
  if ( $pcent_dataPartition > 50 ) {
    $farbe = 'lightyellow';
  }
  if ( $pcent_dataPartition > 70 ) {
    $farbe = 'gold';
  }
  if ( $pcent_dataPartition > 85 ) {
    $farbe = 'orangered';
  }
?>

<!DOCTYPE html>
<html lang="de">

  <head>
    <title>nashorn</title>
    <meta name="apple-mobile-web-app-title" content="nashorn">
    <meta name="application-name" content="nashorn">
    <?php
      // this is to print the meta tag with the redirect to nashorn.eu, if poweroff is in progress (only for noscript version):
      if ( ( $javascript === "no" ) && ( $poweroff_redirect_flag === "poweroff_in_progress" ) ) {
        echo '<meta http-equiv="refresh" content="35; url=https://nashorn.eu/poweroff.html">';
      }
    ?>
    <!--#include virtual="/common_headers.html.inc" -->
    <link rel="stylesheet" href="<?php echo add_timestamp_to_filename('/nashorn.css'); ?>">
  </head>

  <body>
    <h1><img src="images/logo-text-raster.svg"></h1>
    <div id="pullplug">
      <p>Dein <b>nas</b>horn ist heruntergefahren.<br>Bitte ziehe den Stecker, um Strom zu sparen.<br><br><img src="images/pullplug.jpg"></p>
    </div>
    <div id="reboot_required" style="display: <?php echo $display_reboot_required; ?>;">
      <p>Bitte starte dein <b>nas</b>horn neu, um das Update abzuschließen!</p>
    </div>
    <div id="message_container" style="display: <?php echo $display_message_container; ?>;">
      <div id="setup_autoupdate" style="display: <?php echo $display_setup_autoupdate; ?>;">
        <h1>Neu: automatische Updates</h1>
        <br>
        Soll sich das <b>nas</b>horn zukünftig automatisch aktualisieren?<br><br>
        <a href="remote.php?autoupdatecheck=enable&js=no" id="activate_autoupdate" class="buttonlike" style="width: 6em; background-color: #eafff2; border-width: 4px;"><span class="typcn typcn-tick"></span>Ja</a>
        <a href="remote.php?autoupdatecheck=disable&js=no" id="deactivate_autoupdate" class="buttonlike" style="width: 6em"><span class="typcn typcn-times"></span>Nein</a><br><br>
      </div>
    </div>
    <section id="pluggedin">
      <details open="">
        <summary><span class="typcn typcn-point-of-interest"></span><b>nas</b>horn Dienste</summary>
        <p><span class="typcn typcn-folder"></span><b>Dateien</b><br>
          unterteilt in <a href="Daten"><i>Daten</i></a>, <i>Backup</i>, <i>USB-Speicher</i> und <i>Archiv</i>. Hier im Browser nur beschränkter Zugang (nur lesen), voller Zugriff über <i>SMB/Samba</i>: Siehe Anleitungen</p>
        <p><a id="itunes_link" href="http://<?php echo $ip; ?>:3689" target="_blank"><span class="typcn typcn-notes"></span>iTunes-Server</a><br>
           Mit dem iTunes-Server kannst du die Musik im Ordner <i>musik</i> auf geeignete Geräte streamen.<br>
           Stream empfangen: <a id="itunes_stream_link" href="http://<?php echo $ip; ?>:3689/stream.mp3" target="_blank">http://<span class="ip"><?php echo $ip; ?></span>:3689/stream.mp3</a></p>
        <p><span class="typcn typcn-cancel"></span>Tracking- und Werbe-Blocker: <b>DNS = <span class="ip"><?php echo $ip; ?></span></b><br>
           Du kannst das <b>nas</b>horn als Tracking- und Werbe-Blocker verwenden,
           indem du bei deinem Endgerät den Eintrag für <b>DNS</b> auf obigen Wert einstellst.
        </p>
        <p><a id="cups_link" href="https://<?php echo $ip; ?>:631" target="_blank"><span class="typcn typcn-printer"></span>Drucker-Verwaltung</a>
           <br>Hier kannst du per USB ans <b>nas</b>horn angeschlossene Drucker verwalten.</p>
        <p><a href="ytdl.php" target="_blank"><span class="typcn typcn-video"></span>Medien-Downloader</a>
           <br>Hier kannst du Online-Videos (von Youtube, Vimeo und Mediatheken) als Film oder Tondatei als Privatkopie aufs <b>nas</b>horn speichern.</p>
      </details>
      <details<?php echo $open_idevice; ?> id="idevice">
        <summary><span class="typcn typcn-device-phone"></span><span class="typcn typcn-vendor-apple"></span>
          iPhone oder iPad</summary>
        <details>
          <summary><span class="typcn typcn-key"></span> Zertifikat installieren</summary>
          <p> Tippe auf den Link: <a href="NAShorn-Zertifikat.pem">
            <span class="typcn typcn-key" style="font-size:2em"></span><b>nas</b>horn-Zertifikat</a>
              und folge der Anleitung.
          </p>
          <div class="querformatBox">
            <div id="querformat" class="querformat dragscroll">
              <div id="querformatInhalt" class="querformatInhalt">
                <div class="querformatInhaltBlock"> <img src="images/trim-001.png" loading="lazy"></div>
                <div class="querformatInhaltBlock"> <img src="images/trim-002.png" loading="lazy"></div>
                <div class="querformatInhaltBlock"> <img src="images/trim-003.png" loading="lazy"></div>
                <div class="querformatInhaltBlock"> <img src="images/trim-004.png" loading="lazy"></div>
              </div>
            </div>
            <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
              <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                <path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"></path></svg>
            </button> <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
              <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                <path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"></path></svg>
            </button>
          </div>
        </details>
        <details>
          <summary><span class="typcn typcn-folder"></span> auf Dateien zugreifen</summary>
          <div class="querformatBox">
            <div id="querformat" class="querformat dragscroll">
              <div id="querformatInhalt" class="querformatInhalt">
                <div class="querformatInhaltBlock"> <img src="images/trim-005.png" loading="lazy"></div>
                <div class="querformatInhaltBlock"> <img src="images/trim-006.png" loading="lazy"></div>
                <div class="querformatInhaltBlock"> <img src="images/trim-007.png" loading="lazy"></div>
              </div>
            </div>
            <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
              <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                <path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"></path></svg>
            </button> <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
              <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                <path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"></path></svg>
            </button>
          </div>
        </details>
        <details>
          <summary><span class="typcn typcn-business-card"></span> Adressen und Kalender synchronisieren</summary>
          <div class="querformatBox">
            <div id="querformat" class="querformat dragscroll">
              <div id="querformatInhalt" class="querformatInhalt">
                  <div class="querformatInhaltBlock"> <img src="images/trim-008.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-009.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-010.png" loading="lazy"></div>
	              <div class="querformatInhaltBlock"> <img src="images/trim-010a.png" loading="lazy"></div>
              </div>
            </div>
            <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
              <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path
                  d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"></path></svg>
            </button> <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right"
              type="button">
              <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path
                  d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"></path></svg>
            </button>
          </div>
        </details>
        <details>
          <summary><span class="typcn typcn-cancel"></span> DNS-Server nutzen als Werbe- und Trackingblocker</summary>
          <div class="querformatBox">
            <div id="querformat" class="querformat dragscroll">
              <div id="querformatInhalt" class="querformatInhalt">
                  <div class="querformatInhaltBlock"> <img src="images/trim-010b.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-010c.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-010d.png" loading="lazy"></div>
              </div>
            </div>
            <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
              <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path
                  d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"></path></svg>
            </button> <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right"
              type="button">
              <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path
                  d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"></path></svg>
            </button>
          </div>
        </details>
<!-- iDeviceBackup: Beginn noscript   -->
        <div class="myPara" id="idevicebackup_noscript" style="display: block;">
          <script>document.getElementById('idevicebackup_noscript').style.display = 'none';</script>
          <p><?php echo $idevicebackup_link_noscript; ?></p>
          <p>Letzte Sicherung: <span class="nowrap"><?php echo $idevicebackup_date; ?></span></p>
          <p class="platz_oben red" style="display: <?php echo $display_idevicebackup_error; ?>;">
            &#x26A0; Bei der letzten Sicherung ist ein Fehler aufgetreten (<?php echo $idevicebackup_return_code; ?>)
          </p>
          <p class="platz_oben" style="display: <?php echo $display_idevicebackup_unknown_password; ?>;">
            <span class="red">&#x26A0; ACHTUNG:</span> unbekanntes Sicherungspasswort der letzten iPhone/iPad-Sicherung!<br>
            Ohne das richtige Sicherungspasswort kann die Sicherung nicht zur Wiederherstellung verwendet werden!<br>
            Um das Sicherungspasswort zu ändern oder zu überprüfen, kannst du es in die .user-config eintragen.<br>
            Falls du das Sicherungspasswort vergessen hast, <a href="https://support.apple.com/de-de/108313" target="_blank">kannst du es so zurücksetzen...</a>
          </p>
        </div>
<!-- iDeviceBackup: Ende noscript     -->
<!-- iDeviceBackup: Beginn javascript -->
        <div class="myPara" id="idevicebackup_js" style="display: none;">
          <script>document.getElementById('idevicebackup_js').style.display = 'block';</script>
          <p id="idevicebackup_link"><?php echo $idevicebackup_link; ?></p>
          <p>Letzte Sicherung: <span id="idevicebackup_date" class="nowrap"><?php echo $idevicebackup_date; ?></span></p>
          <p id="idevicebackup_error" style="display: <?php echo $display_idevicebackup_error; ?>;">
            &#x26A0; Bei der letzten Sicherung ist ein Fehler aufgetreten
            (<span id="idevicebackup_return_code"><?php echo $idevicebackup_return_code; ?></span>)
          </p>
          <p id="idevicebackup_unknown_password" style="display: <?php echo $display_idevicebackup_unknown_password; ?>;">
            <span class="red">&#x26A0; ACHTUNG:</span> unbekanntes Sicherungspasswort der letzten iPhone/iPad-Sicherung!<br>
            Ohne das richtige Sicherungspasswort kann die Sicherung nicht zur Wiederherstellung verwendet werden!<br>
            Um das Sicherungspasswort zu ändern oder zu überprüfen, kannst du es in die .user-config eintragen.<br>
            Falls du das Sicherungspasswort vergessen hast, <a href="https://support.apple.com/de-de/108313" target="_blank">kannst du es so zurücksetzen...</a>
          </p>
        </div>
<!-- iDeviceBackup: Ende javascript   -->
      </details>
      <details>
        <summary><span class="typcn typcn-device-phone"></span><span class="typcn typcn-vendor-android"></span>
          Android-Geräte</summary>
          <details>
            <summary><span class="typcn typcn-key"></span> Zertifikat installieren</summary>
            <p> Tippe auf den Link: <a href="NAShorn-Zertifikat.crt">
              <span class="typcn typcn-key" style="font-size:2em"></span><b>nas</b>horn-Zertifikat</a>
                und folge der Anleitung.
            </p>
            <div class="querformatBox">
              <div id="querformat" class="querformat dragscroll">
                <div id="querformatInhalt" class="querformatInhalt">
                  <div class="querformatInhaltBlock"> <img src="images/trim-012.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-013.png" loading="lazy"></div>
                </div>
              </div>
              <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                  <path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"></path></svg>
              </button> <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                  <path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"></path></svg>
              </button>
            </div>
          </details>
          <details>
            <summary><span class="typcn typcn-folder"></span> auf Dateien zugreifen</summary>
            <div class="querformatBox">
              <div id="querformat" class="querformat dragscroll">
                <div id="querformatInhalt" class="querformatInhalt">
                  <div class="querformatInhaltBlock"> <img src="images/trim-014.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-015.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-016.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-017.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-018.png" loading="lazy"></div>
                </div>
              </div>
              <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                  <path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"></path></svg>
              </button> <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                  <path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"></path></svg>
              </button>
            </div>
          </details>
          <details>
            <summary><span class="typcn typcn-business-card"></span> Adressen und Kalender synchronisieren</summary>
            <div class="querformatBox">
              <div id="querformat" class="querformat dragscroll">
                <div id="querformatInhalt" class="querformatInhalt">
                    <div class="querformatInhaltBlock"> <img src="images/trim-019.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-020.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-021.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-022.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-023.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-024.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-025.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-026.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-027.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-028.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-029.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-030.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-031.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-032.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-033.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-034.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-035.png" loading="lazy"></div>
                </div>
              </div>
              <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path
                    d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"></path></svg>
              </button> <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right"
                type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path
                    d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"></path></svg>
              </button>
            </div>
          </details>
      </details>
      <details>
        <summary><span class="typcn typcn-device-desktop"></span><span class="typcn typcn-vendor-apple"></span>
          MacOS-Computer</summary>
          <details>
            <summary><span class="typcn typcn-key"></span> Zertifikat installieren</summary>
            <p> Tippe auf den Link: <a href="NAShorn-Zertifikat.pem">
              <span class="typcn typcn-key" style="font-size:2em"></span><b>nas</b>horn-Zertifikat</a>
                und folge der Anleitung.
            </p>
            <div class="querformatBox">
              <div id="querformat" class="querformat dragscroll">
                <div id="querformatInhalt" class="querformatInhalt">
                  <div class="querformatInhaltBlock"> <img src="images/trim-036.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-037.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-038.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-039.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-040.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-041.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-042.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-043.png" loading="lazy"></div>
                </div>
              </div>
              <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                  <path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"></path></svg>
              </button> <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                  <path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"></path></svg>
              </button>
            </div>
          </details>
          <details>
            <summary><span class="typcn typcn-folder"></span> auf Dateien zugreifen</summary>
            <div class="querformatBox">
              <div id="querformat" class="querformat dragscroll">
                <div id="querformatInhalt" class="querformatInhalt">
                  <div class="querformatInhaltBlock"> <img src="images/trim-044.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-045.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-046.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-047.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-048.png" loading="lazy"></div>
                </div>
              </div>
              <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                  <path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"></path></svg>
              </button> <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                  <path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"></path></svg>
              </button>
            </div>
          </details>
          <details>
            <summary><span class="typcn typcn-business-card"></span> Adressen und Kalender synchronisieren</summary>
            <div class="querformatBox">
              <div id="querformat" class="querformat dragscroll">
                <div id="querformatInhalt" class="querformatInhalt">
                    <div class="querformatInhaltBlock"> <img src="images/trim-049.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-050.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-051.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-052.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-053.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-054.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-055.png" loading="lazy"></div>
                </div>
              </div>
              <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path
                    d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"></path></svg>
              </button> <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right"
                type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path
                    d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"></path></svg>
              </button>
            </div>
          </details>
      </details>
      <details>
        <summary><span class="typcn typcn-device-desktop"></span><span class="typcn typcn-user"></span>
          Linux- und andere Computer</summary>
          <details>
            <summary><span class="typcn typcn-key"></span> Zertifikat installieren</summary>
            <p> Wenn du mit dem Browser folgenden Link: <a href="NAShorn-Zertifikat.pem">
              <span class="typcn typcn-key" style="font-size:2em"></span><b>nas</b>horn-Zertifikat</a>
              öffnest, so wird – je nach Browser – dieses Zertifikat systemweit oder auch nur für den Browser selbst
              installiert. Das Zertifikat ermöglicht eine sichere Verbindung (erkennbar am <i>https</i> statt <i>http</i> in der Adresse)
              zwischen <b>nas</b>horn und deinem Endgerät.<br>
              Außerdem ist das Zertifikat Voraussetzung für die Synchronisierung von Kalendern und Adressbüchern per CardDAV und CalDAV.
              Je nach verwendetem Programm muss dazu das Zertifikat entweder systemweit oder im genutzten Programm installiert werden.
            </p>
          </details>
          <details>
            <summary><span class="typcn typcn-folder"></span> auf Dateien zugreifen</summary>
            <p>
              Die Dateien werden mit dem Standard-Dienst Samba (auch als SMB oder CIFS bekannt) vom <b>nas</b>horn ausgeliefert.
              Du kannst jeden Datei-Browser/Datei-Manager verwenden, der diesen Dienst beherrscht. Die Adresse für die Dateien
              wird durch das vorangestellte <i>smb://</i> gekennzeichnet, auf den meisten Endgeräten funktioniert der folgende
              Link: <a href="smb://nashorn.local/">smb://nashorn.local/</a><br>
              Für die Verbindung benötigst du den Benutzernamen <i>nashorn</i> und dein <b>nas</b>horn-Passwort.<br>
              Für verschiedene Zwecke gibt es verschiedene Bereiche: <br>
              <ul>
                <li>Daten: Hier kannst du deine Dateien, wie Dokumente, Filme, Fotos, Musikdateien, ablegen. Dafür sind
                  schon verschiedene Ordner angelegt: <i>filme</i>, <i>musik</i> und <i>bilder</i>.
                <li>Backup: Diesen Bereich nutze für Backup-Programme auf deinem Endgerät als Ziel für Datensicherungen.
                <li>Internes Archiv: Hier findest du die Sicherungen, die das <b>nas</b>horn auf einer zweiten Festplatte erzeugt.
                  Deine Daten werden auch in älteren Versionsständen gesichert (täglich für die vergangene Woche, wöchentlich für
                  den vergangenen Monat, danach monatlich). Deine Sicherungen aus dem Bereich <i>Backup</i> werden nur kopiert,
                  da Backup-Programme selbst sich um ältere Versionsstände kümmern.
                <li>USB-Speicher: Wenn du einen USB-Stick oder eine USB-Festplatte an das <b>nas</b>horn anschließt, so kannst du Hier
                  darauf zugreifen.
              </ul>
            </p>
          </details>
          <details>
            <summary><span class="typcn typcn-business-card"></span> Adressen und Kalender synchronisieren</summary>
            <p>
              Die Synchronisierung von Adressen benutzt das Standard-Protokoll <b>CardDAV</b>. Die Synchronisierung von Kalendern
              benutzt das Standard-Protokoll <b>CalDAV</b>. Du kannst also jedes Programm, das diese Standards unterstützt,
              zur Synchronisierung deiner Adressen oder Kalender verwenden.<br>
              Ein Beispiel ist das Programm <b>Thunderbird</b> von Mozilla. Für dieses Programm gibt es AddOns,
              die die CardDAV- und CalDAV-Fuktionen hinzufügen. Für die Installation kannst du sinngemäß die Anleitung
              für Windows-Computer (untenstehend) verwenden, wobei du für die Installation von Thunderbird selbst
              deinen App-Manager oder Paket-Manager bevorzugt verwenden solltest. Die Zertifikat- und die AddOn-Installation
              erfolgt ähnlich der Thunderbird-Variante für Windows.
            </p>
          </details>
      </details>
      <details>
        <summary><span class="typcn typcn-device-desktop"></span><span class="typcn typcn-vendor-microsoft"></span>
          Windows-Computer</summary>
          <details>
            <summary><span class="typcn typcn-key"></span> Zertifikat installieren (Firefox)</summary>
            <p> Tippe auf den Link: <a href="NAShorn-Zertifikat.pem">
              <span class="typcn typcn-key" style="font-size:2em"></span><b>nas</b>horn-Zertifikat</a>
                und folge der Anleitung.
            </p>
            <div class="querformatBox">
              <div id="querformat" class="querformat dragscroll">
                <div id="querformatInhalt" class="querformatInhalt">
                  <div class="querformatInhaltBlock"> <img src="images/trim-056.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-057.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-058.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-059.png" loading="lazy"></div>
	              <div class="querformatInhaltBlock"> <img src="images/trim-059a.png" loading="lazy"></div>
                </div>
              </div>
              <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                  <path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"></path></svg>
              </button> <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                  <path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"></path></svg>
              </button>
            </div>
          </details>
          <details>
            <summary><span class="typcn typcn-folder"></span> auf Dateien zugreifen</summary>
            <div class="querformatBox">
              <div id="querformat" class="querformat dragscroll">
                <div id="querformatInhalt" class="querformatInhalt">
                  <div class="querformatInhaltBlock"> <img src="images/trim-060.png" loading="lazy"></div>
                  <div class="querformatInhaltBlock"> <img src="images/trim-061.png" loading="lazy"></div>
                </div>
              </div>
              <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                  <path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"></path></svg>
              </button> <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                  <path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"></path></svg>
              </button>
            </div>
          </details>
          <details>
            <summary><span class="typcn typcn-business-card"></span> Adressen und Kalender synchronisieren</summary>
            <div class="querformatBox">
              <div id="querformat" class="querformat dragscroll">
                <div id="querformatInhalt" class="querformatInhalt">
                    <div class="querformatInhaltBlock"> <img src="images/trim-062.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-063.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-064.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-065.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-066.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-067.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-068.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-069.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-070.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-071.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-072.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-073.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-074.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-075.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-076.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-077.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-078.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-079.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-080.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-081.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-082.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-083.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-084.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-085.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-086.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-087.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-088.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-089.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-090.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-091.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-092.png" loading="lazy"></div>
                    <div class="querformatInhaltBlock"> <img src="images/trim-093.png" loading="lazy"></div>
                </div>
              </div>
              <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path
                    d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"></path></svg>
              </button> <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right"
                type="button">
                <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path
                    d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"></path></svg>
              </button>
            </div>
          </details>
      </details>

<!-- Beginn noscript -->

      <details id="settings_noscript" style="display: block;"<?php echo $open_settings; ?>>
        <script>document.getElementById('settings_noscript').style.display = 'none';</script>
        <summary><span class="typcn typcn-spanner"></span> <b>nas</b>horn-Einstellungen</summary>
        <p><a href="remote.php?restart_itunes_server&js=no" class="buttonlike"><span class="typcn typcn-arrow-sync"></span>iTunes-Server neustarten</a>
        </p>
        <div class="myPara">
          <form id="backup_hour_form_noscript" action="remote.php" method="get"> <label form="backup_hour_form_noscript">
            <span class="typcn typcn-archive"></span> Tägliche Sicherung von Daten und Backup ins Archiv<br>um:&nbsp;
            <select class="select-css" id="hour_noscript" name="backup_hour">
              <?php echo $backup_hour_options; ?>
            </select>
            <input type="hidden" name="js" value="no">
            </label><button class="button" type="submit">Speichern</button>
            oder <?php echo $backup_link_noscript; ?><br>
            <i>Wähle eine Uhrzeit aus, zu der <b>nas</b>horn eingeschaltet ist, aber nicht intensiv verwendet wird.</i><br><br>
            <table class="backup_tabelle">
              <?php if ( $date_oldest_backup !== 'undefined' ) { ?>
              <tr><td>Datum des ältesten Backups:</td><td class="nowrap"><?php echo $date_oldest_backup; ?> Uhr</td></tr>
              <?php } ?>
              <tr><td>Anzahl vollständiger Backups im Archiv:</td><td><?php echo $number_of_backups; ?></td></tr>
              <tr><td>Datum des letzten vollständigen Backups:</td><td class="nowrap"><?php echo $date_last_complete_backup; ?></td></tr>
            </table>
          </form>
        </div>
        <p><b>nas</b>horn kann von unserer Software-Website im Internet neue Updates herunterladen und installieren.<br>
          <?php echo $update_link_noscript; ?>
          <a href="remote.php?autoupdatecheck=toggle&js=no" class="buttonlike"><span class="typcn typcn-<?php echo $autoupdatecheckbox; ?>"></span> automatisch täglich nach Updates schauen</a>
          <span class="nowrap">
            Letzte Überprüfung: <?php echo $date_last_update; ?>
          </span>
        </p>
        <details>
          <summary><span class="typcn typcn-info-large"></span><b>nas</b>horn Status</summary>
          <table class="status_tabelle">
            <tr><td><b>nas</b>horn Systemzeit:</td><td><?php echo $nashorn_time; ?> Uhr</td></tr>
            <tr><td><b>nas</b>horn IP-Adresse:</td><td class="ip"><?php echo $ip; ?></td></tr>
            <tr><td><b>nas</b>horn Domain-Namen:</td><td id="fqdn"><?php echo (str_replace( ' ', '<br>', $fqdn) ); ?></td></tr>
            <tr><td>Belegung Haupt-Festplatte:</td><td><?php echo $pcent_dataPartition; ?>% (<?php echo $used_dataPartition.'GB von '.$size_dataPartition.'GB'; ?>)</td></tr>
            <tr><td>Belegung Archiv-Festplatte:</td><td><?php echo $pcent_backup; ?>% (<?php echo $used_backup.'GB von '.$size_backup.'GB'; ?>)</td></tr>
            <tr><td>Temperatur Haupt-Festplatte:</td><td><nobr><?php echo $temperature_sata; ?></nobr></td></tr>
            <tr><td>Temperatur Archiv-Festplatte:</td><td><nobr><?php echo $temperature_usb; ?></nobr></td></tr>
            <tr><td>Prozessorlast (1/5/15 min):</td><td><?php echo $loadavg; ?></td></tr>
            <!-- Anzeige der Netzwerklast macht kaum Sinn für noscript-Version,
                 da nur Total angezeigt werden kann (und kein Durchschnitt wie z.B.: KiB/s):
                 <tr><td>Netzwerklast (Eing/Ausg):</td><td><?php echo $network_received.' / '.$network_transmitted; ?></td></tr>
            -->
            <tr><td>Uptime:</td><td><?php echo $uptime; ?></td></tr>
            <tr><td>Software-Version:</td><td><?php echo $software_version_link; ?></td></tr>
            <tr><td>Software-Commit:</td><td><?php echo $software_commit_link; ?></td></tr>
            <tr><td>y-dlp Version:</td><td><?php echo $ytdl_version; ?></td></tr>
            <tr><td colspan="2"><a href="remote.php?zip_logfiles&js=no">Abruf der Log-Dateien (ZIP-Datei)</a></td></tr>
          </table>
          <p><!-- it seems, that this empty paragraph needs to stay here for correct spacing to next paragraph --></p>
        </details>
        <p><?php echo $poweroff_link_noscript; ?>
           <!-- deactivated until U-Boot for EspressoBin is fixed:
             <a href="remote.php?reboot&js=no" class="buttonlike">
               <span class="typcn typcn-flash"></span><b>nas</b>horn neustarten
             </a>
           -->
        </p>
	<details>
	  <summary><span class="typcn typcn-briefcase"></span>Legal</summary>
	  <p>Alle Icons auf den <b>nas</b>horn-Seiten sind <a href="https://www.s-ings.com/typicons/" target="_blank">Typicons</a>, herzlichen Dank an Stephen Hutchings! (<a href="font/Typicons-Licence.txt">Lizenz</a>)
	  </p>
	  <p>Die Schriftart auf den <b>nas</b>horn-Seiten ist Atkinson-Hyperlegible. (<a href="font/Atkinson-Hyperlegible/Atkinson-Hyperlegible-Font-License-2020-1104.pdf">Lizenz</a>)
	  </p>
	  <p>iPhone, iPad, Android, MacOS, Linux, Windows und die Logos sind eingetragene Markenzeichen der jeweiligen Hersteller.
	  </p>
	  <p>Das <b>nas</b>horn verwendet Open Source Software (quelloffene Software), die frei verwendet werden darf. Die von der Software verwendeten Lizenzen finden sich auf dem <b>nas</b>horn (Zugang über SSH).
	  </p>
	</details>
      </details>

<!-- Ende noscript -->

<!-- Beginn JavaScript -->

      <details id="settings_js" style="display: none;"<?php echo $open_settings; ?>>
        <script>document.getElementById("settings_js").style.display = "block";</script>
        <summary><span class="typcn typcn-spanner"></span> <b>nas</b>horn-Einstellungen</summary>
          <p><a href="remote.php?restart_itunes_server&js=yes" target="dummy_result" class="buttonlike">
               <span class="typcn typcn-arrow-sync"></span>iTunes-Server neustarten</a>
          </p>
        <div class="myPara">
          <form id="backup_hour_form" action="remote.php" method="get" target="dummy_result_form"> <label form="backup_hour_form">
            <span class="typcn typcn-archive"></span> Tägliche Sicherung von Daten und Backup ins Archiv<br>um:&nbsp;
            <select class="select-css" id="hour" name="backup_hour" onchange="this.form.submit()">
              <?php echo $backup_hour_options; ?>
            </select>
            <iframe name="dummy_result_form" style="display:none" onload="if (typeof loadAjax === 'function') { loadAjax(); }"></iframe>
            </label>
            oder <span id="backup_link"><?php echo $backup_link; ?></span><br>
            <i>Wähle eine Uhrzeit aus, zu der <b>nas</b>horn eingeschaltet ist, aber nicht intensiv verwendet wird.</i><br><br>
            <table class="backup_tabelle">
              <tr id="display_oldest_backup" <?php if ( $date_oldest_backup === 'undefined' ) { echo 'style="display: none;"'; } ?>>
                <td>Datum des ältesten Backups:</td><td id="date_oldest_backup" class="nowrap"><?php echo $date_oldest_backup; ?> Uhr</td>
              </tr>
              <tr><td>Anzahl vollständiger Backups im Archiv:</td><td id="number_of_backups"><?php echo $number_of_backups; ?></td></tr>
              <tr><td>Datum des letzten vollständigen Backups:</td><td id="date_last_complete_backup" class="nowrap"><?php echo $date_last_complete_backup; ?></td></tr>
            </table>
          </form>
        </div>
        <p><b>nas</b>horn kann von unserer Software-Website im Internet neue Updates herunterladen und installieren.<br>
          <span id="update_link"><?php echo $update_link; ?></span>
          <a href="remote.php?autoupdatecheck=toggle&js=yes" target="dummy_result" class="buttonlike" id="autoupdate"><span id="autoupdatecheckbox" class="typcn typcn-<?php echo $autoupdatecheckbox; ?>"></span> automatisch täglich nach Updates schauen</a>
          <span class="nowrap">
            Letzte Überprüfung: <span id="date_last_update"><?php echo $date_last_update; ?></span>
          </span>
        </p>
        <details>
          <summary><span class="typcn typcn-info-large"></span><b>nas</b>horn Status</summary>
          <table class="status_tabelle">
            <tr><td><b>nas</b>horn Systemzeit:</td><td><span id="nashorn_time"><?php echo $nashorn_time; ?></span> Uhr</td></tr>
            <tr><td><b>nas</b>horn IP-Adresse:</td><td class="ip"><?php echo $ip; ?></td></tr>
            <tr><td><b>nas</b>horn Domain-Namen:</td><td id="fqdn"><?php echo (str_replace( ' ', '<br>', $fqdn) ); ?></td></tr>
            <tr><td>Belegung Haupt-Festplatte:</td><td id="used_space_dataPartition"><?php echo $pcent_dataPartition; ?>% <nobr>(<?php echo $used_dataPartition.' von '.$size_dataPartition.' GB'; ?>)</nobr></td></tr>
            <tr><td>Belegung Archiv-Festplatte:</td><td id="used_space_backup"><?php echo $pcent_backup; ?>% <nobr>(<?php echo $used_backup.' von '.$size_backup.' GB'; ?>)</nobr></td></tr>
            <tr><td>Temperatur Haupt-Festplatte:</td><td id="temperature_sata"><nobr><?php echo $temperature_sata; ?></nobr></td></tr>
            <tr><td>Temperatur Archiv-Festplatte:</td><td id="temperature_usb"><nobr><?php echo $temperature_usb; ?></nobr></td></tr>
            <tr><td>Prozessorlast (1/5/15 min):</td><td id="loadavg"><?php echo $loadavg; ?></td></tr>
            <tr><td>Netzwerklast (Eing/Ausg):</td><td id="traffic">0 / 0 (KiB/s)</td></tr>
            <tr><td>Uptime:</td><td id="uptime"><?php echo $uptime; ?></td></tr>
            <tr><td>Software-Version:</td><td id="software_version"><?php echo $software_version_link; ?></td></tr>
            <tr><td>Software-Commit:</td><td id="software_commit_link"><?php echo $software_commit_link; ?></td></tr>
            <tr><td>yt-dlp Version:</td><td id="ytdl_version"><?php echo $ytdl_version; ?></td></tr>
            <tr><td colspan="2" id="zip_logfiles"><a href="remote.php?zip_logfiles&js=yes">Abruf der Log-Dateien (ZIP-Datei)</a></td></tr>
          </table>
          <p><!-- it seems, that this empty paragraph needs to stay here for correct spacing to next paragraph --></p>
        </details>
        <p><span id="poweroff_link"><?php echo $poweroff_link; ?></span>
           <!-- deactivated until U-Boot for EspressoBin is fixed:
             <a href="remote.php?reboot&js=yes" target="dummy_result" class="buttonlike">
               <span class="typcn typcn-flash"></span><b>nas</b>horn neustarten
             </a>
           -->
        </p>
        <iframe name="dummy_result" style="display:none" onload="if (typeof loadAjax === 'function') { loadAjax(); }"></iframe>
	<details>
	  <summary><span class="typcn typcn-briefcase"></span>Legal</summary>
	  <p>Alle Icons auf den <b>nas</b>horn-Seiten sind <a href="https://www.s-ings.com/typicons/" target="_blank">Typicons</a>, herzlichen Dank an Stephen Hutchings! (<a href="font/Typicons-Licence.txt">Lizenz</a>)
	  </p>
	  <p>Die Schriftart auf den <b>nas</b>horn-Seiten ist Atkinson-Hyperlegible. (<a href="font/Atkinson-Hyperlegible/Atkinson-Hyperlegible-Font-License-2020-1104.pdf">Lizenz</a>)
	  </p>
	  <p>iPhone, iPad, Android, MacOS, Linux, Windows und die Logos sind eingetragene Markenzeichen der jeweiligen Hersteller.
	  </p>
	  <p>Das <b>nas</b>horn verwendet Open Source Software (quelloffene Software), die frei verwendet werden darf. Die von der Software verwendeten Lizenzen finden sich auf dem <b>nas</b>horn (Zugang über SSH).
	  </p>
	</details>
      </details>

<!-- Ende JavaScript -->

    </section>
    <div class="balkenrahmen" style="background-color: <?php echo $farbe; ?>;">
      <div class="balken" style="width: <?php echo $pcent_dataPartition; ?>%;"></div>
    </div>
    <script language="javascript" src="<?php echo add_timestamp_to_filename('/nashorn.js'); ?>"></script>
  </body>
</html>
