var SETTINGS = {
	navBarTravelling: false,
	navBarTravelDirection: "",
	navBarTravelDistance: 300
}

//        document.documentElement.classList.remove("no-js");
//        document.documentElement.classList.add("js");

// Out advancer buttons
var pnAdvancerLeft = document.getElementById("pnAdvancerLeft");
var pnAdvancerRight = document.getElementById("pnAdvancerRight");
var pnProductNav = document.getElementById("querformat");
var pnProductNavContents = document.getElementById("querformatInhalt");

pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));

// Handle the scroll of the horizontal container
var last_known_scroll_position = 0;
var ticking = false;

function doSomething(scroll_pos) {
	pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
}

pnProductNav.addEventListener("scroll", function() {
	last_known_scroll_position = window.scrollY;
	if (!ticking) {
		window.requestAnimationFrame(function() {
			doSomething(last_known_scroll_position);
			ticking = false;
		});
	}
	ticking = true;
});


pnAdvancerLeft.addEventListener("click", function() {
	// If in the middle of a move return
	if (SETTINGS.navBarTravelling === true) {
		return;
	}
	// If we have content overflowing both sides or on the left
	if (determineOverflow(pnProductNavContents, pnProductNav) === "left" || determineOverflow(pnProductNavContents, pnProductNav) === "both") {
		// Find how far this panel has been scrolled
		var availableScrollLeft = pnProductNav.scrollLeft;
		// If the space available is less than two lots of our desired distance, just move the whole amount
		// otherwise, move by the amount in the settings
		if (availableScrollLeft < SETTINGS.navBarTravelDistance * 2) {
			pnProductNavContents.style.transform = "translateX(" + availableScrollLeft + "px)";
		} else {
			pnProductNavContents.style.transform = "translateX(" + SETTINGS.navBarTravelDistance + "px)";
		}
		// We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
		pnProductNavContents.classList.remove("querformatInhalt-no-transition");
		// Update our settings
		SETTINGS.navBarTravelDirection = "left";
		SETTINGS.navBarTravelling = true;
	}
	// Now update the attribute in the DOM
	pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
});

pnAdvancerRight.addEventListener("click", function() {
	// If in the middle of a move return
	if (SETTINGS.navBarTravelling === true) {
		return;
	}
	// If we have content overflowing both sides or on the right
	if (determineOverflow(pnProductNavContents, pnProductNav) === "right" || determineOverflow(pnProductNavContents, pnProductNav) === "both") {
		// Get the right edge of the container and content
		var navBarRightEdge = pnProductNavContents.getBoundingClientRect().right;
		var navBarScrollerRightEdge = pnProductNav.getBoundingClientRect().right;
		// Now we know how much space we have available to scroll
		var availableScrollRight = Math.floor(navBarRightEdge - navBarScrollerRightEdge);
		// If the space available is less than two lots of our desired distance, just move the whole amount
		// otherwise, move by the amount in the settings
		if (availableScrollRight < SETTINGS.navBarTravelDistance * 2) {
			pnProductNavContents.style.transform = "translateX(-" + availableScrollRight + "px)";
		} else {
			pnProductNavContents.style.transform = "translateX(-" + SETTINGS.navBarTravelDistance + "px)";
		}
		// We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
		pnProductNavContents.classList.remove("querformatInhalt-no-transition");
		// Update our settings
		SETTINGS.navBarTravelDirection = "right";
		SETTINGS.navBarTravelling = true;
	}
	// Now update the attribute in the DOM
	pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
});

pnProductNavContents.addEventListener(
"transitionend",
function() {
	// get the value of the transform, apply that to the current scroll position (so get the scroll pos first) and then remove the transform
	var styleOfTransform = window.getComputedStyle(pnProductNavContents, null);
	var tr = styleOfTransform.getPropertyValue("-webkit-transform") || styleOfTransform.getPropertyValue("transform");
	// If there is no transition we want to default to 0 and not null
	var amount = Math.abs(parseInt(tr.split(",")[4]) || 0);
	pnProductNavContents.style.transform = "none";
	pnProductNavContents.classList.add("querformatInhalt-no-transition");
	// Now lets set the scroll position
	if (SETTINGS.navBarTravelDirection === "left") {
		pnProductNav.scrollLeft = pnProductNav.scrollLeft - amount;
	} else {
		pnProductNav.scrollLeft = pnProductNav.scrollLeft + amount;
	}
	SETTINGS.navBarTravelling = false;
},
false
);

function determineOverflow(content, container) {
	var containerMetrics = container.getBoundingClientRect();
	var containerMetricsRight = Math.floor(containerMetrics.right);
	var containerMetricsLeft = Math.floor(containerMetrics.left);
	var contentMetrics = content.getBoundingClientRect();
	var contentMetricsRight = Math.floor(contentMetrics.right);
	var contentMetricsLeft = Math.floor(contentMetrics.left);
	if (containerMetricsLeft > contentMetricsLeft && containerMetricsRight < contentMetricsRight) {
		return "both";
	} else if (contentMetricsLeft < containerMetricsLeft) {
		return "left";
	} else if (contentMetricsRight > containerMetricsRight) {
		return "right";
	} else {
		return "none";
	}
}


/***************************************************************
**  START: alle 3 Sekunden über AJAX diejenigen Werte anfordern, die sich verändern können
***************************************************************/

// Funktion zum Füllen des Balkens beim Herunterfahren:
var dummy = 0;
function fill_poweroff() {
  if ( dummy == 0 ) {
    dummy = 1;
    var elem = document.getElementById("poweroff_link");
    var width = -5;
    var width_white = width + 10;
    // alle 0,35 Sekunden wird der Balken um 1% nach rechts verschoben (entspricht 35 Sekunden Poweroffzeit):
    var id = setInterval(fillbar, 350);
    function fillbar() {
      if ( width >= 100 ) {
        // dummy auf 2 setzen, damit Balken beim Reload von Ajax nicht entfernt und von vorne begonnen wird:
        dummy = 2;
        elem.innerHTML = '<span class="buttonlike disabled"><span class="typcn typcn-power"></span><b>nas</b>horn ist ausgeschaltet.</span>';
        clearInterval(id);
        document.getElementById("pluggedin").style.display = 'none';
        document.getElementById("pullplug").style.display = 'block';
      } else {
        width++;
        if ( width_white < 100 ) { width_white++; }
        elem.style.backgroundImage = 'linear-gradient(90deg, #DCF0DC ' + width + '%, white ' + width_white + '%)';
      }
    }
  }
}

function loadAjax() {

  // neues http-Request-Objekt erstellen:
  var xhttp = new XMLHttpRequest();

  // Funktion definieren, die ausgeführt wird, wenn sich der Status der empfangenen Daten ändert:
  xhttp.onreadystatechange = function() {

    // wenn die Antwort des Requests bereit steht (4) und http-Code 200 (OK) übergeben wurde:
    if (this.readyState == 4 && this.status == 200) {

      // Antwort des Requests (Ausgabe von 'ajax.php') in Array aufsplitten:
      var textarray = this.responseText.split("|");

      // Farbe für Balken festlegen, der die Belegung der dataPartion anzeigt:
      var farbe = "palegreen";
      if (textarray[7] > 50) {
        farbe = "lightyellow";
      }
      if (textarray[7] > 70) {
        farbe = "gold";
      }
      if (textarray[7] > 85) {
        farbe = "orangered";
      }

      //  0: alle Elemente, die die IP anzeigen sollen (class="ip") mit der IP bestücken:
      var ip_items = document.getElementsByClassName("ip"), i , len;
      for (i = 0, len = ip_items.length; i < len; i++) {
        ip_items[i].innerHTML = textarray[0];
      }

      //  0: Link zum Webfrontend von cups erzeugen (https://IP.IP.IP.IP:631):
      document.getElementById("cups_link").href = "https://" + textarray[0] + ":631";

      //  1: Link zum Webfrontend von owntone (iTunes-Server) erzeugen (http://FQDN:3689):
      document.getElementById("itunes_link").href = "http://" + textarray[0] + ":3689";
      //     Link zum iTunes-Stream von owntone erzeugen:
      document.getElementById("itunes_stream_link").href = "http://" + textarray[0] + ":3689/stream.mp3";

      // iDeviceBackup-Abteilung:
      // 30: Link, um ein iDeviceBackup zu starten:
      document.getElementById("idevicebackup_link").innerHTML = textarray[30];
      // 31: Datum des letzten iDeviceBackups (wird nur angezeigt, falls schon eine Sicherung erfolgte):
      document.getElementById("idevicebackup_date").innerHTML = textarray[31];
      // 32: return_code von iDeviceBackup - Fehlermeldung wird nur angezeigt, falls return_code > 0:
      if (textarray[32] != "0") {
        // Fehlercode in Sicherungsfehlernachricht einfügen:
        document.getElementById("idevicebackup_return_code").innerHTML = textarray[32];
        // ...und die Sichtbarkeit der Sicherungsfehlernachricht auf block ändern:
        document.getElementById("idevicebackup_error").style.display = 'block';
        // bei Sicherungsfehler, sollen die iDevice-Settings aufgeklappt werden:
        document.getElementById("idevice").open = true;
      } else {
        // Fehlernachricht ausblenden, falls kein Fehler mehr existiert:
        document.getElementById("idevicebackup_error").style.display = 'none';
        // Notiz: die iDevice-Settings nicht zuklappen, sonst werden sie (falls sie manuell geöffnet
        //        werden) alle 3 Sekunden zu geklappt.
      }
      // 33: Sicherungspasswort unbekannt - nur Warnung anzeigen, falls yes:
      if (textarray[33] == "yes") {
        document.getElementById("idevicebackup_unknown_password").style.display = 'block';
      } else {
        document.getElementById("idevicebackup_unknown_password").style.display = 'none';
      }

      // Backup-Abteilung:
      //  2: Link, um sofort ein internes Backup zu erstellen:
      document.getElementById("backup_link").innerHTML = textarray[2];
      //  3: Anzahl vollständiger Backups auf der internen Backup-Platte:
      document.getElementById("number_of_backups").innerHTML = textarray[3];
      //  4: Datum des letzten vollständigen internen Backups:
      document.getElementById("date_last_complete_backup").innerHTML = textarray[4];
      // 25: Datum des ältesten internen Backups (soll nur angezeigt werden, wenn schon ein ältestes Backup existiert):
      if ( textarray[25] !== "undefined" ) {
        document.getElementById("date_oldest_backup").innerHTML = textarray[25] + " Uhr";
        document.getElementById("display_oldest_backup").style.display = 'table-row';
      } else {
        document.getElementById("display_oldest_backup").style.display = 'none';
      }

      // Update-Abteilung:
      // 26: NAShorn-Systemzeit:
      document.getElementById("nashorn_time").innerHTML = textarray[26];
      //  5: Link, um ein Update der Software zu starten (Aufruf von update_nashorn.sh):
      document.getElementById("update_link").innerHTML = textarray[5];
      //  6: Datum des letzten Aufrufs von update_nashorn.sh
      document.getElementById("date_last_update").innerHTML = textarray[6];
      // 22: Status der Checkbox für Autoupdate (angehakt oder nicht, typcn-...):
      document.getElementById("autoupdatecheckbox").className = 'typcn typcn-' + textarray[22];
      // 23: Sichtbarkeit des Containers mit Benachrichtigungen:
      document.getElementById("message_container").style.display = textarray[23];
      // 24: Sichtbarkeit der Box für das initiale Setup von autoupdate:
      document.getElementById("setup_autoupdate").style.display = textarray[24];
      // 29: Sichtbarkeit der Box für die Nachricht, dass Neustart erforderlich ist:
      document.getElementById("reboot_required").style.display = textarray[29];

      // Status-Abteilung:
      //  1: nashorn Domain-Namen (Leerzeichen der Domainliste mit <br> ersetzt):
      document.getElementById("fqdn").innerHTML = textarray[1].replace(/ /g, '<br>');
      //  7, 18, 19: Belegung Haupt-Festplatte (dataPartition):
      document.getElementById("used_space_dataPartition").innerHTML = textarray[7] + '% <nobr>(' + textarray[18] + ' von ' + textarray[19] + ' GB)</nobr>';
      //  8, 20, 21: Belegung Archiv-Festplatte (backup):
      document.getElementById("used_space_backup").innerHTML = textarray[8] + '% <nobr>(' + textarray[20] + ' von ' + textarray[21] + ' GB)</nobr>';
      // 27: Temperatur Haupt-Festplatte (sata, dataPartition):
      document.getElementById("temperature_sata").innerHTML = '<nobr>' + textarray[27] + '</nobr>';
      // 28: Temperatur Archiv-Festplatte (usb, backup):
      document.getElementById("temperature_usb").innerHTML = '<nobr>' + textarray[28] + '</nobr>';
      // 16: Software Version (human readable):
      document.getElementById("software_version").innerHTML = textarray[16];
      //  9: Software Commit mit Link zum entsprechenden Commit auf codeberg.org:
      document.getElementById("software_commit_link").innerHTML = textarray[9];
      // 17: yt-dlp Version:
      document.getElementById("ytdl_version").innerHTML = textarray[17];
      // 10: Uptime (angeschaltet seit):
      document.getElementById("uptime").innerHTML = textarray[10];
      // 13: Load-Average des Systems:
      document.getElementById("loadavg").innerHTML = textarray[13].replace(/ /g,' / ') + ' (%)';
      // 14: Netzwerklast (Received) in kb/sec (espressobin: br0 / odroid: eth0):
      rec_current = textarray[14];
      if ( typeof rec_last === 'undefined' ) { rec_last = rec_current; }
      rec_diff = rec_current - rec_last;
      rec_avg_kbs = ( rec_diff / 1024 / 3 ).toFixed(1);
      rec_avg_kbs = Intl.NumberFormat('de-DE').format(rec_avg_kbs);
      rec_last = rec_current;
      // 15: Netzwerklast (Transmitted) in kb/sec (espressobin: br0 / odroid: eth0):
      tra_current = textarray[15];
      if ( typeof tra_last === 'undefined' ) { tra_last = tra_current; }
      tra_diff = tra_current - tra_last;
      tra_avg_kbs = ( tra_diff / 1024 / 3 ).toFixed(1);
      tra_avg_kbs = Intl.NumberFormat('de-DE').format(tra_avg_kbs);
      tra_last = tra_current;
      // Ausgabe von Netzwerklast (Received/Transmitted):
      document.getElementById("traffic").innerHTML = rec_avg_kbs + ' / ' + tra_avg_kbs + ' (KiB/s)';

      // Poweroff-Abteilung:
      // 11: Link, um nashorn herunterzufahren (falls Poweroff gerade läuft):
      document.getElementById("poweroff_link").innerHTML = textarray[11];
      // 12: poweroff_in_progress-flag prüfen und ggfls. "Herunterfahr"-Balken füllen:
      if ( textarray[12] === "poweroff_in_progress" ) {
        // Poweroff läuft gerade. Den "Herunterfahren"-Balken füllen:
        fill_poweroff();
      } else {
        // falls nashorn wieder angeschaltet wurde und Locks entfernt wurden, wieder zurück zum Original:
        document.getElementById('poweroff_link').style.backgroundImage = 'none';
        document.getElementById('poweroff_link').classList.remove('buttonlike');
        document.getElementById("pluggedin").style.display = 'block';
        document.getElementById("pullplug").style.display = 'none';
        if ( dummy == 2 ) { dummy = 0; }
      }

      // Festplattenbelegungsbalken-Abteilung:
      //  7: Farbe und Breite (7) des Balkens am unteren Browser-Rand, der die Belegung der dataPartition anzeigt:
      document.getElementsByClassName("balken")[0].style.width = textarray[7] + "%";
      document.getElementsByClassName("balkenrahmen")[0].style.backgroundColor = farbe;
    }
  }

  // http-Request zusammenstellen und senden:
  xhttp.open("GET", "ajax.php", true);
  xhttp.send();
}

// alle 3 Sekunden die ajax-Werte anfordern:
setInterval('loadAjax()', 3000);

/***************************************************************
**  ENDE: alle 3 Sekunden über AJAX diejenigen Werte anfordern, die sich verändern können
***************************************************************/

// Enter und Escape erkennen, wenn setup_autoupdate angezeigt wird:
document.addEventListener("keyup", function(event) {
  // 13 = "Enter"-Taste; 27 = "Escape"-Taste
  if (event.keyCode === 13 || event.keyCode === 27) {
    // default-action verhindern (nicht nötig in unserem Fall):
    //event.preventDefault();
    // Aktion nur ausführen, falls message_container sichtaber ist:
    if (document.getElementById("message_container").style.display === "block") {
      if (event.keyCode === 13 ) {
        // bei Enter werden autoupdates aktiviert:
        document.getElementById("activate_autoupdate").click();
      }
      if (event.keyCode === 27 ) {
        // bei Escape werden autoupdates deaktiviert:
        document.getElementById("deactivate_autoupdate").click();
      }
    }
  }
});
