<?php

/************************************************
*
* PHP-Skript, das von statischer Seite aufgerufen wird, um auf nashorn Shell-Befehle auszuführen
* gebraucht für: restart_itunes_server, reboot, update, backup_now (und vielleicht noch mehr...)
* Nach dem Aufruf des entsprechenden Terminal-Befehls wird weitergeleitet zu $location (siehe unten)
*
* Um dieses PHP-Skript aufzurufen genügt ein Aufruf eines folgendermaßen gestalteten Links:
* <a href="remote.php?restart_itunes_server">iTunes-Server neustarten</a>
*
* Dieses Skript muss im selben Verzeichnis wie das nashorn-Webinterface index.php liegen.
* Ansonsten müssen die Links in index.php angepasst werden!
*
************************************************/



/////////////////////////////////////
// FUNKTION zur Ausgabe einer Feedback-Webseite
function feedback_page($title, $text, $go_back = null) {
  // gibt dem User Feedback über eine aufgerufene Funktion
  // es können drei Variablen übergeben werden: Titel, Text und Zurück-Link (mit Text) für die Webseite.
  // die dritte Variable (Zurück-Link mit Text) ist optional - falls nicht übergeben wird der Link hier zusammengestellt.

  $nashornlink = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
  $datenlink = $nashornlink.'/Daten/';
  $requesturi = $nashornlink.$_SERVER['REQUEST_URI'];

  if ( isset ($go_back) ) {
    $go_back .= PHP_EOL;
  } else {
    if ( ( strpos($_SERVER['REQUEST_URI'], '/Daten') ) === 0 ) {
      $go_back = 'Hier geht es zurück zum Datenverzeichnis:<br>'.PHP_EOL;
      $go_back .= '<a href="'.$datenlink.'">'.$datenlink.'</a>'.PHP_EOL;
    } else {
      $go_back = 'Hier geht es zurück zum <b>nas</b>horn:<br>';
      $go_back .= '<a href="'.$nashornlink.'">'.$nashornlink.'</a>'.PHP_EOL;
    }
  }

  // es folgt die Ausgabe der Webseite in HTML (angereichert mit Variablen):
  ?>

  <!DOCTYPE html>
  <html lang="de">

  <head>
    <title><?php echo $title; ?></title>
    <!--#include virtual="/common_headers.html.inc" -->
    <link rel="stylesheet" href="/nashorn.css">
  </head>

  <body>
    <div style="font-size: 2em; font-weight: bold; margin: 0.2em 0em;">
      <span class="typcn typcn-info" style="color: green; font-size: 2em; vertical-align: middle; padding-left: 0.1em;"></span>
    </div>
    <div style="font-size: 1.4em; line-height: 1.4em; font-weight: bold; margin: 0em 0em 0em 0.6em; color: #282828;">
      <?php echo $title; ?><hr>
    </div>
    <div style="font-size: 1em; line-height: 1.4em; color: #282828; padding: 0em 0em 0.8em 0.8em;">
      <?php echo $text; ?><hr>
      <div style="padding-top: 0.7em;">
        <?php echo $go_back; ?>
      </div>
    </div>
  </body>
  </html>

  <?php
  return;
}



/////////////////////////////////////
// VARIABLEN

// Pfad zu remote.sh:
$remotesh = '/root/nashorn/scripts/remote.sh';

// Ausgabe ins Logfile oder nach /dev/null umleiten:
if (is_writable('/var/www/nashorn.log.txt')) {
    // Ausgabe zu Logfile umleiten, falls Symlink zu Logfile existiert und beschreibbar ist:
    $logging_wait = '>> /var/www/nashorn.log.txt 2>&1';
} else {
    // falls Logfile aus irgendeinem Grund nicht beschreibbar sein sollte, Ausgabe zu /dev/null umleiten:
    $logging_wait = '> /dev/null 2>&1';
}
// Befehl im Hintergrund ausführen, falls PHP nicht warten soll, bis der Befehl abgearbeitet ist.
$logging_dont_wait = $logging_wait.' &';

// default location zu der nach Abarbeitung dieses Skripts weitergeleitet wird
// (davon ausgehend, dass die meisten User Javascript aktiviert haben dürften):
$location = 'index.php?js=yes&open=settings#settings_js';

// Variable für JavaScript setzen (falls JavaScript deaktiviert ist, muss auf index.php weitergeleitet werden)
$javascript = isset($_GET['js']) ? $_GET['js'] : '';
if ( ! preg_match ( '(yes|no)', $javascript ) ) {
  $javascript = 'undefined';
}
if ( $javascript === 'no' ) {
  // falls JavaScript deaktiviert ist, wird am Ende dieses Skripts wird auf diese Seite weitergeleitet.
  // Da remote.php immer mit geöffnetem Einstellungsblock in index.php aufgerufen wird, soll er beim Redirect auch gleich wieder geöffnet sein.
  // (kann im Laufe dieses Skripts geändert werden):
  $location = 'index.php?js=no&open=settings#settings_noscript';
}
// NOTIZ:
// falls $javascript auf "yes" gesetzt ist (dies ist nur bei Aufruf über index.php der Fall),
// wird am Ende dieses Skripts nur ein Zeitstempel ausgegeben, den index.php in einem versteckten(!) iframe "anzeigt".

// Zeit in Mikrosekunden, um vor der Weiterleitung zu warten bis ein Skript ein Status-Lock gesetzt hat:
$microseconds = '250000';

// Der Zurück-Link zum NAShorn mit geöffneten Settings (für die Feedback-Seite). Wird diese Variable beim Funktionsaufruf nicht übergeben, dann
// verweist der Zurück-Link nur auf $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'] - bei Aufruf aus dem Datenordner wird dann noch der jeweilige
// Pfad angehängt.
$go_back = 'Hier geht es zurück zum <b>nas</b>horn:<br>
            <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/'.$location.'">'.
            $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'</a>';



/////////////////////////////////////
// AUTOUPDATECHECK
if ( isset($_GET['autoupdatecheck']) ) {

  // regex zum Überprüfen auf gültigen Wert:
  $pattern = '/^(disable|enable|toggle)$/';
  if ( preg_match ( $pattern, $_GET['autoupdatecheck'] ) ) {
    // status der täglichen automatischen Suche nach Updates ändern:
    exec('sudo '.$remotesh.' autoupdatecheck "'.$_GET['autoupdatecheck'].'"'.$logging_dont_wait);
    // prüfen, ob dis-/enabled wurde und Weiterleitung entsprechend einrichten:
    $pattern = '/^(disable|enable)$/';
    if ( preg_match ( $pattern, $_GET['autoupdatecheck'] ) ) {
      // falls dis- oder enabled wurde, geschah das über einen Link oder Direkteingabe, daher
      // nach kurzer Wartezeit direkt zu index.php mit geöffneten Settings weiterleiten (link wird von index.php nicht in hidden form geladen):
      usleep($microseconds);
      header('location: '.$location);
      exit(0);
    }
    // bei toggle geschah dies über die Checkbox im Webinterface.
    // Die Weiterleitung verzögern, damit remote.sh den Wert in status.txt geändert hat:
    usleep($microseconds);
  }
}



/////////////////////////////////////
// BACKUP_NOW
if ( isset($_GET['backup_now']) ) {

    // Backup auf die 2. HDD anstoßen (backup-lock muss in remote.sh gesetzt werden, da in cronjob 'remote.sh backup_now'aufgerufen wird)
    exec('sudo '.$remotesh.' backup_now Webfrontend '.$logging_dont_wait);
    // die Weiterleitung verzögern, damit remote.sh das Backup-Lock in status.txt gesetzt hat:
    usleep($microseconds);
}



/////////////////////////////////////
// BACKUP_HOUR
if ( isset($_GET['backup_hour']) ) {

    // Stunde einstellen, bei der das tägliche Backup gestartet werden soll

    $hour = $_GET['backup_hour']; // nur Vereinfachung

    // regex zum Prüfen, ob gültiger Wert (00-23) übermittelt wurde:
    $pattern = '/^([01][0-9]|2[0-3])$/';
    if (preg_match($pattern, $hour)) {
        // remote.sh wird nur aufgerufen, wenn gültiger Wert übermittelt wurde.
        // NOTIZ: Backup-Hour kann nicht per Ajax aktualisiert werden, da ansonsten das Auswählen einer Stunde nicht möglich ist!
        if ( $javascript === 'yes' ) {
            exec('sudo '.$remotesh.' backup_hour '.$hour.' '.$logging_dont_wait);
        } else {
            // Falls JavaScript deaktiviert ist, abwarten bis Befehl ausgeführt ist.
            // Ansonsten wird veralteter Wert in index.php angezeigt werden.
            exec('sudo '.$remotesh.' backup_hour '.$hour.' '.$logging_wait);
        }
    }
}



/////////////////////////////////////
// CHANGE_HOSTNAME
if ( isset($_GET['change_hostname']) ) {

    // Ändert den Hostnamen des Systems nachdem der übergebene Parameter auf gültigen Hostnamen überprüft wurde.

    // alten Hostnamen ermitteln und abspeichern für die Feedback-Seite:
    $index = strpos ( $_SERVER['HTTP_HOST'], '.' );
    if ( $index === false ) {
      $old_hostname = $_SERVER['HTTP_HOST'];
      $network = '';
    } else {
      $old_hostname = substr ( $_SERVER['HTTP_HOST'], 0, $index );
      $network = substr ( $_SERVER['HTTP_HOST'], $index );
    }

    // Feedback-Seite zusammenstellen BEVOR remote.sh aufgerufen wird, damit die Feedback-Seite nach Aufruf von remote.sh möglichst
    // sofort ausgeliefert wird (bevor das nashorn den reboot in der Funktion change_hostname() ausführt)
    $title = 'Hostname geändert zu: <span style="font-style: italic; color: grey;">'.$_GET['change_hostname'].'</span>';
    $text  = '(Alter Hostname: <span style="font-style: italic; color: grey;">'.$old_hostname.')</span><br><br>
              <b>nas</b>horn WIRD JETZT NEU GESTARTET!<br><br>
              Falls das <b>nas</b>horn in 5 Minuten nicht unter dem Link unten erreichbar sein sollte,
              muss es für mindestens 10 Sekunden vom Stromnetz getrennt werden.';
    $go_back = 'Neuer Link zum <b>nas</b>horn:<br>
                <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_GET['change_hostname'].$network.'">'.
                $_SERVER['REQUEST_SCHEME'].'://'.$_GET['change_hostname'].$network.'</a>';

    // regex für gültige Hostnamen
    // (Buchstaben+Ziffern+Minuszeichen, Minuszeichen darf nicht am Anfang oder Ende stehen, 2-63 Zeichen)
    $regex_hostname = '/^[A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9]$/';
    // Falls Hostname gültig ist, remote.sh change_hostname <HOSTNAME> aufrufen:
    if ( preg_match ( $regex_hostname, $_GET['change_hostname'] ) ) {
        // übergebener Hostname ist gültig
        // NOTIZ: $output und $exitcode hier nicht nötig, da Skriptabarbeitung sowieso nicht abgewartet werden kann (wegen reboot)
        exec('sudo '.$remotesh.' change_hostname '.$_GET['change_hostname'].' Webfrontend '.$logging_dont_wait);
        // jetzt schnell vor dem Reboot die Feedback-Seite ausgeben:
        feedback_page($title, $text, $go_back);
    } else {
        // übergebener Hostname ist ungültig:
        $title = 'ungültiger Hostname';
        $text  = '<span style="color: red;">Fehler:</span> Der übergebene Hostname
                  <span style="font-style: italic; color: grey;">'.$_GET['change_hostname'].'</span> ist ungültig.<br><br>
                  Bedingungen für einen gültigen Hostnamen:<br>
                  - Maximal 63 Zeichen bestehend aus Buchstaben, Ziffern und Minuszeichen.<br>
                  - An Anfang und Ende darf kein Minuszeichen stehen.';
        // Feedback-Seite senden:
        feedback_page($title, $text);
    }

    // Feedback-Seite wurde gesendet, daher hier dieses Skript beenden:
    exit(0);
}



/////////////////////////////////////
// CHANGE_TO_LOCAL_NAMESERVER
if ( isset($_GET['change_to_local_nameserver']) ) {

    // Ändert die /etc/resolv.conf und dnsmasq-Konfiguration so, dass nur der lokale Nameserver benutzt wird (nicht dnscrypt-proxy)
    // Dies ist eine fallback-Option, falls die Namensauflösung nach Update auf dnscrypt-proxy nicht funktionieren sollte.
    // Der Link hierzu wird dann per E-Mail verschickt und soll nicht im nashorn-Webinterface auftauchen.
    // Der Weg zurück zu dnscrypt-proxy kann mit 'remote.php?run_all_upgrades' beschritten werden.

    exec('sudo '.$remotesh.' change_to_local_nameserver '.$logging_wait);

    // Feedback-Seite zusammenstellen:
    $title = 'Verschlüsseltes DNS deaktiviert!';
    $text  = '<b>nas</b>horn führt DNS-Anfragen ab sofort unverschlüsselt über den lokalen Nameserver aus.<br><br>
              Um DNS-Anfragen wieder zu verschlüsseln, muss dieser Link aufgerufen werden:<br>
              <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/remote.php?run_all_updates">'.
              $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/remote.php?run_all_updates</a>';

    // Feedback-Seite senden und Script beenden:
    feedback_page($title, $text);
    exit(0);
}



/////////////////////////////////////
// CLEAR_VTODOLIST
if ( isset($_GET['clear_vtodolist']) ) {

  // löscht alle Einträge der Erinnerungsliste 'Meldung' des system-Users, die mit nashorn user geteilt ist
  // NOTIZ: $output und $exitcode hier nicht nötig, da Skriptabarbeitung sowieso nicht abgewartet wird
  exec('sudo '.$remotesh.' clear_vtodolist '.$logging_dont_wait);

  // Ausgabe für Feedback-Seite zusammenstellen:
  $title = 'Meldungen löschen';
  $text  = 'Die Erinnerungsliste <span style="font-style: italic; color: grey;">Meldung</span> wird gelöscht.';

  /********************************************
  *
  *  Wenn die drei obenstehenden Anweisungen durch folgende Zeilen ersetzt werden, wartet php mit der Ausgabe der Bestätigungsseite,
  *  bis alle Erinnerungen gelöscht sind und gibt auf der Bestätigungsseite aus, wieviele Erinnerungen gelöscht wurden und wieviele
  *  Erinnerungen nicht gelöscht werden konnten.
  *  ACHTUNG: wenn mehr als etwa 15 Erinnerungen zu löschen sind, gibt es einen Gateway-Timeout!
  *
  *  // Aufruf von remote.sh -> deliveready.sh:
  *  exec('sudo '.$remotesh.' clear_vtodolist '.$logging_wait);
  *
  *  // letzte Einträge der Logdatei lesen, um dem User Feedback geben zu können, wieviele Einträge gelöscht und nicht gelöscht werden konnten
  *  if (is_readable('/var/www/nashorn.log.txt')) {
  *    $logtail   = shell_exec('grep clear_tasklist "/var/www/nashorn.log.txt" | tail -n 1');
  *    $deleted   = preg_replace( '/.+\(clear_tasklist\) /', '', $logtail);
  *    $deleted   = preg_replace( '/, .+/', '', $deleted);
  *    $undeleted = preg_replace('/.+, /', '', $logtail);
  *    $undeleted = preg_replace('/ \(.+\)\./', '', $undeleted);
  *    $total     = preg_replace('/.+insgesamt /', '', $logtail);
  *    $total     = preg_replace('/\)\./', '', $total);
  *  }
  *  // Ausgabe für Feedback-Seite zusammenstellen:
  *  $title = 'Meldungen gelöscht';
  *  $text  = 'In der Erinnerungesliste <span style="font-style: italic; color: grey;">Meldung</span> befanden sich insgesamt '.$total.' Erinnerungen:'.PHP_EOL;
  *  $text  .= '<ul style="line-height: 1.5em;"><li>'.$deleted.'</li><li>'.$undeleted.'</li></ul>';
  *
  ********************************************/

  // Feedback-Seite senden und Script beenden:
  feedback_page($title, $text);
  exit(0);
}



/////////////////////////////////////
// DATE
if ( isset($_GET['date']) ) {

    // stellt das Datum auf den übergebenen Wert ein.
    // übergebener Wert muss Datum in folgendem Format sein: `DD.MM.YYYYY`

    $title = 'Datum einstellen';
    $text = '';

    // prüfen, ob übergebenes Datum gültiger Wert ist (DD.MM.YYYY):
    $regex = '/^(3[01]|[12][0-9]|0[1-9])\.(1[012]|0[1-9])\.[0-9]{4}$/';
    if ( preg_match ($regex, $_GET['date']) ) {

        // Aufruf von remote.sh, um das Datum zu prüfen und zu ändern:
        exec( 'sudo '.$remotesh.' date "'.$_GET['date'].'" 2>&1', $output, $exitcode );
        // exec() gibt ein Array zurück, das Zeile für Zeile als vorformatierter Text ausgegeben werden soll:
        $text .= '<pre style="line-height: 1.2em;">' . PHP_EOL;
        foreach ($output as $line) { $text .=  $line . PHP_EOL; }
        $text .= 'Exit-Code: '.$exitcode.'</pre>' . PHP_EOL;

        if ( $exitcode === 0 ) {
            // remote.sh hat Fehlercode 0 zurückgegeben. Das bedeutet, dass das Datum geändert wurde:
            $text .= 'Das Datum wurde erfolgreich geändert auf den ' . $_GET['date'] . '.' . PHP_EOL;
        } else {
            // remote.sh hat Fehlercode zurückgegeben. Das bedeutet, dass `date` einen Fehler zurückgegeben hat.
            $text .= '<span style="color: red;">FEHLER:</span> Das Datum konnte nicht auf '.$_GET['date'].' eingestellt werden.' . PHP_EOL;
        }

    } else {

        // Datum wurde in einem ungültigen Format übergeben:
        $text .= 'Das Datum wurde in einem ungültigen Format übergeben.<br>
                  Gültig ist: <code>TT.MM.JJJJ</code><br>( z.B.: <code>remote.php?date=23.05.2021</code> )' . PHP_EOL;

    }

    // Feedback-Seite senden und Script beenden:
    feedback_page($title, $text);
    exit(0);
}



/////////////////////////////////////
// DELIVEREADY
if ( isset($_GET['deliveready']) ) {

    // Sorgt durch Aufruf von deliveready.sh (über remote.sh) dafür, dass Spuren eines Testboot beseitigt werden.
    // Es wird abgewartet, bis deliveready.sh beendet ist. Die Ausgabe von deliveready.sh wird ins Browserfenster geladen.
    // ACHTUNG: am Ende von deliveready.sh wird 5 Sekunden gewartet, Poweroff wird hier in php am Ende dieser Funktion eingeleitet!

    // die Ausgabe des Shell-Aufrufs soll in einer Variablen gespeichert und auf einer Webseite ausgegeben werden:
    // Aufruf von remote.sh -> deliveready.sh:
    exec( 'sudo '.$remotesh.' deliveready 2>&1', $output, $exitcode );

    // Das Ausgabe-Array wird in der folgenden Schleife zu einem String zusammengefügt. Die String-Variable wird hier deklariert:
    $output_string = '';
    // Farbvariable für die Ausgabe Textfarbe auf Standard setzten (Standard green = OK, red = Fehler):
    $color = 'green';
    // Regex zum prüfen, ob das Wort FEHLER in der Ausgabe vorkommt (wird von remote.sh ausgegeben, falls deliveready Fehlercode zurückgibt):
    $regex = '/FEHLER/';

    // Falls Fehlercode zurückgegeben wurde, die Farbvariable auf red setzen:
    if ( $exitcode !== 0 ) {
        $color = 'red';
    }
    // Jedes Array Element durchgehen und zu $output_string hinzufügen. Außerdem Farbvariable setzen, falls der regex gefunden wird:
    foreach ($output as $line) {
        $output_string .= $line . PHP_EOL;
        if ( preg_match ($regex, $line) ) { $color = 'red'; }
    }

    // je nach Farbvariable Text für Fazit-Zeile erstellen:
    if ( $color == "red" ) {
        $result = '<b>NOT OK:</b> es sind Fehler aufgetreten - <b>nas</b>horn kann so nicht ausgeliefert werden!';
    } else {
        $result = '<b>OK:</b> es sind keine Fehler aufgetreten - <b>nas</b>horn kann ausgeliefert werden.';
    }

    // Ausgabe der Webseite mit dem Log als Feedback für den User:
    $title = 'deliveready - '.date('d.m.Y, H:i:s');
    $text = '<div style="line-height: 1em;">Ausgabe von <code>deliveready.sh:</code>
             <pre style="border: 2px solid '.$color.'; padding: 0.5em; line-height: 1.2em; overflow: scroll">'.$output_string.'</pre>
             <span style="color: '.$color.';"><br>'.$result.'</span><br><br></div>';
    if ( $color === "green" ) {
        $go_back = '<div><b>nas</b>horn wird in 5 Sekunden mit <code>poweroff</code> heruntergefahren.<br>Tschüss!</div>';
        // Poweroff in die Wege leiten:
        exec('sudo '.$remotesh.' poweroff '.$logging_dont_wait);
    } else {
        $go_back = '<div><b>nas</b>horn wird NICHT heruntergefahren.<br>Bitte Fehler korrigieren!<br><br>'.$go_back.'</div>';
    }

    // Feedback-Seite senden und Script beenden:
    feedback_page($title, $text, $go_back);
    exit(0);
}



/////////////////////////////////////
// FIX_PERMISSIONS
if ( isset($_GET['fix_permissions']) ) {

    // Korrigiert die Berechtigungen, falls der Zugriff per Browser auf ein(e) Verzeichnis/Datei per Browser (directory listing)
    // einen 403 (Zugriff verweigert) produziert. Diese Funktion ist die Weiterleitung bei einem 403 von nginx. Daher findet sich
    // der Pfad zum/r Datei/Verzeichnis in der request_uri (die allerdings per 'rawurldecode' für remote.sh dekodiert werden
    // muss, denn z.B. Leerzeichen übergibt nginx als '%20').

    exec('sudo '.$remotesh.' fix_permissions "'.rawurldecode($_SERVER['REQUEST_URI']).'" '.$logging_wait, $output, $exitcode);

    // NOTIZ: Die Ausgabe kann nicht über die Feedback-Seite erfolgen, da per meta-Tag sofort weitergeleitet wird!
    if ( $exitcode === 0 ) {
        // Berechtigungen konnten korrigiert werden. Weiterleiten auf die Datei mit den korrigierten Berechtigungen:
        echo '<html><head>
              <title>fix_permissions</title>
              <meta http-equiv="refresh" content="0; url='.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'">
              </head>
              <body><h1>Berechtigungen korrigiert.</h1>
              <div><hr><br><b>nas</b>horn musste Berechtigungen korrigieren, um auf die angeforderte Datei zugreifen zu können.<br><br>
              Du wirst sofort weitergeleitet.<br><br>
              <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'">
              Hier klicken, falls die Weiterleitung nicht funktionieren sollte...</a>
              </div></body></html>';
    } else {
        // Beim Korrigieren der Berechtigungen trat ein Fehler auf. Daher die Fehlerseite 403.php ausgeben:
        include '/var/www/errorpages/403.php';
    }

    // NOTIZ: Die Ausgabe kann nicht über die Feedback-Seite erfolgen, da per meta-Tag sofort weitergeleitet wird!

    // Weiterleitungs-Seite wurde gesendet, daher kann das Skript auch beendet werden:
    exit(0);
}



/////////////////////////////////////
// IDEVICEBACKUP
if ( isset($_GET['idevicebackup']) ) {

    // Backup eines angeschlossenen iDevices anstoßen (iDeviceBackup-Lock wird in remote.sh gesetzt)
    exec('sudo '.$remotesh.' idevicebackup '.$logging_dont_wait);
    if ( $javascript === 'no' ) {
      // Weiterleitung für noscript ändern auf idevicebackup_noscript id:
      $location = 'index.php?js=no&open=idevice#idevicebackup_noscript';
      // die Weiterleitung verzögern, damit remote.sh das iDeviceBackup-Lock in status.txt gesetzt hat:
      usleep( $microseconds );
    }
}



/////////////////////////////////////
// POWEROFF
if ( isset($_GET['poweroff']) ) {

    // poweroff:
    exec('sudo '.$remotesh.' poweroff '.$logging_dont_wait);

    // die Weiterleitung etwas verzögern, damit remote.sh das Poweroff-Lock in status.txt gesetzt hat:
    usleep($microseconds);
}



/////////////////////////////////////
// REBOOT
if ( isset($_GET['reboot']) ) {

    // reboot:
    exec('sudo '.$remotesh.' reboot '.$logging_dont_wait);

    // die Weiterleitung etwas verzögern, damit remote.sh das Poweroff-Lock in status.txt gesetzt hat:
    usleep($microseconds);
}



/////////////////////////////////////
// REMOVE_LOCKS
if ( isset($_GET['remove_locks']) ) {

    // entfernt alle Locks aus der Statusdatei /var/www/status.txt
    if ( $javascript === 'yes' ) {
        exec('sudo '.$remotesh.' remove_locks '.$logging_dont_wait);
    } else {
        // falls JavaScript deaktiviert ist, warten bis Befehl ausgeführt ist, ansonsten wird veralteter Wert in index.php geladen:
        exec('sudo '.$remotesh.' remove_locks '.$logging_wait);
    }
}



/////////////////////////////////////
// RENEW_ROOTCERT
if ( isset($_GET['renew_rootcert']) ) {

    // nashorn-Root-Zertifikat löschen und neu erstellen:
    exec('sudo '.$remotesh.' renew_rootcert '.$logging_wait);

    $title = 'nashorn Root-Zertifikat wurde erneuert!';
    $text = 'Damit die Synchronisation von Kalendern/Adressbüchern funktioniert, muss das <b>nas</b>horn-Zertifikat
             wie in der Anleitung beschrieben wieder auf den Geräten installiert werden.<br><br>
             Ein eventuell schon installiertes älteres <b>nas</b>horn-Zertifikat sollte von allen Geräten zuvor gelöscht werden.';

    // Feedback-Seite senden und Skript beenden:
    feedback_page($title, $text);
    exit(0);
}



/////////////////////////////////////
// RESTART_ITUNES_SERVER
if ( isset($_GET['restart_itunes_server']) ) {

    // Service owntone neustarten:
    exec('sudo '.$remotesh.' restart_itunes_server '.$logging_dont_wait);
}



/////////////////////////////////////
// RUN_ALL_UPDATES
if ( isset($_GET['run_all_updates']) ) {

    // ${statustxt[updated_until]} zurücksetzen und NAShorn Update aufrufen, um alle Update-Funktionen auszuführen
    // (Notiz: warten nicht möglich, da Update-Prozess zu lange dauert und einen Gateway Timeout erzeugen würde):
    exec('sudo '.$remotesh.' run_all_updates '.$logging_dont_wait);

    $title = 'run_all_updates';
    $text = 'Es werden alle Update-Funktionen seit <b>nas</b>horn Version 1.0.0 ausgeführt.<br>
             Dies wird etliche Minuten dauern.';

    // etwas warten, bevor die Feedback-Seite ausgegeben wird, damit remote.sh sicher das Update-Lock in status.txt gesetzt hat:
    usleep($microseconds);

    // Feedback-Seite senden und Skript beenden:
    feedback_page($title, $text, $go_back);
    exit(0);
}



/////////////////////////////////////
// UPDATE
if ( isset($_GET['update']) ) {

    // Update ausführen (Notiz: warten nicht möglich, da Update-Prozess zu lange dauert und einen Gateway Timeout erzeugen würde):
    exec('sudo '.$remotesh.' update Webfrontend '.$logging_dont_wait);

    // die Weiterleitung etwas verzögern, damit remote.sh das Update-Lock in status.txt gesetzt hat:
    usleep($microseconds);
}



/////////////////////////////////////
// VIDEOLINK
if ( isset($_POST['videolink']) ) {

    // prüfen, ob nur Audio heruntergeladen werden soll und Flag entsprechend setzen:
    if ( isset($_POST['only_audio']) ) {
        $medium = 'Audio';
    } else {
        $medium = 'Video';
    }

    // Video des angegebenen Videolinks mit Hilfe von yt-dlp herunterladen
    exec('sudo '.$remotesh.' youtube-dl "'.$_POST['videolink'].'" "'.$medium.'"'.$logging_dont_wait);
    $location = 'ytdl.php';
    // disable javascript to be able to redirect to website (without javascript, output is only timestamp):
    $javascript = 'no';

    // die Weiterleitung etwas verzögern, damit das Download-Log den neuesten Download schon beinhaltet:
    usleep($microseconds);
}



/////////////////////////////////////
// VIDEOLINK_HISTORY_DELETE
if ( isset($_GET['videolink_history_delete']) ) {

    // Download-History löschen
    exec('sudo '.$remotesh.' youtube-dl videolink_history_delete 0 '.$logging_dont_wait);
    $location = 'ytdl.php';
    // disable javascript to be able to redirect to website (without javascript, output is only timestamp):
    $javascript = 'no';

    // die Weiterleitung etwas verzögern, damit das Download-Log den neuesten Download schon beinhaltet:
    usleep($microseconds);
}



/////////////////////////////////////
// ZIP_LOGFILES
if ( isset($_GET['zip_logfiles']) ) {

    // die Logfiles /var/log/nashorn/* werden gezippt und an den Browser ausgeliefert

    // Zeitstempel erstellen, der als Erweiterung an den Dateinamen des Zip-Files angehängt wird:
    $now = date('Ymd-His');

    // remote.sh aufrufen, um das Zip-File zu erstellen (nicht im Hintergrund ausführen):
    exec('sudo '.$remotesh.' zip_logfiles '.$now.' '.$logging_wait);

    // auf das erstellte Zip-File weiterleiten, damit der Browser dieses herunterlädt:
    header('location: nashorn-logfiles_'.$now.'.zip');

    // und damit das Skript hier beenden (ansonsten werden bei deaktiviertem JavaScript zwei Header gesendet - das kann nicht gut gehen...):
    exit(0);
}



if ( $javascript === 'yes' ) {
    // bei aktiviertem JavaScript landet die Serverantwort in einem versteckten iFrame auf der index.php ('target'-Attribut bei Link-Elementen).
    // Der Webserver liefert html-Code 200 zurück an den Client (falls es keine Fehler gab).
    // Daher muss hier eigentlich nichts weiter ausgegeben oder weitergeleitet werden (zum Debugging ist ein Zeitstempel aber ganz praktisch).
    $zeitstempel = date('d.m.Y - H:i:s');
    echo $zeitstempel;
} else {
    // ohne JavaScript oder falls unbekannt, ob JavaScript: entsprechend weiterleiten (falls nicht im Laufe des Skripts geändert, ist das: index.php):
    header('location: '.$location);
}

?>
