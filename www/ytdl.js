/***************************************************************
**  alle 10 Sekunden über AJAX den Verlauf des Video-Downloaders aktualisieren
***************************************************************/

function loadAjax() {

  // neues http-Request-Objekt erstellen:
  var xhttp = new XMLHttpRequest();

  // Funktion definieren, die ausgeführt wird, wenn sich der Status der empfangenen Daten ändert:
  xhttp.onreadystatechange = function() {

    // wenn die Antwort des Requests bereit steht (4) und http-Code 200 (OK) übergeben wurde:
    if (this.readyState == 4 && this.status == 200) {

     // kompletten Download-Verlauf mit der Antwort des Ajax-Requests ersetzen:
     document.getElementById("download_history").innerHTML = this.responseText;

    }
  }

  // http-Request zusammenstellen und senden:
  xhttp.open("GET", "ytdl_ajax.php", true);
  xhttp.send();
}

// alle 10 Sekunden den Verlauf aktualisieren:
setInterval('loadAjax()', 10000);
