<?php
require '#_functions_ytdl.php';
require '#_functions_common.php';
read_download_history();
?>

<!DOCTYPE html>
<html lang="de">

  <head>
    <title>nashorn Medien-Downloader</title>
    <meta name="apple-mobile-web-app-title" content="nashorn Medien-Downloader">
    <meta name="application-name" content="nashorn Medien-Downloader">
    <!--#include virtual="/common_headers.html.inc" -->
    <link rel="stylesheet" href="<?php echo add_timestamp_to_filename('/ytdl.css'); ?>">
  </head>

  <body>
    <h1><img src="images/logo-text-raster.svg"> Medien-Downloader</h1>
    <p>
      In das Eingabefeld kannst du Links zu Videos aus öffentlich-rechtlichen Mediatheken oder von YouTube einfügen.<br>
      Nach einem Klick auf <i>Herunterladen</i> wird das Video in den Ordner <i>filme</i>
      oder nur das Audio in den Ordner <i>musik</i> auf <b>nas</b>horn heruntergeladen.<br>
    </p>
    <form id="formular" action="remote.php" method="post" class="center">
      <label for="videolink_input">Link zu Video:</label>
      <br>
      <input class="border" type="text" id="videolink_input" name="videolink" placeholder="https://www.youtube.com/watch?v=1234567890">
      <br>
      <label><input type="checkbox" id="only_audio" name="only_audio" value="yes"> nur Audio</label>
      <br>
      <button class="border" type="submit">Herunterladen</button>
    </form>
    <div id="download_history">
      <?php echo $download_history; ?>
    </div>
    <script language="javascript" src="<?php echo add_timestamp_to_filename('/ytdl.js'); ?>"></script>
  </body>

</html>
